package com.perfomatix.sportzapp;

import org.json.JSONObject;

import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SportzApp_UserDetails extends Activity{

	
	LinearLayout user_layout;
	
	RelativeLayout outer;
	
	Firebase user;
	
	ImageLoader imageLoader; 
	
	String Myid ;
	String id,pic,name ;
	
   private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	 
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportzapp_userdetails);
		
		
		user = new Firebase(FIREBASE_URL).child("User/"+Myid);
		
		 imageLoader = new ImageLoader(getApplicationContext());
		 
		user.addListenerForSingleValueEvent(new ValueEventListener() {
	         @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
				@SuppressLint("ResourceAsColor")
				public void onDataChange(DataSnapshot snapshot) {                             
	            
	         	 System.out.println(snapshot.getValue());
	 		    try {
		    		    	
			    			     id = snapshot.child("User_profile/myid").getValue(String.class);
			    			     pic = snapshot.child("User_profile/mypic").getValue(String.class);
			    			     name = snapshot.child("User_profile/myname").getValue(String.class);
			    			    				    		    	
		    		    		
		    		    		outer = new RelativeLayout(getApplicationContext()); 
		    			    	RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
		    			                RelativeLayout.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		    			    	outer.setLayoutParams(rlp);
		    			    	outer.setClickable(true);
		    			    	outer.setBackgroundResource(R.drawable.gradient1); 
		    			    
		    			    	 
		    			    	 JSONObject job_user = new JSONObject(pic);
		 			 			 String user_url = job_user.getString("data");
		 			 			 JSONObject job_user_url = new JSONObject(user_url);
		 			 			 final String user_pic_url = job_user_url.getString("url");
		    			    	 
		    			    		ImageView imageView = new ImageView(getApplicationContext()); 
		    			    		RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
		    			    		imageView.setLayoutParams(ilp); 
		    			    		ilp.addRule(RelativeLayout.CENTER_VERTICAL);
		    			    		ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
		    			    		
		    			    		TextView textView = new TextView(getApplicationContext());
		    			    		RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
		    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
		    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
		    			    		tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
		    			    		textView.setLayoutParams(tlp);
		    			    		textView.setTextColor(Color.parseColor("#424242")); 
		    			    		textView.setTextSize(15);
		    			    		textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
		    			    		textView.setText(name); 
		    			    		
		    			    		TextView tv_vp= new TextView(getApplicationContext());
		    			    		RelativeLayout.LayoutParams tlp1 = new RelativeLayout.LayoutParams(
		    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
		    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
		    			    		tlp1.addRule(RelativeLayout.ALIGN_PARENT_END);
		    			    		tlp1.addRule(RelativeLayout.CENTER_VERTICAL); 
		    			    		tv_vp.setLayoutParams(tlp1);
		    			    		tv_vp.setPaddingRelative(0, 0, 2, 0);
		    			    		tv_vp.setTypeface(Typeface.SERIF,Typeface.BOLD);
		    			    		tv_vp.setTextColor(Color.parseColor("#4dd0e1")); 
		    			    		tv_vp.setTextSize(12); 
		    			    		tv_vp.setText("     View\nPredictions"); 
		    			    		
		    			    		
		    			    		  outer.addView(imageView);
		    			    		  outer.addView(textView);
		    			    		  outer.addView(tv_vp);
		    			    		  user_layout.addView(outer);
		    			    		  imageLoader.DisplayImage(user_pic_url, imageView); 
	 		    }catch(Exception e) {
	 		    	e.printStackTrace();
	 		    	}
	 		    }

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
	 		    
	         });
	}
	

}
