package com.perfomatix.sportzapp;

public class RequestNotification {
	 
	private String created_by;
	    
	    private String Fixture_id = "";
	    private String _match_time= "";
	    private String _match_Date= "";
	    private String Event_id= "";
	    private String flag= "";
	    private String eventName= "";
	 
	    
	    // Required default constructor for Firebase object mapping
	    @SuppressWarnings("unused")
	    private RequestNotification() { }

	    RequestNotification(String flag,String Event_id,String created_by, String Fixture_id,String _match_Date,String _match_time,String eventName) {
	        this.created_by = created_by;        
	        this.Fixture_id = Fixture_id;
	        this._match_Date=_match_Date;
	        this._match_time=_match_time;
	        this.Event_id = Event_id;
	        this.flag = flag;
	        this.eventName = eventName;
	    }
	   
	    public String geteventName() {
	        return eventName;
	    }
	    public String getEvent_id() {
	        return Event_id;
	    }
	    
	    public String getflag() {
	        return flag;
	    }

	    public String getcreated_by() {
	        return created_by;
	    }
	    public String getFixture_id() {
	        return Fixture_id;
	    }
	    public String get_match_Date() {
	        return _match_Date;
	    }
	    public String get_match_time() {
	        return _match_time;
	    }
	

}
