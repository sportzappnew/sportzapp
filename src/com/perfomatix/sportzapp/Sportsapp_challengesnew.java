package com.perfomatix.sportzapp;

import java.util.ArrayList;
import java.util.Hashtable;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;

public class Sportsapp_challengesnew extends Activity {
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	Firebase Qns_Data;
	Firebase Questions;
	Firebase FixtureDetails;
	Firebase challenges;
	Firebase user_Notifications;
	Firebase challenges1;
	Firebase userNotificationAccept;
	Firebase userCreatedDetails;
	
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	Button btninvite;
	RadioButton Rb1;
	RadioButton Rb2;
	RadioButton Rb3;
	
	Hashtable<String,ArrayList<EditText>> editTextDetails;
	Hashtable<String,RadioGroup> radioGroupDetails;
	
	String result;
	String teamA_half = "0";
	String teamB_half = "0";
	String teamA_full = "0";
	String teamB_full = "0";
	String winner;
	String teamAname;
	String teamBname;
	String teamA_yellowCard = "0";
	String teamB_yellowCard = "0";
	String teamA_RedCard = "0";
	String teamB_RedCard = "0";
	String inviteflag="";
	String Myname,Myid,Mypic;
	String created_by;
	String event_id; 
	
	User Mydetails;
	Challenges predictions;
	Firebase declineRef;
	Firebase declineRefCreated;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_challengesnew);
		
			final LinearLayout ll = (LinearLayout)findViewById(R.id.linear);
			
			Questions = new Firebase(FIREBASE_URL).child("Questions");
        	
        	editTextDetails=new Hashtable<String, ArrayList<EditText>>();
        	radioGroupDetails=new Hashtable<String,RadioGroup>();
        	
        	current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
    		current_event_editor = current_event.edit();    		
    		
    		My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
    		My_Details_editor = My_Details.edit();
        	
    		
    		teamAname = current_event.getString("homeTeam",null);
    		teamBname = current_event.getString("awayTeam",null);
    		inviteflag = current_event.getString("inviteflag",null);
    		created_by = current_event.getString("created_by",null);
    		event_id = current_event.getString("Event_id",null);
    		
    		
    		
    		
    		Myid = My_Details.getString("Myid",null); 
    		Myname = My_Details.getString("Myname",null); 
    		Mypic = My_Details.getString("Mypic",null); 
    		
			Questions.addValueEventListener(new ValueEventListener() {
				
			    @SuppressLint("ResourceAsColor")
				@Override
			    public void onDataChange(DataSnapshot snapshot) {
			    	
			    result = snapshot.getValue().toString();
			    			    Log.i("qnsdata", result);			   			     
                
                for(DataSnapshot eachDatasnapshot: snapshot.getChildren()){
                	
                	RelativeLayout li=new RelativeLayout(getApplicationContext());
            		li.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
            		li.setBackgroundColor(R.drawable.gradient2);
            		int form=eachDatasnapshot.child("form").getValue(int.class);
                	String value=eachDatasnapshot.child("value").getValue(String.class);
                	String QndId=eachDatasnapshot.child("Questionid").getValue(String.class);
                	Log.i("inside switch", eachDatasnapshot.child("value").getValue(String.class));
                	Log.i("form data",""+form);
            		
            		TextView tv = new TextView(getApplicationContext());
	     			tv.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	     			tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
	     			tv.setTextSize(15);
	     			tv.setTextColor(Color.BLACK);
	     			tv.setText(value);	     			        			   
	     			ll.addView(li);
	     			ll.addView(tv);

                	
                	
                switch(form){
                		
                	case 1:	
                			LinearLayout l2=new LinearLayout(getApplicationContext());            				
            				LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            				params5.setMargins(20, 10, 10, 10); 
            				params5.weight=1f;
            				l2.setLayoutParams(params5);
            				l2.setLayoutParams(params5);
            				l2.setBackgroundColor(R.drawable.gradient1);
            				            				
                			RadioGroup rg = new RadioGroup(getApplicationContext());
                			rg.setOrientation(RadioGroup.HORIZONTAL);
		        		    
                			Rb1 = new RadioButton(getApplicationContext());		        		    
		        		    RelativeLayout.LayoutParams param_Rb1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);		                	
		        		    Rb1.setLayoutParams(param_Rb1);		        		    
		                	Rb1.setGravity(Gravity.LEFT);		                	
		                	Rb1.setTextColor(Color.BLACK);		                	
		                	Rb1.setText(teamAname);		                	
		                	
		                	Rb2 = new RadioButton(getApplicationContext());
		                	RelativeLayout.LayoutParams param_Rb2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Rb2.setLayoutParams(param_Rb2);
		                	
		                	Rb2.setTextColor(Color.BLACK);
		                	Rb2.setText(teamBname);
		                	Rb2.setGravity(Gravity.CENTER);
		                	
		                	Rb3 = new RadioButton(getApplicationContext());
		                	RelativeLayout.LayoutParams param_Rb3 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Rb3.setLayoutParams(param_Rb3);
		                	Rb3.setTextColor(Color.BLACK);
		                	Rb3.setText("Match Draw");
		                	Rb3.setGravity(Gravity.RIGHT);
		                	
		                	rg.addView(Rb1);
		                	rg.addView(Rb2);
		                	rg.addView(Rb3);
		                	
		                	radioGroupDetails.put(QndId, rg);               			                			                	
		                	
		                	l2.addView(rg);
		                	ll.addView(l2);	            			  
                		break;
                		
                	case 2:	
                			LinearLayout l4=new LinearLayout(getApplicationContext());                			
                			l4.setOrientation(LinearLayout.HORIZONTAL);
                			LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            				params1.setMargins(20, 10, 10, 10);            				           				
            				l4.setLayoutParams(params1);
            				l4.setBackgroundColor(R.drawable.gradient1);
                			
                			TextView tv_Et = new TextView(getApplicationContext());
                			tv_Et.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                			tv_Et.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);                			
                			tv_Et.setTextColor(Color.BLACK);
                			tv_Et.setText(teamAname);
                			
	                		EditText Et = new EditText(getApplicationContext());
	                		RelativeLayout.LayoutParams param_Et = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Et.setLayoutParams(param_Et);
		                	Et.setInputType(InputType.TYPE_CLASS_NUMBER);
		                	Et.setTextColor(Color.BLACK);		                	
		                	Et.setFilters(new InputFilter[] {new InputFilter.LengthFilter(2)});
		                	Et.setGravity(Gravity.LEFT| Gravity.FILL_HORIZONTAL);
		                	Et.setHint("  ");
		                	//Et.setText(teamAname);
		                	//Et.setPadding(5,0,0,0);
		                	
		                	
		                	TextView tv_Et2 = new TextView(getApplicationContext());
		                	tv_Et2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		                	tv_Et2.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);		                	
		                	tv_Et2.setTextColor(Color.BLACK);
		                	tv_Et2.setText(teamBname);
		                	
		                	EditText Et2 = new EditText(getApplicationContext());
		                	RelativeLayout.LayoutParams param_Et2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Et2.setLayoutParams(param_Et2);
		                	Et2.setInputType(InputType.TYPE_CLASS_NUMBER);
		                	Et2.setTextColor(Color.BLACK);
		                	Et2.setFilters(new InputFilter[] {new InputFilter.LengthFilter(2)});
		                	Et2.setHint("  ");
		                	Et2.setGravity(Gravity.RIGHT| Gravity.FILL_HORIZONTAL);
		                	//Et2.setPadding(5,0,0,0);
		                	ArrayList<EditText> edit_Text =new ArrayList<EditText>();
		                	edit_Text.add(Et);
		                	edit_Text.add(Et2);
		                	
		                	editTextDetails.put(QndId,edit_Text);
		                	
		                	l4.addView(tv_Et);
		                	l4.addView(Et);
		                	l4.addView(tv_Et2);
		                	l4.addView(Et2);
		                	ll.addView(l4);	
                			
                		break;
                		
                	case 3:	
                			LinearLayout l3=new LinearLayout(getApplicationContext());
                			LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            				params4.setMargins(20,10,10,10);            				
            				l3.setLayoutParams(params4);
            				l3.setBackgroundColor(R.drawable.gradient1);
	            			l3.setOrientation(LinearLayout.HORIZONTAL);
	            			
	            			TextView tv_Et1 = new TextView(getApplicationContext());
		                	tv_Et1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		                	tv_Et1.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);		                	
		                	tv_Et1.setTextColor(Color.BLACK);
		                	tv_Et1.setText(teamAname);
		                	
	            		    EditText Et1 = new EditText(getApplicationContext());
	            		    RelativeLayout.LayoutParams param_Et1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Et1.setLayoutParams(param_Et1);
	            		    Et1.setTextColor(Color.BLACK);
	            		    //Et1.setText(teamAname);
	            		    Et1.setFilters(new InputFilter[] {new InputFilter.LengthFilter(8)});
	            		    Et1.setHint("   ");
	            		   
	            		    Et1.setGravity(Gravity.LEFT);
	            		    
	            		    TextView tv_Et3 = new TextView(getApplicationContext());
		                	tv_Et3.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		                	tv_Et3.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);		                	
		                	tv_Et3.setTextColor(Color.BLACK);
		                	tv_Et3.setText(teamBname);
		                	
		                	EditText Et3 = new EditText(getApplicationContext());
		                	RelativeLayout.LayoutParams param_Et3 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                	Et3.setLayoutParams(param_Et3);
		                	Et3.setTextColor(Color.BLACK);
		                	//Et3.setText(teamBname);
		                	Et3.setFilters(new InputFilter[] {new InputFilter.LengthFilter(8)});
	            		    Et3.setHint("  ");
		                	Et3.setGravity(Gravity.RIGHT);
		                	
		                	ArrayList<EditText> edit_Text1 =new ArrayList<EditText>();
		                	edit_Text1.add(Et1);
		                	edit_Text1.add(Et3);
		                	
		                	editTextDetails.put(QndId,edit_Text1);
		                	
		                	l3.addView(tv_Et1);
		                	l3.addView(Et1);
		                	l3.addView(tv_Et3);
		                	l3.addView(Et3);
		                	ll.addView(l3);
	                	
	                	break;
                	}
                
                } 
                 btninvite = new Button(getApplicationContext());          
                 btninvite.setText("Challenge Friends");
                 if(inviteflag.equals("notificationConfirm"))
                	 btninvite.setText("Confirm");
                 btninvite.setBackgroundColor(R.drawable.gradient2);
               
                 LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                 ll.addView(btninvite, lp);
                 
               
                 btninvite.setOnClickListener(new View.OnClickListener() {			
          			@Override
          			public void onClick(View v) {
          				
          				RadioButton selectRadio = (RadioButton) findViewById(radioGroupDetails.get("1").getCheckedRadioButtonId());
          				
          				if(selectRadio == null){
          					Toast.makeText(getApplicationContext(),"Please predict the winner", Toast.LENGTH_SHORT).show();
          				}else{
          					
 		         				ArrayList<EditText> halfGoalPre = editTextDetails.get("2");
 		         				ArrayList<EditText> fullGoalPre = editTextDetails.get("3");
 		         				ArrayList<EditText> RedCardPre = editTextDetails.get("4");
 		         				ArrayList<EditText> YellowCardPre = editTextDetails.get("5"); 
 		         				winner = selectRadio.getText().toString();		         				
          	            
 		         				if(validationQns(selectRadio,halfGoalPre,fullGoalPre,RedCardPre,YellowCardPre)){
 		         					if(inviteflag.equals("notificationConfirm")){
 		             					
 			         					Log.i("inside if conditoion","Haiii");
 			         					 
 			         					String friendeventurl = "userEvents/"+created_by+"/"+event_id;
 			         					
 			         					userNotificationAccept = new Firebase(FIREBASE_URL).child("User/"+Myid+"/FriendsEvents");
 			         					userCreatedDetails = new Firebase(FIREBASE_URL).child("userEvents/"+created_by+"/"+event_id+"/attendees/"+Myid);
 			         					 
 			         					teamA_half = halfGoalPre.get(0).getText().toString();
 			             	        	teamB_half = halfGoalPre.get(1).getText().toString();
 			             	        	teamB_full = fullGoalPre.get(1).getText().toString();
 			             	        	teamA_full = fullGoalPre.get(0).getText().toString();
 			             	        	teamA_yellowCard=YellowCardPre.get(0).getText().toString();
 			             	        	teamB_yellowCard=YellowCardPre.get(1).getText().toString();
 			             	        	teamA_RedCard=RedCardPre.get(0).getText().toString();
 			             	        	teamB_RedCard=RedCardPre.get(1).getText().toString();
 			             	        	
 			             	        	userNotificationAccept.push().setValue(friendeventurl);
 			             	        	
 			             	        	Mydetails = new User(Myid,Mypic,Myname);
 			             	        	predictions = new Challenges(event_id, created_by, winner, teamA_half, teamB_half, teamA_full, teamB_full,teamA_yellowCard,teamB_yellowCard,teamA_RedCard,teamB_RedCard);
 			             	        	
 			             	        	userCreatedDetails.child("attendeesDetails").setValue(Mydetails);
 			             	        	userCreatedDetails.child("predictions").setValue(predictions);
 			             	        	
 			             	        	 
 			             	        	 declineRef = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Notifications/"+event_id);
 						   				 declineRef.removeValue();
 						   				 declineRefCreated =  new Firebase(FIREBASE_URL).child("userEvents/"+created_by+"/"+event_id+"/invitees/"+Myid);
 						   				 declineRef.removeValue();
 						   				 
 						   				 Toast.makeText(getApplicationContext(),"Added to your events", Toast.LENGTH_SHORT).show();
 						   				 
 						   			    Intent i = new Intent(Sportsapp_challengesnew.this, Sportz_Notifications.class);				
 										Bundle b = new Bundle();
 										i.putExtras(b);
 										startActivity(i);
 		             					
 		             				}else{
 		             					
 		             					teamA_half = halfGoalPre.get(0).getText().toString();
 		                 	        	teamB_half = halfGoalPre.get(1).getText().toString();
 		                 	        	teamB_full = fullGoalPre.get(1).getText().toString();
 		                 	        	teamA_full = fullGoalPre.get(0).getText().toString();
 		                 	        	teamA_yellowCard=YellowCardPre.get(0).getText().toString();
 		                 	        	teamB_yellowCard=YellowCardPre.get(1).getText().toString();
 		                 	        	teamA_RedCard=RedCardPre.get(0).getText().toString();
 		                 	        	teamB_RedCard=RedCardPre.get(1).getText().toString();
 		                 				
 		                 	            // Get Selected Radio Button and display output
 		                 	            
 		                 	             
 		                 	             //firebase data push
 		            	                Intent i = new Intent(getApplicationContext(), SportzApp_FB_Invite.class);				
 		            					Bundle b = new Bundle();
 		            					b.putString("winner", winner);
 		            					b.putString("teamA_half", teamA_half);
 		            					b.putString("teamB_half", teamB_half);
 		            					b.putString("teamA_full", teamA_full);
 		            					b.putString("teamB_full", teamB_full);
 		            					b.putString("teamA_yellowCard", teamA_yellowCard);
 		            					b.putString("teamB_yellowCard", teamB_yellowCard);
 		            					b.putString("teamA_RedCard", teamA_RedCard);
 		            					b.putString("teamB_RedCard", teamB_RedCard);    					
 		            					i.putExtras(b);
 		            					startActivity(i);
 		             				}
 		         				}
          				 
 						
          			 
          					
          				}
          			}
          		});
 	            }
 				@Override
 				public void onCancelled() {
 					
 					
 				}
 				public boolean validationQns(RadioButton winner,ArrayList<EditText> halfgoal,ArrayList<EditText> fullgoal,ArrayList<EditText> yellowCard,ArrayList<EditText> RedCard){
  					
 					boolean result = false;
  					RadioButton winnerStat = winner; 					
  					int data=winnerStat.getId();
  					
  					String teamAhalf = halfgoal.get(0).getText().toString();
  					String teamBhalf = fullgoal.get(1).getText().toString();
  					String teamBfull = fullgoal.get(1).getText().toString();
  					String teamAfull = fullgoal.get(0).getText().toString();
  					String teamAyellowCard=yellowCard.get(0).getText().toString();
  					String teamByellowCard=yellowCard.get(1).getText().toString();
  					String teamARedCard=RedCard.get(0).getText().toString();
  					String teamBRedCard=RedCard.get(1).getText().toString();
  					
  					
  				
  					
  					if(teamAhalf.equals(null)||teamAhalf.equals("")||teamBhalf.equals(null)||teamBhalf.equals("")||teamAfull.equals(null)||teamAfull.equals("")||teamBfull.equals(null)||teamBfull.equals("")||teamAyellowCard.equals(null)||teamAyellowCard.equals("")||teamByellowCard.equals(null)||teamByellowCard.equals("")||teamARedCard.equals(null)||teamARedCard.equals("")||teamBRedCard.equals(null)||teamBRedCard.equals("")){
  						Toast.makeText(getApplicationContext(),"All fields are mandatory", Toast.LENGTH_SHORT).show();
  						
  					}else{
  						int gteamAhalf = Integer.parseInt(teamAhalf);
  	 					int gteamBhalf = Integer.parseInt(teamBhalf);
  	 					int gteamBfull = Integer.parseInt(teamBfull);
  	 					int gteamAfull = Integer.parseInt(teamAfull);
  	 					
  	 					if(gteamAhalf>gteamAfull||gteamBhalf>gteamBfull){
  							Toast.makeText(getApplicationContext(),"Please enter valid Fulltime goal data", Toast.LENGTH_SHORT).show();
  							
  					}else if(data!=0){
  						
  							 if(Rb1.isChecked()){
  								 
  								 if(gteamAfull<=gteamBfull){
  									 
  									Toast.makeText(getApplicationContext(),"Prediction and details are not matching", Toast.LENGTH_SHORT).show();
  								 }else{
  	 								 result=true;
  	 							 }
  								 
  							 }else if(Rb2.isChecked()){
  								if(gteamAfull>=gteamBfull){
 									 
  									Toast.makeText(getApplicationContext(),"Prediction and details are not matching", Toast.LENGTH_SHORT).show();
  								 }else{
  	 								 result=true;
  	 							 }
  						
  							 }else if(Rb3.isChecked()){
  								if(gteamAfull!=gteamBfull){
 									 
  									Toast.makeText(getApplicationContext(),"Prediction and details are not matching", Toast.LENGTH_SHORT).show();
  								 }else{
  	 								 result=true;
  	 							 }
  							 }
  					
  					}
  						
  					}
  						
  					Log.i("wccccinner",""+data);
  					
  					return result;
  					
  				}
 				
 			});
 			
 	}

 }