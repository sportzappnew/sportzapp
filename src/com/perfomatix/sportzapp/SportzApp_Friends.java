package com.perfomatix.sportzapp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences.Editor;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.fedorvlasov.lazylist.ImageLoader;
 

@SuppressWarnings("deprecation")
public class SportzApp_Friends extends Activity {  
	
		private static String APP_ID = "751359634927818";  
		private Facebook facebook = new Facebook(APP_ID);
		private AsyncFacebookRunner mAsyncRunner;
		String FILENAME = "AndroidSSO_data";
		private SharedPreferences mPrefs;
		
		private SharedPreferences My_Details;
		Editor My_Details_editor;
		
		ImageView football_Image;
		LinearLayout linear_friendlist;
		CheckBox checkBox;
		RelativeLayout rowViewLayout;
		
		ImageLoader imageLoader;
		
		Hashtable<CheckBox, String> hash_check_fbname;
		Hashtable<CheckBox, String> hash_check_fburl;
		Hashtable<CheckBox, String> hash_check_fbappuserid;
		
		ArrayList<CheckBox> checkbox_List;
		
		boolean flag_ = true;
		
		Menu menu;
		
		 ImageButton btn_tellafriend;
		 String Myname,Myid,Mypic;
		 
		
		public static final int DIALOG_LOADING = 1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState); 
	    setContentView(R.layout.sportzapp_friends); 
	    
       /* ActionBar actionBar = getActionBar(); 
        actionBar.setDisplayHomeAsUpEnabled(true);*/
	    
	    btn_tellafriend = (ImageButton)findViewById(R.id.btn_tellafriend);
	    
	    hash_check_fbname = new Hashtable<CheckBox, String>();
	    hash_check_fburl = new Hashtable<CheckBox, String>();
	    hash_check_fbappuserid = new Hashtable<CheckBox, String>();
	    
	    checkbox_List = new ArrayList<CheckBox>();
	    
	    My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
		My_Details_editor = My_Details.edit();
		
	    
	    imageLoader = new ImageLoader(getApplicationContext());
	    linear_friendlist = (LinearLayout)findViewById(R.id.linear_friendlist);
	    
	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    StrictMode.setThreadPolicy(policy);  
	   
	    loginToFacebook_FRIENDS();
	    
	    btn_tellafriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
					String content = "";
					
					if(hash_check_fbname.size() > 0){
						
						Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
						for(int i = 0; enum_check.hasMoreElements(); i++){
							
							CheckBox cb = enum_check.nextElement();
							
							String name = hash_check_fbname.get(cb);
							String pic = hash_check_fburl.get(cb);
							String id = hash_check_fbappuserid.get(cb);
							
							content = content + name +"*" + pic + "*" + id + "#"; 
							
							
						}
		    			
		    		}else{
		    			Toast.makeText(getApplicationContext(), "Select Friends to send invitation", Toast.LENGTH_SHORT).show();
		    		}
					
					flag_ = true;
		    		//((ImageButton)v).setImageResource(R.drawable.ic_action_add_person);
		    		
		    		for(int i = 0; i < checkbox_List.size(); i++){
		    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
		    			checkbox_List.get(i).setChecked(false);
		    		}  
		    		hash_check_fbname.clear();
		    		hash_check_fburl.clear();
		    		hash_check_fbappuserid.clear();
		    		Bundle bundle = new Bundle();
		    		bundle.putString("content", content);
		    		Intent intent = new Intent(getApplicationContext(), Sportz_FB_Tab.class);
		    		intent.putExtras(bundle);
		    		startActivity(intent);
		    		 
				}  	
			
		});
	    
	} 
	
	public void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token); 

			
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" },
					new DialogListener() { 
						@Override
						public void onCancel() { 
						}

						@Override
						public void onComplete(Bundle values) { 
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token", facebook.getAccessToken());
							editor.putLong("access_expires", facebook.getAccessExpires());
							editor.commit(); 
						} 
						@Override
						public void onError(DialogError error) { 

						} 
						@Override
						public void onFacebookError(FacebookError fberror) { 

						}

					});
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	} 
	
	public void logoutFromFacebook() {
		mAsyncRunner.logout(this, new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
			
				if (Boolean.parseBoolean(response) == true) { 
				}
			} 
			@Override
			public void onIOException(IOException e, Object state) {
			} 
			@Override
			public void onFileNotFoundException(FileNotFoundException e, Object state) {
			} 
			@Override
			public void onMalformedURLException(MalformedURLException e, Object state) {
			} 
			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}
	
	public void loginToFacebook_FRIENDS() {
		
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token); 
			 
		     new LoginFb().execute(); 
			 
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		} 
		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" ,"user_photos", "publish_actions", "publish_checkins", "user_friends", "read_friendlists","manage_friendlists"},
					new DialogListener() { 
						@Override
						public void onCancel() { 
						} 
						@Override
						public void onComplete(Bundle values) { 
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token", facebook.getAccessToken());
							editor.commit();   
						     
						    new LoginFb().execute();  
						}

						@Override
						public void onError(DialogError error) { 
						}
						@Override
						public void onFacebookError(FacebookError fberror) { 
						} 
				});
		}
		 
	} 
	
	class LoginFb extends AsyncTask<String, String, String> {
		
		 @Override
	     protected void onPreExecute() {
	          super.onPreExecute(); 
	          
	          showDialog(DIALOG_LOADING);
	     } 
		 protected String doInBackground(String... f_url) {
			 
			 Bundle params = new Bundle();
		     params.putString("fields", "name, picture");
		    // params.putString("fields", "id");
		     
			 try { 
				 JSONObject jsonme = Util.parseJson(facebook.request("me", params));
		    	 JSONObject jsonFrends = Util.parseJson(facebook.request("me/friends", params));
		    	 
		    	 new GetFriendsList().execute(jsonFrends.toString());
		    	 		    	 
		    	    Myname = jsonme.getString("name");								 			
		 			Myid = jsonme.getString("id");
		 			Mypic =  jsonme.getString("picture");	
		 			
		 			    My_Details_editor.putString("Mypic", Mypic );
			    		My_Details_editor.putString("Myname", Myname);
		                My_Details_editor.putString("Myid",Myid );
		                My_Details_editor.commit();
		    	 
		    	 //new CallAPI().execute();
		    	 
			} catch (MalformedURLException e) { 
				e.printStackTrace();
			} catch (JSONException e) { 
				e.printStackTrace();
			} catch (IOException e) { 
				e.printStackTrace();
			} catch (FacebookError e) { 
				e.printStackTrace();
			}  
			 
			 return null;
	     }  
	     
		
		@Override
	     protected void onPostExecute(String file_url) {
	        	 
	    	 dismissDialog(DIALOG_LOADING);
	     }
	}
	 
    class GetFriendsList extends AsyncTask<String, String, String> { 
        @Override
        protected void onPreExecute() {
            super.onPreExecute(); 
        } 
        @Override
        protected String doInBackground(String... f_url) {
        	
        	JSONObject jsonFrends;
			try {
				jsonFrends = new JSONObject(f_url[0]);
				
				JSONArray arrayFriend = jsonFrends.getJSONArray("data"); 
			 	
			 	if (arrayFriend != null) {
			 		
			 		for (int i = 0; i < arrayFriend.length(); i++) {
			 			
			 			final String name = arrayFriend.getJSONObject(i).getString("name");								 			
			 			final String id = arrayFriend.getJSONObject(i).getString("id");
			 			String pic = arrayFriend.getJSONObject(i).getString("picture");
			 			
			 			JSONObject job_user = new JSONObject(pic);
			 			String user_url = job_user.getString("data");
			 			JSONObject job_user_url = new JSONObject(user_url);
			 			final String user_pic_url = job_user_url.getString("url");
			 			
			 			
			 			runOnUiThread(new Runnable() {							
							@SuppressLint("ResourceAsColor")
							@Override
							public void run() {
								
								rowViewLayout = new RelativeLayout(getApplicationContext()); 
								RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
						                RelativeLayout.LayoutParams.MATCH_PARENT,110);
								rowViewLayout.setLayoutParams(rlp);
								rowViewLayout.setClickable(true);
								rowViewLayout.setBackgroundResource(R.drawable.gradient1); 
							 								
									ImageView imageView = new ImageView(getApplicationContext()); 
									RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
									imageView.setLayoutParams(ilp); 
									ilp.addRule(RelativeLayout.CENTER_VERTICAL);
									ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
									
									TextView textView = new TextView(getApplicationContext());
									RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.WRAP_CONTENT,
							                RelativeLayout.LayoutParams.WRAP_CONTENT);
									tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
									textView.setLayoutParams(tlp);
									textView.setTextColor(Color.parseColor("#424242")); 
									textView.setTextSize(12); 
									textView.setText(name); 
									
									checkBox = new CheckBox(getApplicationContext());
									RelativeLayout.LayoutParams clp = new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.WRAP_CONTENT,
							                RelativeLayout.LayoutParams.WRAP_CONTENT);
									clp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT); 
									clp.addRule(RelativeLayout.CENTER_IN_PARENT);
									checkBox.setLayoutParams(clp);
									checkBox.setVisibility(CheckBox.INVISIBLE);
									checkbox_List.add(checkBox);
									
									LinearLayout linearLayout = new LinearLayout(getApplicationContext());
									RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.MATCH_PARENT, 1);
									llp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
									linearLayout.setBackgroundColor(Color.parseColor("#212121"));
 									linearLayout.setLayoutParams(llp);  
 									
									rowViewLayout.addView(imageView);
									rowViewLayout.addView(textView);
									rowViewLayout.addView(linearLayout);
									
									linear_friendlist.addView(rowViewLayout);
					 			 
					 			imageLoader.DisplayImage(user_pic_url, imageView); 
					 			
					 			
					 			
					 			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() { 
									@Override
									public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
										 
										CheckBox chbox = (CheckBox)buttonView;
										if(isChecked == true){
											
											hash_check_fbname.put(chbox, name);
											hash_check_fburl.put(chbox, user_pic_url); 
											hash_check_fbappuserid.put(chbox, id); 
											
											//Toast.makeText(getApplicationContext(), hash_check_fbname.size()+"", Toast.LENGTH_SHORT).show();
											
										}else if(isChecked == false){ 
											
											hash_check_fbname.remove(chbox);
											hash_check_fburl.remove(chbox);
											hash_check_fbappuserid.remove(chbox);
											
											//Toast.makeText(getApplicationContext(), hash_check_fbname.size()+"", Toast.LENGTH_SHORT).show();
											
										}
									}
									
								}); 
					 			for(int j = 0; j < checkbox_List.size(); j++){
					    			checkbox_List.get(j).setVisibility(CheckBox.INVISIBLE);
					    			checkbox_List.get(j).setChecked(false);
					    		}
							}
							
						}); 
			 			
			 		}
			 	}
			} catch (JSONException e) { 
				e.printStackTrace();
			} 
        	
            return null;
        }  
        @Override
        protected void onPostExecute(String file_url) {
        	
        	//btn_next.setVisibility(Button.VISIBLE);
        	//football_Image.setAnimation(null);
        }
    }    

	@Override
	public void onBackPressed() { 
		if(flag_ == true){
			startActivity(new Intent(this,SportzApp_Home.class));
		}else if(flag_ == false){ 
			for(int i = 0; i < checkbox_List.size(); i++){
    			checkbox_List.get(i).setVisibility(CheckBox.INVISIBLE);
    		}
			MenuItem item_done = menu.findItem(R.id.action_send);
		    item_done.setVisible(false);
		    
		    
			flag_ = true;
		}	
	} 
	
	
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case R.id.action_send:	 
	    		for(int i = 0; i < checkbox_List.size(); i++){
	    			checkbox_List.get(i).setVisibility(CheckBox.INVISIBLE);
	    			checkbox_List.get(i).setChecked(false);
	    		}
	    		flag_ = false;
	    		
	    		hash_check_fbname.clear();
	    		hash_check_fburl.clear();
	    		
	    		MenuItem item_done = menu.findItem(R.id.action_send);
	    	    item_done.setVisible(true); 
	    	    item_done.setTitle(R.string.action_send_invitation); 
			    item_done.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			    
	            return true; 
	    
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	 
	 @Override
	 protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DIALOG_LOADING:
	            final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent);
	            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
	            dialog.setContentView(R.layout.custom_progress_dialog);
	            dialog.setCancelable(true);
	            dialog.setOnCancelListener(new OnCancelListener() {
	                public void onCancel(DialogInterface dialog) { 
	 
	                }
	            });
	        return dialog;  
	 
	        default:
	            return null;
	        }
	 };
	 
	 public void getFriendID(){
		 
		 mAsyncRunner = new AsyncFacebookRunner(facebook);
		 Bundle params = new Bundle();
		 params.putString("fields", "name,id");
		 mAsyncRunner.request("/me/friends", params, "GET", new FriendIDListener(), null); 
		 
	 }
	 
	 public class FriendIDListener implements RequestListener { 

		@Override
		public void onComplete(String response, Object state) {
		
			
			/*JSONObject json;
			try {
				json = Util.parseJson(response);
				JSONArray friendsData = json.getJSONArray("data");
				String ids[] = new String[friendsData.length()]; 
				String names[] = new String[friendsData.length()];
				for(int i = 0; i < friendsData .length(); i++){ 
				         ids[i] = friendsData .getJSONObject(i).getString("id");
				         names[i] = friendsData .getJSONObject(i).getString("name");
				}
				
				Log.d("***JSon***", ids.length + " * " + names.length);
				
			} catch (JSONException e) { 
				e.printStackTrace();
			} catch (FacebookError e) { 
				e.printStackTrace();
			}*/
			
		}

		@Override
		public void onIOException(IOException e, Object state) { 
		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) { 
			
		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) { 
			
		}

		@Override
		public void onFacebookError(FacebookError e, Object state) { 
			
		}		  
	 }
	 
		@SuppressWarnings("unused")
		private class CallAPI extends AsyncTask<String, String, String> {		 
		    @Override
		    protected String doInBackground(String... params) {
		    
		    	try{  
		    		
		    		HttpClient httpClient = new DefaultHttpClient();
		    		HttpContext localContext = new BasicHttpContext();
		    		HttpGet httpGet = new HttpGet("http://sportzchat-perfomatix03.rhcloud.com/SportsChat/team/all");	    		 
					
					/*JSONStringer json = new JSONStringer()
				    .object() 
				       .key("id").value(userID)
				       .key("latitude").value(lat)
				       .key("longitude").value(lon)
				       .key("deviceType").value("ANDROID")
				    .endObject();

				    StringEntity entity = new StringEntity(json.toString());
				    entity.setContentType("application/json;charset=UTF-8"); 
				    entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
				    httpPost.setEntity(entity);  */ 
		    		
		    		try {
		    			
		    			HttpResponse response = httpClient.execute(httpGet, localContext);
		    			if(response != null){  
		    				 
		    				InputStream inputStream = response.getEntity().getContent(); 
		    	            String result = convertInputStreamToString(inputStream);
		    				
		    	            
		    			}

		    		} catch (Exception e) { 
		    			
		    			return e.getLocalizedMessage();
		    		}
		    		
		    	}catch(Exception e){ 
		    		Log.i("API Call", e+"");
		    	}	    	
		        return "";
		 
		 }
		 
		 protected void onPostExecute(String result) {	 
		 }
		  
		}
		private static String convertInputStreamToString(InputStream inputStream) throws IOException{ 
			BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream)); 
			String line = ""; 
			String result = "";
			while((line = bufferedReader.readLine()) != null) 
				result += line; 
				inputStream.close(); 
				return  result; 
		}
 
		 
}



