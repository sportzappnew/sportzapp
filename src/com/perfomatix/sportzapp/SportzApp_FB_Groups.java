package com.perfomatix.sportzapp;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fedorvlasov.lazylist.ImageHelper;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;

public class SportzApp_FB_Groups extends Activity{
	
	private LinearLayout group_layout; 
	RelativeLayout outer;
	
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	ArrayList<Bitmap> bitmaps;
	ImageView imageView;
	Bitmap b;
	String Myid ;
	 
	ImageHelper imageHelper;
	ListView listView;
    ImageButton btn_create_new_group;
    Hashtable<RelativeLayout,String > hash_attendes_details;
	Hashtable<RelativeLayout, String> hash_attendes_click;
   
    private Firebase Sportz_ref_group;
    private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_fb_groups);
		
		imageHelper = new ImageHelper();
		
		bitmaps = new ArrayList<>();

		imageView = (ImageView)findViewById(R.id.btn_create_new_group);
	    
		My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
	    My_Details_editor = My_Details.edit();
		
	    hash_attendes_click = new  Hashtable<RelativeLayout, String>();
	    
	    Myid = My_Details.getString("Myid",null);
		listView = (ListView) findViewById(R.id.groupname);
		btn_create_new_group = (ImageButton)findViewById(R.id.btn_create_new_group);
		group_layout = (LinearLayout)findViewById(R.id.group_layout);
		
		
		
		if (Myid!=null|| Myid!=""){
			
			groups();
		}
		
		
		
		try{
			
			Bundle bundle = this.getIntent().getExtras();
			String content = bundle.getString("content");
			
			Log.i("haiiiiii", content);
			
			
			
			if(getIntent().hasExtra("byteArray")) {
			   
			    b = BitmapFactory.decodeByteArray(
			    getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);        
			    imageView.setImageBitmap(b);
			   
			   // groups();
			
			}
			
		}catch(Exception e){
			
			btn_create_new_group.setImageResource(R.drawable.ic_action_add_group);
		}
		
		btn_create_new_group.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				Intent i = new Intent(SportzApp_FB_Groups.this, Sportz_app_used_friends.class);				
				Bundle b = new Bundle();
				i.putExtras(b);
				startActivity(i);
				
			}
		});
		
	
   }
	
	
	
	private void groups() {
		// TODO Auto-generated method stub
		
		Sportz_ref_group = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Group/");
		Sportz_ref_group.addListenerForSingleValueEvent(new ValueEventListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
			@SuppressLint("ResourceAsColor")
			public void onDataChange(DataSnapshot snapshot) {                             
               
            	 System.out.println(snapshot.getValue());
    		    try {
	    		    	for ( DataSnapshot snapshot1 : snapshot.getChildren()) {
	    		    		
	    		    		System.out.println(snapshot1.getValue());
	    		    		
		    			    	 String id = snapshot1.child("id").getValue(String.class);
		    			    	 String Groupid = snapshot1.child("Groupid").getValue(String.class);
		    			    	 String gname = snapshot1.child("gname").getValue(String.class);
		    			    
		    			    	 Log.i("id", id);
		    			    	 Log.i("pic", Groupid);
		    			    	 Log.i("name", gname);

	    		    		
	    		    		outer = new RelativeLayout(getApplicationContext()); 
	    			    	RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
	    			                RelativeLayout.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
	    			    	outer.setLayoutParams(rlp);
	    			    	outer.setClickable(true);
	    			    	outer.setBackgroundResource(R.drawable.gradient1); 
	    			    	
	    			    	 hash_attendes_click.put(outer,Groupid);	
	    			    	 
	    			    	/* JSONObject job_user = new JSONObject(pic);
	 			 			 String user_url = job_user.getString("data");
	 			 			 JSONObject job_user_url = new JSONObject(user_url);
	 			 			 final String user_pic_url = job_user_url.getString("url");*/
	    			    	 
	    			    		ImageView imageView = new ImageView(getApplicationContext()); 
	    			    		RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
	    			    		imageView.setLayoutParams(ilp); 
	    			    		ilp.addRule(RelativeLayout.CENTER_VERTICAL);
	    			    		ilp.addRule(RelativeLayout.ALIGN_RIGHT );
	    			    		imageView.setImageResource(R.drawable.ic_communities);
	    			    		
	    			    		TextView textView = new TextView(getApplicationContext());
	    			    		RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
	    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
	    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
	    			    		tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
	    			    		textView.setLayoutParams(tlp);
	    			    		textView.setTextColor(Color.parseColor("#424242")); 
	    			    		textView.setTextSize(15);
	    			    		textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
	    			    		textView.setText(gname); 
	    			    		
	    			    		
	    			    		
	    			    		  outer.addView(imageView);
	    			    		  outer.addView(textView);	    			    		  	    			    		  
	    			    		  group_layout.addView(outer);
				 			       
				 			
				 			       outer.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {  
											
											/*Bundle b = new Bundle();
				    							  Intent i = new Intent(getApplicationContext(), Sportz_Event_Details.class);
				    							  i.putExtras(b);
				    							  startActivity(i);*/
									
									}		
								});	
	    		    	}
    		    }catch(Exception e){
    		    	
    		    	e.printStackTrace();
    		    	
    		      }
    		    }

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}

		
    		    
            });
       
	} 

	



	@SuppressWarnings("unused")
	private Image getImage(ImageView previewThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}



	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	@SuppressWarnings("unused")
	private Bitmap ProcessingBitmap(){
		
		  Bitmap bm1 = null;
		  Bitmap bm2 =  null;
		  Bitmap bm3 = null;
		  Bitmap bm4 =  null;
		  
		  Bitmap newBitmap = null;
		  
		  bm1 = bitmaps.get(0);
		  bm2 = bitmaps.get(1);
		  bm3 = bitmaps.get(2);
		  bm4 = bitmaps.get(3);		   
		  
		  int w = bm1.getWidth() + bm2.getWidth();
		  int h ;
		  if(bm1.getHeight() >= bm2.getHeight()){
		   h = bm1.getHeight() * 2;
		  }else{
		   h = bm2.getHeight() * 2;
		  }
		  
		  Config config = bm1.getConfig();
		  if(config == null){
		   config = Bitmap.Config.ARGB_8888;
		  }
		   
		  newBitmap = Bitmap.createBitmap(w, h, config);
		  Canvas newCanvas = new Canvas(newBitmap);
		   
		  newCanvas.drawBitmap(bm1, 0, 0, null);
		  newCanvas.drawBitmap(bm2, bm1.getWidth(), 0, null);
		  newCanvas.drawBitmap(bm3, 0, bm1.getHeight(), null);
		  newCanvas.drawBitmap(bm4, bm1.getWidth(), bm1.getHeight(), null); 
		  
		  return newBitmap;
	}
	    
	public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
		  float ratio = Math.min(
		            (float) maxImageSize / realImage.getWidth(),
		            (float) maxImageSize / realImage.getHeight());
		  int width = Math.round((float) ratio * realImage.getWidth());
		  int height = Math.round((float) ratio * realImage.getHeight());
          Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
		  return newBitmap;
	}
	
	
	
	

}
