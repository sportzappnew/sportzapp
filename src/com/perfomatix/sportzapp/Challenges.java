package com.perfomatix.sportzapp;

public class Challenges {
	
	private String teamA_half_goal="0";
	private String teamB_half_goal="0";
	private String teamA_full_goal="0";
	private String teamB_full_goal="0";
	private String teamA_yellowCard="0";
	private String teamB_yellowCard="0";
	private String teamA_RedCard="0";
	private String teamB_RedCard="0";
	private String winner="0";
	private String Event_id="0";
	private String user_id="0";

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Challenges() { }

    Challenges(String Event_id ,String user_id ,String winner, String teamA_half_goal, String teamB_half_goal, String teamA_full_goal, String teamB_full_goal,String teamA_yellowCard,String teamB_yellowCard,String teamA_RedCard,String teamB_RedCard) {
        this.winner = winner;
        this.teamA_half_goal = teamA_half_goal;
        this.teamB_half_goal = teamB_half_goal;
        this.teamA_full_goal = teamA_full_goal;
        this.teamB_full_goal = teamB_full_goal;
        this.teamA_yellowCard=teamA_yellowCard;
    	this.teamB_yellowCard=teamB_yellowCard;
    	this.teamA_RedCard=teamA_RedCard;
    	this.teamB_RedCard=teamB_RedCard;
        this.Event_id = Event_id;
        this.user_id = user_id;
    }

    public String getteamA_half_goal() {
        return teamA_half_goal;
    }
    public String getteamB_half_goal() {
        return teamB_half_goal;
    }
    public String getteamA_full_goal() {
        return teamA_full_goal;
    }
    public String getteamB_full_goal() {
        return teamB_full_goal;
    }
    public String getEvent_id() {
        return Event_id;
    }
    public String getwinner() {
        return winner;
    }
    public String getteamA_yellowCard() {
        return teamA_yellowCard;
    }
    public String getteamB_yellowCard() {
        return teamB_yellowCard;
    }
    public String getteamA_RedCard() {
        return teamA_RedCard;
    }
    public String getteamB_RedCard() {
        return teamB_RedCard;
    }
    public String getuser_id() {
        return user_id;
    }

}
