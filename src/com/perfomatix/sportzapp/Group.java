package com.perfomatix.sportzapp;

public class Group {

    private String gname;
 
    private String Id;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Group() { }

    Group(String gname, String Id) {
        this.gname = gname;
        this.Id = Id;
    }

    public String getgname() {
        return gname;
    }

    public String getId() {
        return Id;
    }
}
