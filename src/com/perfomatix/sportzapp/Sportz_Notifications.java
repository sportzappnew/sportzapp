package com.perfomatix.sportzapp;

import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;


public class Sportz_Notifications extends Activity {  
	
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	String Myid ;
	
	LinearLayout notifications;
	RelativeLayout notifyViewLayout;
	
	Menu menu;
	
	ImageLoader imageLoader;
	
	
	
	Hashtable<String, ArrayList<String>> hash_notification_details;
	Hashtable<RelativeLayout, String> hash_notification_click;
	
	
	 public static final int DIALOG_LOADING = 1;
	
	 Firebase Notification;
	 String VText="";
	 
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	
 @Override
 public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState); 
    setContentView(R.layout.sportz_notifications); 
    hash_notification_click = new Hashtable<RelativeLayout, String>();
    
    hash_notification_details = new Hashtable<String, ArrayList<String>>();
    My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
    My_Details_editor = My_Details.edit();
    
	Myid = My_Details.getString("Myid","null"); 
	Log.i("myid", Myid);
    
    
    Notification = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Notifications");
    
    new Notification().execute();
    
    imageLoader = new ImageLoader(getApplicationContext());
    
    notifications = (LinearLayout)findViewById(R.id.notificationlist);
    current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
	current_event_editor = current_event.edit();
   
   
    		
   }
		
	
  public class Notification extends AsyncTask<String, String, String> {	
			
			String result = ""; 
			
			@Override
		    protected void onPreExecute() {
				 
				super.onPreExecute(); 
				
		    } 
			@Override
		    protected String doInBackground(String... params) {
				
				  Notification.addValueEventListener(new ValueEventListener() {
			            @SuppressLint({ "ResourceAsColor", "InlinedApi" })
						@Override
			            public void onDataChange(DataSnapshot snapshot) { 
			            	
			               
			                	
			                	 try { 
			                		 if(snapshot == null){
						                	
						                	Toast.makeText(getApplicationContext(), "No Notifications", Toast.LENGTH_SHORT).show();
						                	
						                }else{
					    		    	for ( DataSnapshot snapshot1 : snapshot.getChildren()) {
					    		    		
						    			    	ArrayList<String> nfdetails = new ArrayList<String>();
						    			    	
						    			    	nfdetails.add(snapshot1.child("event_id").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("flag").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("fixture_id").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("created_by").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("_match_Date").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("_match_time").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("eventName").getValue(String.class));	
						    			    	nfdetails.add(snapshot1.child("CreatedBy/myid").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("CreatedBy/myname").getValue(String.class));
						    			    	nfdetails.add(snapshot1.child("CreatedBy/mypic").getValue(String.class));	
						    			    	//details.add(snapshot1.child("createdUserName").getValue(String.class));
						    			    	
						    			    	hash_notification_details.put(snapshot1.child("event_id").getValue(String.class), nfdetails);
						    			    	
						    			    	 String event_id = snapshot1.child("event_id").getValue(String.class);
						    			    	 String flag = snapshot1.child("flag").getValue(String.class);
						    			    	 String fixture_id = snapshot1.child("fixture_id").getValue(String.class);
						    			    	 String created_by =snapshot1.child("created_by").getValue(String.class);
						    			    	 String _match_Date =snapshot1.child("_match_Date").getValue(String.class);
							 			   		 String _match_time = snapshot1.child("_match_time").getValue(String.class);
												 String eventName = snapshot1.child("eventName").getValue(String.class); 
												 String CreatedByid =snapshot1.child("CreatedBy/myid").getValue(String.class);
							 			   		 String CreatedByname = snapshot1.child("CreatedBy/myname").getValue(String.class);
												 String CreatedBypic = snapshot1.child("CreatedBy/mypic").getValue(String.class);

												
																    			    	
						    			    	notifyViewLayout = new RelativeLayout(getApplicationContext()); 
						    			    	RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
						    			        RelativeLayout.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
						    			    	notifyViewLayout.setLayoutParams(rlp);
						    			    	notifyViewLayout.setClickable(true);
						    			    	notifyViewLayout.setBackgroundResource(R.drawable.gradient1); 
						    			    	
						    			    	if(flag.equals("createChallenge")){
						    			    		
						    			    		notifyViewLayout.setBackgroundResource(R.drawable.gradient2); 
						    			    	
						    			    	}else if(flag.equals("createInvite")){
						    			    		
						    			    		notifyViewLayout.setBackgroundResource(R.drawable.gradient);
						    			    		
						    			    	}
						    			    	
						    			    	 JSONObject job_user = new JSONObject(CreatedBypic);
						 			 			 String user_url = job_user.getString("data");
						 			 			 JSONObject job_user_url = new JSONObject(user_url);
						 			 			 final String user_pic_url = job_user_url.getString("url");
						    			    	
						    			    	hash_notification_click.put(notifyViewLayout,event_id);	
						    			    	
					    			    		ImageView imageView = new ImageView(getApplicationContext()); 
					    			    		RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
					    			    	
					    			    		imageView.setLayoutParams(ilp); 
					    			    		ilp.addRule(RelativeLayout.CENTER_VERTICAL);
					    			    		ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
						    			    		
					    			    		TextView textView = new TextView(getApplicationContext());
					    			    		RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
					    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
					    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
					    			    		tlp.addRule(RelativeLayout.CENTER_VERTICAL);
					    			    		tlp.addRule(RelativeLayout.RIGHT_OF,imageView.getId());
					    			    		textView.setLayoutParams(tlp);
					    			    		textView.setTextColor(Color.parseColor("#424242")); 
					    			    		textView.setTextSize(12);
					    			    		textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
					    			    		VText=CreatedByname+" has challenged you ! ";
					    			    		if(flag.toString().equals("createInvite")){
					    			    			VText=CreatedByname+" has invited you to \n the ISL game on "+_match_Date+".";
					    			    		}
					    			    		textView.setText(VText); 
						    			    			
				    			    			notifyViewLayout.addView(imageView);
				    			    			notifyViewLayout.addView(textView);
				    			    			
				    			    		
				    			    			 notifications.addView(notifyViewLayout);
				    			    			 imageLoader.DisplayImage(user_pic_url, imageView);
						    			    			
						    			    			notifyViewLayout.setOnClickListener(new View.OnClickListener() {									
														 
															@Override
															public void onClick(View v) {  
																
																RelativeLayout out = (RelativeLayout)v;
																String id = hash_notification_click.get(out);
																ArrayList<String> notifidetailsclick_ = hash_notification_details.get(id);
																
																Log.i("event_id sss", notifidetailsclick_.get(0));
																Log.i("flag sss", notifidetailsclick_.get(1));
																Log.i("fixture_id sss", notifidetailsclick_.get(2));
																Log.i("created_by sss", notifidetailsclick_.get(6));
																
																Bundle b = new Bundle();
					     		    							  
																current_event_editor.putString("Event_id",notifidetailsclick_.get(0));
								    			                current_event_editor.putString("flag",notifidetailsclick_.get(1) );
								    			                current_event_editor.putString("fixture_id",notifidetailsclick_.get(2) );
								    			                current_event_editor.putString("created_by",notifidetailsclick_.get(3) );
								    			                current_event_editor.putString("_match_Date", notifidetailsclick_.get(4) );
								    			                current_event_editor.putString("_match_time", notifidetailsclick_.get(5) );
								    			                current_event_editor.putString("user_event_name", notifidetailsclick_.get(6) );							    			               
								    						    current_event_editor.commit();
																 	
																 // b.putString("createdUserName", notifidetailsclick_.get(7));
				     		    							
					     		    							  Intent i = new Intent(Sportz_Notifications.this, SportzApp_Notification_Details.class);
					     		    							  i.putExtras(b);
					     		    							  startActivity(i);
																
																}	
															});	 
						    			    			
					    		    	}
					    		    	
						                }	
						            	
				    		    }catch(Exception e){
				    		    	Log.e("error",""+e);
				    		      }
			                	
			    		    }

						@Override
						public void onCancelled() {
							// TODO Auto-generated method stub
							
						}
			    		    
			            });
				return result;
				  
				
			}
			protected void onPostExecute(String result) {
				// dismissDialog(DIALOG_LOADING);
				
				 }
			  
			}
	 
		

	}
	
	

	


	

