package com.perfomatix.sportzapp;


public class Userevents {
 
 private String created_by;
    private String userevents_id;
    private String Fixture_id = "";
    private String _match_time= "";
    private String _match_Date= "";
    private String eventName= "";
 
 
    
    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Userevents() { }

    Userevents(String created_by, String Fixture_id,String _match_Date,String _match_time, String eventName) {
        this.created_by = created_by;        
        this.Fixture_id = Fixture_id;
        this._match_Date=_match_Date;
        this._match_time=_match_time;
        this.eventName=eventName;
    }

    public String getuserevents_id() {
        return userevents_id;
    }

    public String getcreated_by() {
        return created_by;
    }
    public String getFixture_id() {
        return Fixture_id;
    }
    public String get_match_Date() {
        return _match_Date;
    }
    public String get_match_time() {
        return _match_time;
    }
    public String geteventName() {
        return eventName;
    }

}