package com.perfomatix.sportzapp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.firebase.client.Firebase;

public class Sportz_myEvents extends Activity {
	
	LinearLayout myevents_layout;
    private static final int DIALOG_LOADING = 1;
	String _id = "";
	String _teamA_logo_url = "";
	String _teamB_logo_url = "";
	String _teamA_Name = "";
	String _teamB_Name = ""; 
	String _match_venue = "";
	String userName = "";
	String userID = "483763955060529";
	String userPic = "";
	String created_by = "";
	String ueid = "";
	String _match_time = "";
	SharedPreferences UserName_;
	
	 Hashtable<RelativeLayout, String> hash_event_click;
	 Hashtable<RelativeLayout, String> hash_event_teamIDs;
	 Hashtable<String, ArrayList<String>> hash_event_details; 
	 Hashtable<Date, String> hash_event_date_id; 
	 
	
	Firebase Event_ref;
	Firebase myevents_ref;
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState); 
	    setContentView(R.layout.sportz_myevents); 
	    
	   
	    myevents_layout = (LinearLayout)findViewById(R.id.myevent_layout);
	    
	    Event_ref = new Firebase(FIREBASE_URL).child("Fixtures");	
	    myevents_ref = new Firebase(FIREBASE_URL).child("userEvents/"+userID+"/");
	    new CallEventsApi().execute();  
	   
	}

	private class CallEventsApi extends AsyncTask<String, String, String> {
		
		@SuppressWarnings("deprecation")
		protected void onPreExecute() {
	         super.onPreExecute(); 
	         showDialog(DIALOG_LOADING);
	         
	    } 
		@Override
		protected String doInBackground(String... params) {
			
			return null;
		}
		@SuppressWarnings("deprecation")
		protected void onPostExecute(String result) {
			 dismissDialog(DIALOG_LOADING);
			
		}
	}
	
	@SuppressWarnings("unused")
	private String getDate(String event_Date){
		String date = "";
		String month = "";
		
		String a[] = event_Date.split("\\,"); 
		String day = a[0];
		String date_[] = a[1].split("\\-");
		switch(date_[1]){
		case "01":
			month = "January";
			break;
		case "02":
			month = "February";
			break;
		case "03":
			month = "March";
			break;
		case "04":
			month = "April";
			break;
		case "05":
			month = "May";
			break;
		case "06":
			month = "June";
			break;
		case "07":
			month = "July";
			break;
		case "08":
			month = "August";
			break;
		case "09":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
			
		}	
		
		date = day+" "+date_[0]+" "+month+" "+date_[2]; 
		return date;
	}
	 @Override
	 protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DIALOG_LOADING:
	            final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent);
	            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
	            dialog.setContentView(R.layout.custom_progress_dialog);
	            dialog.setCancelable(true);
	            dialog.setOnCancelListener(new OnCancelListener() {
	                public void onCancel(DialogInterface dialog) { 
	 
	                }
	            });
	        return dialog;  
	 
	        default:
	            return null;
	        }
	 };
	class DateCompare implements Comparator<Date> {
		public int compare(Date one, Date two){
		return one.compareTo(two);
		}
    }
}
