package com.perfomatix.sportzapp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;

public class Sportz_HomeFragment extends Fragment{ 	
 
	public static final int DIALOG_LOADING = 1; 
	
	private static boolean flag = true;
	
	FragmentManager fm;
    DFragment dFragment;
    
    Button fixtures;
    Button myevents;
    Button frndsevents;
    
    String fixture_id = "";
    String created_by = "";
	String ueid = "";
	String teamA;
	String Event_id;
    String teamB;
    String Myid ;
    String myEvent_id ;
    String myCreated_by ;
    String myFixture_id;
    String myFrndsFixture_id;
    String countryId = "india";
    String sportsId = "football";
    String leagueId = "isl2014";
    String fixtureUrl;
    String resultDetails;
    String result_Flag = "0";	

    
    SharedPreferences My_Details;
    SharedPreferences teamName_pref;
    SharedPreferences current_event;
    
    Editor current_event_editor;    
    Editor teamName_editor;    
	Editor My_Details_editor;
    
    TextView allfixtures;
    
    boolean flagFixture = true ;
	boolean flagMyEvent = true ;
    
    Hashtable<RelativeLayout, String> hash_fixtureEvent_click;
    Hashtable<RelativeLayout, String> hash_myEvent_click;
    Hashtable<RelativeLayout, String> hash_myFrndsEvent_click;
    Hashtable<RelativeLayout, String> hash_event_teamIDs;
    static Hashtable<String, ArrayList<String>> hash_event_details; 
    Hashtable<String, ArrayList<String>> hash_myevent_details; 
    Hashtable<String, ArrayList<String>> hash_myFrndEvent_details;
    Hashtable<Date, String> hash_event_date_id;
    Hashtable<Date, String> hash_myevent_date_id;
    Hashtable<Date, String> hash_myFrndEvent_date_id;
    Hashtable<Date, String> hash_fixture_date_id; 
    
    Hashtable<String, ArrayList<String>> hash_fixture;
    
    Hashtable<String, ArrayList<Parselable_Fixture>> array_list_FV;
    ArrayList<Parselable_Fixture> arr_fixtures;
    
    ArrayList<Date> a;
    ArrayList<Date> myEventsDate;
    ArrayList<Date> myFrndsEventsDate;
    DateCompare compare = new DateCompare();
    DateCompare compare12 = new DateCompare();
    
    Firebase Event_ref;
	Firebase myevents_ref;
	Firebase frndsevents_ref;
	Firebase myeventsFriendsEvents_ref;
	
	LinearLayout fixtureLayout;
	LinearLayout eventLayout;
	LinearLayout FrndsEventLayout;
	
	LinearLayout hostLayout_f;
	LinearLayout hostLayout_e;
	LinearLayout hostLayout_frnds;
	
	long count;
    
    private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";  
    
    @SuppressWarnings("deprecation")
	public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState); 
        
        if(savedInstanceState != null){
        	
        	Test t = new Test();
     		Bundle bundle = t.getBundle();
     		hostLayout_f = t.getHost_F();
     		hostLayout_e = t.getHost_E();
     		hostLayout_frnds = t.getHost_FR();
        	
        } 
        
        hash_fixture = new Hashtable<String, ArrayList<String>>();
		hash_fixture_date_id = new Hashtable<Date,String>();
		
		hash_fixtureEvent_click = new Hashtable<RelativeLayout, String>();
		hash_myEvent_click = new Hashtable<RelativeLayout, String>();
		hash_myFrndsEvent_click = new Hashtable<RelativeLayout, String>();
		hash_event_teamIDs = new Hashtable<RelativeLayout, String>();
		hash_event_details = new Hashtable<String, ArrayList<String>>();
		hash_myevent_details = new Hashtable<String, ArrayList<String>>();
		hash_event_date_id = new Hashtable<Date,String>();
		hash_myevent_date_id = new Hashtable<Date,String>();
		
		hash_myFrndEvent_date_id = new Hashtable<Date,String>();		
		hash_myFrndEvent_details = new Hashtable<String, ArrayList<String>>();
		myFrndsEventsDate = new  ArrayList<Date>();
		
		My_Details =  getActivity().getApplicationContext().getSharedPreferences("My_Details", Context.MODE_PRIVATE);
        My_Details_editor = My_Details.edit();

		current_event = getActivity().getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		current_event_editor.clear(); 
		
	    fixtureLayout = new LinearLayout(getActivity().getApplicationContext());
	    fixtureLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	    fixtureLayout.setOrientation(LinearLayout.VERTICAL);
	    
	    eventLayout = new LinearLayout(getActivity().getApplicationContext());
	    eventLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	    eventLayout.setOrientation(LinearLayout.VERTICAL); 
		
		array_list_FV = new Hashtable<String, ArrayList<Parselable_Fixture>>(); 
	
		fm = getFragmentManager();
	    dFragment = new DFragment();
		
        if(savedInstanceState != null){
			Log.e("Inside onCreate", "Content Exists in savedInstanceState");
		}else{
			Log.e("Inside onCreate", "Content not Exists in savedInstanceState");
		}
    }

	@SuppressLint("ResourceAsColor")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		 		
		/*Test t = new Test();
		Bundle bundle = t.getBundle();*/
		
		View rootView = inflater.inflate(R.layout.sportz_app_fixture_view, container, false);
		/*
		if(bundle == null){	*/		
			
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build(); 
			StrictMode.setThreadPolicy(policy);  
			Myid = My_Details.getString("Myid",null); 
			
		    fixtures = (Button)rootView.findViewById(R.id.fixtures);
		    myevents = (Button)rootView.findViewById(R.id.myevents);
		    frndsevents = (Button)rootView.findViewById(R.id.frndevents);
		   
		    fixtures.setBackgroundResource(R.drawable.btn_greeen_pressed);
		    fixtures.setTextColor(R.color.btn_green);
		    frndsevents.setBackgroundResource(R.drawable.btn_green);
		    frndsevents.setTextColor(Color.WHITE);		    
		    myevents.setBackgroundResource(R.drawable.btn_green);
		    myevents.setTextColor(Color.WHITE);
		    
		    if(Myid == null){
		    	
		    	frndsevents.setEnabled(false);
		    	myevents.setEnabled(false);
		    	
		    	myevents.setTextColor(Color.WHITE);
		    	frndsevents.setTextColor(Color.WHITE);
		    	myevents.setBackgroundResource(R.drawable.gradient1);
		    	frndsevents.setBackgroundResource(R.drawable.gradient1);
		    }
		    
		      
		    
		   
		    
		    if(flag == true){
		    	
		    	new VIEW_ALL_FIXTURE().execute();
		    }		  
			
		    fixtures.setOnClickListener(new View.OnClickListener() 
		    {               
		        @SuppressLint("ResourceAsColor")
				@Override
		        public void onClick(View arg0) 
		        {  
		        	myevents.setBackgroundResource(R.drawable.btn_green);
		        	myevents.setTextColor(Color.WHITE);
		        	frndsevents.setBackgroundResource(R.drawable.btn_green);
		        	frndsevents.setTextColor(Color.WHITE);
		        	fixtures.setBackgroundResource(R.drawable.btn_greeen_pressed); 
		        	fixtures.setTextColor(R.color.btn_green); 
		        	
		        		if(hostLayout_e.getVisibility() == LinearLayout.VISIBLE){
		        			if(hostLayout_frnds!=null){
						          hostLayout_frnds.setVisibility(View.GONE);						         
				        		}
			            	hostLayout_e.setVisibility(View.GONE);		            	
			        		hostLayout_f.setVisibility(View.VISIBLE);
			        	
			        	
			        	}else if(hostLayout_frnds!=null){
			        		
			        		if(hostLayout_frnds.getVisibility() == LinearLayout.VISIBLE){
			        			
				            	hostLayout_e.setVisibility(View.GONE);	
				            	hostLayout_frnds.setVisibility(View.GONE);
				        		hostLayout_f.setVisibility(View.VISIBLE);
			        		
			        		}    		
			        	}else{
			        		hostLayout_f.setVisibility(View.VISIBLE);
			        	}
		        }
		        
		    }); 
		    
		    myevents.setOnClickListener(new View.OnClickListener() 
		    {   
		        @SuppressLint("ResourcedAsColor")
				@Override
		        public void onClick(View arg0) 
		        { 	 if(Myid == null){
		        	
		        	  Toast.makeText(getActivity().getApplicationContext(), "Please create an event", Toast.LENGTH_SHORT).show();
		        	
			        }else{
			        	Log.i("hostLayout_f","inside my events else   myid  " +Myid);
			        	if(hostLayout_f.getVisibility() == LinearLayout.VISIBLE ){
			        		Log.i("hostLayout_f","my even hostLayout_f value not null");        		
			        		hostLayout_f.setVisibility(View.GONE); 
			        		if(hostLayout_frnds!=null)
			        		if(hostLayout_frnds.getVisibility() == LinearLayout.VISIBLE){		 	        		
			        			hostLayout_frnds.setVisibility(View.GONE);	
			        		}
			        		if(hostLayout_e!=null)
			        		hostLayout_e.setVisibility(View.VISIBLE);
			        	   
			        	}else if(hostLayout_frnds!=null){
			        		if(hostLayout_frnds.getVisibility() == LinearLayout.VISIBLE){
			        			
			        			hostLayout_f.setVisibility(View.GONE);
			        			hostLayout_frnds.setVisibility(View.GONE);
			        			hostLayout_e.setVisibility(View.VISIBLE);
			        			
			        		}
			        	}else{
			        		hostLayout_e.setVisibility(View.VISIBLE);
			        	}	        		 
			        	  
			        	myevents.setBackgroundResource(R.drawable.btn_greeen_pressed); 
			        	myevents.setTextColor(R.color.btn_green);
			        	fixtures.setBackgroundResource(R.drawable.btn_green); 
			        	fixtures.setTextColor(Color.WHITE);
			        	frndsevents.setBackgroundResource(R.drawable.btn_green); 
			        	frndsevents.setTextColor(Color.WHITE);
			        	
			        	    
			           }
			        	
		        	}
		        	
		        	
		       });
		    
		    frndsevents.setOnClickListener(new View.OnClickListener() 
		    {               
		        @SuppressLint("ResourcedAsColor")
				@Override
		        public void onClick(View arg0) 
		        { 	
		        	if(Myid==null){
		        		
		        		Toast.makeText(getActivity().getApplicationContext(), "Please create an event", Toast.LENGTH_SHORT).show();
			        	
		        	}else{
		        		Log.i("hostLayout_f","inside my friends events else myid" +Myid);
		        		if(hostLayout_f.getVisibility() == LinearLayout.VISIBLE || hostLayout_e.getVisibility() == LinearLayout.VISIBLE){
		 	        		
			        		hostLayout_f.setVisibility(View.GONE); 
			        		hostLayout_e.setVisibility(View.GONE);
			        		if(hostLayout_frnds!=null)
			        		hostLayout_frnds.setVisibility(View.VISIBLE);
			        	}	        		 
			        	  
			        	frndsevents.setBackgroundResource(R.drawable.btn_greeen_pressed); 
			        	frndsevents.setTextColor(R.color.btn_green);
			        	fixtures.setBackgroundResource(R.drawable.btn_green); 
			        	fixtures.setTextColor(Color.WHITE);
			        	myevents.setBackgroundResource(R.drawable.btn_green); 
			        	myevents.setTextColor(Color.WHITE);
		        	}
		        			        	    
		           }
		       });
		        
			 
	/*	}else{ 
			
			String keys_id = bundle.getString("keys");
			Log.e("Inside onActivityCreated", "Content Exists in savedInstanceState");
			Log.e("Keys...", keys_id);
			
			String[] keys_ids = keys_id.split("\\*");
			
			Set<String> s = bundle.keySet();
			
			for (String myVal : s) {
		        //System.out.println(myVal);
		    }			
			
			for (String myVal : s) {
							
				System.out.println(myVal);
				if(myVal.trim().startsWith("Mat")){  
					
					arr_fixtures = bundle.getParcelableArrayList(myVal.trim()); 
					Log.e("Length", ""+arr_fixtures.size());
					
					Parselable_Fixture a = arr_fixtures.get(0);
					Log.e("Item - ID ", ""+ a.getId());
					Log.e("Item - HomeTeam ", ""+ a.getHomeTeam());
					Log.e("Item - AwayTeam", ""+ a.getAwayTeam());
					Log.e("Item - StartTime", ""+ a.getStartTime());
					Log.e("Item - EventDate ", ""+ a.getEventDate());
					Log.e("Item - Venue", ""+ a.getVenue());
					
				    ArrayList<String> details = new ArrayList<String>();
				    details.add(a.getId());
				    details.add(a.getHomeTeam());
				    details.add(a.getAwayTeam());
				    details.add(a.getEventDate());
				    details.add(a.getStartTime());				    
				    details.add(a.getVenue());
				    
				    hash_fixture.put(a.getId(), details);
				    Log.i("Size..1..",hash_fixture.size()+"");
			   } 
			}
			
				Log.i("Hash fixture size",""+hash_fixture.size());
				ArrayList<Date> a2 = new ArrayList<Date>();
		        
    			DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    				
    			String sec = "00";
    			Enumeration<String> enum_key = hash_fixture.keys();
    			for(int p = 0; enum_key.hasMoreElements(); p++){	    				
    				try
    				{
    					String key = enum_key.nextElement();
    					ArrayList<String> detail = hash_fixture.get(key);
    					String event_Date = detail.get(3);
						String a1[] = event_Date.split("\\,"); 
						String day = a1[1];
						Date today = df.parse(day+" "+detail.get(4)+":"+sec);
						a2.add(today);
						hash_fixture_date_id.put(today,detail.get(0)); 
						
					}catch (Exception e){
						e.printStackTrace();
					}
    				
    			}
    			Collections.sort(a2, compare);
    			Log.i("Hash fixture date size",""+hash_fixture_date_id.size());
    			ViewAllFixtures(a2,hash_fixture,hash_fixture_date_id); 
    			  
		}*/
		 
		return rootView;
	}
	
	@SuppressLint("NewApi")
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		
		/*if(savedInstanceState != null){
			Log.e("Inside onViewStateRestored", "Content Exists in savedInstanceState");
		}else{
			Log.e("Inside onViewStateRestored", "Content not Exists in savedInstanceState");
		}*/
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) { 		
		super.onActivityCreated(savedInstanceState);  
		
		/*Test t = new Test();
		Bundle bundle = t.getBundle();
		hostLayout_f = t.getHost_F();
		hostLayout_e = t.getHost_E();
		
		if(hash_fixture.size() > 0){
			
			hash_fixture.clear();
			hash_fixture_date_id.clear();
			
		} 
		
		if(bundle != null){
			 			
			String keys_id = bundle.getString("keys");
			Log.e("Inside onActivityCreated", "Content Exists in savedInstanceState");
			Log.e("Keys...", keys_id);
			
			String[] keys_ids = keys_id.split("\\*");
			
			Set<String> s = bundle.keySet();
			
			for (String myVal : s) {
		        //System.out.println(myVal);
		    }			
			
			for (String myVal : s) {
							
				System.out.println(myVal);
				if(myVal.trim().startsWith("Mat")){  
					
					arr_fixtures = bundle.getParcelableArrayList(myVal.trim()); 
					Log.e("Length", ""+arr_fixtures.size());
					
					Parselable_Fixture a = arr_fixtures.get(0);
					Log.e("Item - ID ", ""+ a.getId());
					Log.e("Item - HomeTeam ", ""+ a.getHomeTeam());
					Log.e("Item - AwayTeam", ""+ a.getAwayTeam());
					Log.e("Item - StartTime", ""+ a.getStartTime());
					Log.e("Item - EventDate ", ""+ a.getEventDate());
					Log.e("Item - Venue", ""+ a.getVenue());
					
				    ArrayList<String> details = new ArrayList<String>();
				    details.add(a.getId());
				    details.add(a.getHomeTeam());
				    details.add(a.getAwayTeam());
				    details.add(a.getEventDate());
				    details.add(a.getStartTime());				    
				    details.add(a.getVenue());
				    
				    hash_fixture.put(a.getId(), details);
				    Log.i("Size..1..",hash_fixture.size()+"");
			   } 
			}
			
				Log.i("Hash fixture size",""+hash_fixture.size());
				ArrayList<Date> a2 = new ArrayList<Date>();
		        
    			DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    				
    			String sec = "00";
    			Enumeration<String> enum_key = hash_fixture.keys();
    			for(int p = 0; enum_key.hasMoreElements(); p++){	    				
    				try
    				{
    					String key = enum_key.nextElement();
    					ArrayList<String> detail = hash_fixture.get(key);
    					String event_Date = detail.get(3);
						String a1[] = event_Date.split("\\,"); 
						String day = a1[1];
						Date today = df.parse(day+" "+detail.get(4)+":"+sec);
						a2.add(today);
						hash_fixture_date_id.put(today,detail.get(0)); 
						
					}catch (Exception e){
						e.printStackTrace();
					}
    				
    			}
    			Collections.sort(a2, compare);
    			Log.i("Hash fixture date size",""+hash_fixture_date_id.size());
    			ViewAllFixtures(a2,hash_fixture,hash_fixture_date_id); 
			
		}else{
			Log.e("Inside onActivityCreated", "Content not Exists in savedInstanceState");
		}*/
	}

	public class VIEW_ALL_FIXTURE extends AsyncTask<String, String, String> {	 
		 
		String result = "";
		@Override
	    protected void onPreExecute() {	 
			super.onPreExecute();   
			Log.e("My Call", "&&&&&&&&&&&&&&&&&&&&&  3");
			dFragment.show(fm, ""); 
			
	    } 
		
		@Override	    
		protected String doInBackground(String... params) { 

			Event_ref = new Firebase(FIREBASE_URL).child("country/india/sports/football/leagues/isl2014/fixture");
			Event_ref.addValueEventListener(new ValueEventListener() {
				
				@Override
				public void onDataChange(DataSnapshot snapshot) {
					
					try { 
						
						for (DataSnapshot snapshot1 : snapshot.getChildren()) {
							
							ArrayList<String> details = new ArrayList<String>();
							
							ArrayList<Parselable_Fixture> details_F = new ArrayList<Parselable_Fixture>();
							fixtureUrl = "country/"+ countryId +"/sports/"+ sportsId +"/leagues/"+leagueId+"/fixture/"+snapshot1.child("id").getValue(String.class);
							Parselable_Fixture P_F = new Parselable_Fixture(fixtureUrl,
							snapshot1.child("homeTeam").getValue(String.class), snapshot1.child("awayTeam").getValue(String.class),
							snapshot1.child("eventDate").getValue(String.class), snapshot1.child("startTime").getValue(String.class),
							snapshot1.child("venue").getValue(String.class));
							
							details.add(fixtureUrl);
	    			    	details.add(snapshot1.child("homeTeam").getValue(String.class));
	    			    	details.add(snapshot1.child("awayTeam").getValue(String.class));
	    			    	details.add(snapshot1.child("eventDate").getValue(String.class));
	    			    	details.add(snapshot1.child("startTime").getValue(String.class));
	    			    	details.add(snapshot1.child("venue").getValue(String.class));
	    			    	details.add(snapshot1.child("startflag").getValue(String.class));
	    			    	details.add(snapshot1.child("endflag").getValue(String.class));
	    			    	
	    			    	
	    			    	if(snapshot1.child("startflag").getValue(String.class).equals("1")){
	    			    		
	    			    		details.add(snapshot1.child("result/awayTeamHalfTime").getValue(String.class));
	    			    		details.add(snapshot1.child("result/homeTeamHalfTime").getValue(String.class));
	    			    		details.add(snapshot1.child("result/awayTeamFulltime").getValue(String.class));
	    			    		details.add(snapshot1.child("result/homeTeamFullTime").getValue(String.class));
	    			    		details.add(snapshot1.child("result/awayTeamShortOnTarget").getValue(String.class));
	    			    		details.add(snapshot1.child("result/homeTeamShortOnTarget").getValue(String.class));
	    			    		details.add(snapshot1.child("result/awayTeampossession").getValue(String.class));
	    			    		details.add(snapshot1.child("result/homeTeampossession").getValue(String.class));
	    			    	}
	    			    		
	    			    	details_F.add(P_F); 
				    			
				    		hash_event_details.put(fixtureUrl, details);
				    		
				    		ArrayList <Parselable_Fixture> values_fixture = new ArrayList <Parselable_Fixture>();
				    		String key_id = fixtureUrl;
				    		for (int i = 0; i < details_F.size(); i++)
				    			values_fixture.add(details_F.get(i)); 
				    		
				    		array_list_FV.put(key_id, values_fixture);
				    		
			    		  	} 
							
			    		    a = new ArrayList<Date>();
			    		        
			    			DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			    				
			    			String sec = "00";
			    			for(DataSnapshot snapshot1 : snapshot.getChildren()){		    				
			    				try
			    				{   String fixtureUrl1 = "country/"+ countryId +"/sports/"+ sportsId +"/leagues/"+leagueId+"/fixture/"+snapshot1.child("id").getValue(String.class);
			    					String event_Date = snapshot1.child("eventDate").getValue(String.class);
									String a1[] = event_Date.split("\\,"); 
									String day = a1[1];
									Date today = df.parse(day+" "+snapshot1.child("startTime").getValue(String.class)+":"+sec);
									a.add(today);
									hash_event_date_id.put(today,fixtureUrl1); 
									
		    					}catch (Exception e){
		    						e.printStackTrace();
		    					}
			    				
			    			}
			    			Collections.sort(a, compare);
			    			dFragment.dismiss();
			    			ViewAllFixtures(a,hash_event_details,hash_event_date_id);
		    			} catch (Exception e) { 
							e.printStackTrace();
					}	                
		          
					
				}
				
				@Override
				public void onCancelled() {
					// TODO Auto-generated method stub
					
				}
			});
			
			return result; 
		}
	 
		protected void onPostExecute(String result) { 
			
			new VIEW_ALL_EVENTS().execute();
			new VIEW_ALL_FRNDS_EVENTS().execute();
		}
	}
	
	public class VIEW_ALL_EVENTS extends AsyncTask<String, String, String> {	 
		 
		String result = "";
		@Override
	    protected void onPreExecute() {	 
			super.onPreExecute();   		
			
	    } 
		
		@Override	    
		protected String doInBackground(String... params) {
			 
	        myevents_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/");				 			
			myevents_ref.addListenerForSingleValueEvent(new ValueEventListener() {
				
				@SuppressLint("SimpleDateFormat")
				@Override
				public void onDataChange(DataSnapshot snapshot) {
		    	
		    	 try { 
		    		 if(snapshot != null){
		    		 
		    		 	myEventsDate = new ArrayList<Date>();
	    				DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	    				DateCompare compare = new DateCompare();
	    				String sec = "00";
	    				for(DataSnapshot snapshot1 : snapshot.getChildren()){
					
	    					try
	    						{   ArrayList<String> myDetails = new ArrayList<String>();
		    						myDetails.add(snapshot1.child("ueid").getValue(String.class));
		    						myDetails.add(snapshot1.child("created_by").getValue(String.class));
		    						myDetails.add(snapshot1.child("fixture_id").getValue(String.class));
		    						myDetails.add(snapshot1.child("eventName").getValue(String.class));
		    						myDetails.add(snapshot1.child("startTime").getValue(String.class));
		    						myDetails.add(snapshot1.child("_match_Date").getValue(String.class)); 
		    						myDetails.add(snapshot1.child("_match_time").getValue(String.class)); 
		    						
						    		hash_myevent_details.put(snapshot1.child("ueid").getValue(String.class), myDetails);	 
									String event_Date = snapshot1.child("_match_Date").getValue(String.class);
									String a1[] = event_Date.split("\\,"); 
									String day = a1[1];
									Date today = df.parse(day+" "+snapshot1.child("_match_time").getValue(String.class)+":"+sec);
									myEventsDate.add(today); 
									hash_myevent_date_id.put(today,snapshot1.child("ueid").getValue(String.class));
						
		    					} catch (Exception e){
		    						e.printStackTrace();
		    				    }
					
	    				}
	    			   Collections.sort(myEventsDate, compare); 
	    			   viewMyEvents(); 
		    		 
		    		 
		    	 		}else{
		    	 			Toast.makeText(getActivity(), "No Myevents", Toast.LENGTH_SHORT).show();
		    	 		}
		    		   
		    	 	}catch (Exception e) {
						
						e.printStackTrace();
		    	 	} 
				}
		   
				@Override
				public void onCancelled() {
			
				}
			});
			return result;
    	}
	 
		protected void onPostExecute(String result) { 
			
		}
	}
	
	public class VIEW_ALL_FRNDS_EVENTS extends AsyncTask<String, String, String> {	 
		 
		String result = "";
		@Override
	    protected void onPreExecute() {	 
			super.onPreExecute();   		
			
	    } 
		
		@Override	    
		protected String doInBackground(String... params) {
					
			frndsevents_ref = new Firebase(FIREBASE_URL).child("User/"+Myid+"/FriendsEvents");				 			
			frndsevents_ref.addListenerForSingleValueEvent(new ValueEventListener() {
				
				@SuppressLint("SimpleDateFormat")
				@Override
				public void onDataChange(DataSnapshot snapshot) {
						
	    		 	
		    	if(snapshot!= null){
		    	 try {	    		 
		    		 	 count = snapshot.getChildrenCount();
		    		 	 Log.i("Cont","initial"+count);
		    			 for(DataSnapshot snapshot1 :snapshot.getChildren()){
		    				  
		    				    
		    				    Log.i("Cont","initial after"+count);
				    		   String myfriendurls = snapshot1.getValue().toString();				    		   
				    		   
				    		   if(myfriendurls == null ||myfriendurls.equals("")){
				    			 
				    		   }else{

				    					myeventsFriendsEvents_ref = new Firebase(FIREBASE_URL).child(myfriendurls);
				    					myeventsFriendsEvents_ref.addListenerForSingleValueEvent(new ValueEventListener() {
				    						
					    		 		@SuppressLint("SimpleDateFormat")
					    		 		@Override
					    		 		public void onDataChange(DataSnapshot snapshot12) {
					    		 			count--;
					    		 			// TODO Auto-generated method stub

					    		 			
									 			try {		
									 				  if(snapshot12 != null){
									 					  	ArrayList<String> myDetails1 = new ArrayList<String>();
									   						myDetails1.add(snapshot12.child("ueid").getValue().toString());
									   						myDetails1.add(snapshot12.child("created_by").getValue().toString());
									   						myDetails1.add(snapshot12.child("fixture_id").getValue().toString());
									   						myDetails1.add(snapshot12.child("eventName").getValue().toString());
									   						myDetails1.add(snapshot12.child("_match_time").getValue().toString());
									   						myDetails1.add(snapshot12.child("_match_Date").getValue().toString());
									   						
									   						
									   						hash_myFrndEvent_details.put(snapshot12.child("ueid").getValue(String.class), myDetails1);
												    		
												    		
												    		DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
															String event_Date = snapshot12.child("_match_Date").getValue(String.class);											
															String a1[] = event_Date.split("\\,"); 
															String day = a1[1]; 
															Date today = df.parse(day+" "+snapshot12.child("_match_time").getValue(String.class)+":00");
															myFrndsEventsDate.add(today);
															hash_myFrndEvent_date_id.put(today,snapshot12.child("ueid").getValue(String.class));
															
															
																				
							    		 				
									 				  }
									 				 if(count == 0){
														 Log.i("Cont","inside if   "+count);
											    		   if(hash_myFrndEvent_details.size()!=0){		    			   
												    			 Collections.sort(myEventsDate, compare12); 
												    			 Log.i("myFriends EventsDate",""+myEventsDate.toString());
												    			 viewMyFrndsEvents();	
												    		 }	
											    		   }else{
											    			   Log.i("count value"," in else "+count);
											    		   }
						    		 				} catch (Exception e) {
					    		 					// TODO Auto-generated catch block
					    		 					e.printStackTrace();
					    		 				}
									 			
									 
								}
								
								@Override
								public void onCancelled() {
									// TODO Auto-generated method stub
									
								}
								  
							  });
				    			 
				    		 
				    		 }
				    		  
				    		  
				    	 }
		    			 
		    		   
				 }catch (Exception e) {
						
						e.printStackTrace();
		    	 	} 
				}else{
					Toast.makeText(getActivity(), "No Friends Events", Toast.LENGTH_SHORT).show();
				}
				}
				@Override
				public void onCancelled() {
			
				}
			
			});
			return result;
    	}
	 
		protected void onPostExecute(String result) { 
			
		}
	}
	
	protected void viewMyEvents(){ 
				
		hostLayout_e = (LinearLayout)getActivity().findViewById(R.id.event_layout);   
		
		eventLayout = new LinearLayout(getActivity().getApplicationContext());
		eventLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		eventLayout.setOrientation(LinearLayout.VERTICAL); 
		
		if(hash_myevent_details.size() != 0){
		
	 	for (int j = 0; j < hash_myevent_details.size(); j++) {
	 		  
	 		  String id =	hash_myevent_date_id.get(myEventsDate.get(j));
			  ArrayList<String> myDetails_ = hash_myevent_details.get(id);
			  SimpleDateFormat  dateformat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss'Z'");  
	          Date cur_date = new Date();	
		
	          if(cur_date.before(myEventsDate.get(j))){
	          	
	        	    myEvent_id = myDetails_.get(0);
	  			    myCreated_by = myDetails_.get(1);
	  			    myFixture_id = myDetails_.get(2);	  			    
	  			    final String myEventName = myDetails_.get(3);
	  			    
	  			    Log.i("mydetails", myDetails_.toString());
	  						
					Event_ref = new Firebase(FIREBASE_URL).child(myFixture_id);			    	
					Event_ref.addListenerForSingleValueEvent(new ValueEventListener() {
						@SuppressLint("ResourceAsColor")
						@Override
						public void onDataChange(DataSnapshot snapshot1) { 
			    	
			    			final String myFixtureId1 = snapshot1.child("id").getValue().toString();
			    			String myHomeTeam = snapshot1.child("homeTeam").getValue().toString();
			    			String myAwayTeam = snapshot1.child("awayTeam").getValue().toString();			    			
			    			String myStartTime = snapshot1.child("startTime").getValue().toString();
			    			final String myeventDate = snapshot1.child("eventDate").getValue().toString();
			    			String myVenue1 = snapshot1.child("venue").getValue().toString();
			    			final String myResultFlag = snapshot1.child("startflag").getValue().toString();
	  			
			    			String myTeamAScore="0";
							String myTeamBScore="0";
							
							
							if(snapshot1.child("startflag").getValue(String.class).equals("1")){
								 
								if(snapshot1.child("result/awayTeamFulltime").getValue()!= null){
									myTeamAScore = snapshot1.child("result/awayTeamFulltime").getValue().toString();
									myTeamBScore =  snapshot1.child("result/homeTeamFullTime").getValue().toString();
								}else{
									 
									myTeamAScore = snapshot1.child("result/awayTeamHalfTime").getValue().toString();
									myTeamBScore =  snapshot1.child("result/homeTeamHalfTime").getValue().toString();
								}
								
							}
			    			
			    			LinearLayout myHead = new LinearLayout(getActivity());
			    			myHead.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
			    			myHead.setOrientation(LinearLayout.HORIZONTAL);
			    			myHead.setBackgroundColor(R.color.green_new);
			    			
					        final TextView myTvEventname = new TextView(getActivity());					       
					        myTvEventname.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.6f));
					        myTvEventname.setTextColor(Color.parseColor("#FAFAFA"));
					        myTvEventname.setTextSize(15);	     		    				     
					        myTvEventname.setGravity(Gravity.CENTER_VERTICAL);
					        myTvEventname.setText(myEventName);
					      
					        final TextView myTvEventCountdown = new TextView(getActivity());					       
					        myTvEventCountdown.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.6f));
					        myTvEventCountdown.setTextColor(Color.parseColor("#FAFAFA"));
					        myTvEventCountdown.setTextSize(11);	     		    				     
					        myTvEventCountdown.setGravity(Gravity.CENTER_VERTICAL);
					        
					        String oldFormat = "dd-MM-yyyy HH:mm:ss";
					        SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
					        TimeZone obj = TimeZone.getTimeZone("CST");
					        sdf1.setTimeZone(obj); 
					        Date date1;
					        String dd[] = myeventDate.split("\\,"); 
					        long  millis = 00;
								try {
									 date1 = sdf1.parse(dd[1]+" "+ myStartTime+":00");
									 millis = date1.getTime();
								} catch (ParseException e) {									
										e.printStackTrace();
								}
								
					        long currentTimeInMili = new Date().getTime(); // Current time
					        final CountDownTimer Counter1 = new CountDownTimer(millis - currentTimeInMili, 1 * 1000) {
						       
					        	public void onTick(long millisUntilFinished) {
						            	  myTvEventCountdown.setText("" + formatTime(millisUntilFinished) +"");
						        }

						        public void onFinish() {
					        	myTvEventCountdown.setText("Finished!");
					            }
					         }.start();

					      
					        
				        	final TextView myTv_name = new TextView(getActivity());
				        	myTv_name.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.4f));
				        	myTv_name.setTextColor(Color.parseColor("#FAFAFA"));
				        	myTv_name.setTextSize(12);
				        	String event_Date = myeventDate;
				        	String date = getDate(event_Date); 
				        	myTv_name.setGravity(Gravity.RIGHT);
				        	myTv_name.setText(date);
				        	
				        	myHead.addView(myTvEventname);
				        	myHead.addView(myTvEventCountdown);
				        	myHead.addView(myTv_name);
				        	
					        RelativeLayout myOuter = new RelativeLayout(getActivity());
					        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
					        RelativeLayout.LayoutParams.MATCH_PARENT,100); 
					        myOuter.setLayoutParams(rlp);
					        myOuter.setClickable(true);
					        myOuter.setBackgroundResource(R.drawable.gradient1);
			        
					        hash_myEvent_click.put(myOuter,myFixture_id);
			        
					        ImageView football = new ImageView(getActivity());
			    	
					        LinearLayout layout_time = new LinearLayout(getActivity());
					    	layout_time.setOrientation(LinearLayout.HORIZONTAL);
					    	layout_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
					    	
					    	RelativeLayout.LayoutParams layout_timeParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					    	layout_timeParams.addRule(RelativeLayout.ALIGN_LEFT);
					    	layout_timeParams.addRule(RelativeLayout.CENTER_VERTICAL);
					    	layout_timeParams.setMargins(10, 0, 0, 0);
					    	
					    		    				    	
					        final TextView myTv_time = new TextView(getActivity());
					        myTv_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					        myTv_time.setGravity(Gravity.CENTER_VERTICAL);
					        myTv_time.setId(1);
					        myTv_time.setTextColor(Color.parseColor("#424242"));
					        myTv_time.setTextSize(12); 
					        myTv_time.setPadding(10, 0, 0, 0);
					        myTv_time.setText(myStartTime); 
		    		
				    		layout_time.addView(myTv_time);
				    		
				    		LinearLayout layout_teamA = new LinearLayout(getActivity());
				    		layout_teamA.setOrientation(LinearLayout.HORIZONTAL);
				    		layout_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
	                      
				    		RelativeLayout.LayoutParams layout_teamAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	                        layout_teamAParams.addRule(RelativeLayout.RIGHT_OF,myTv_time.getId());
	                        layout_teamAParams.addRule(RelativeLayout.CENTER_VERTICAL);
	                        layout_teamAParams.setMargins(50, 0, 0, 0);  
		    		 
				    		final TextView myTv_teamA = new TextView(getActivity());
				    		myTv_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				    		myTv_teamA.setGravity(Gravity.LEFT);
				    		myTv_teamA.setId(2);
				    		myTv_teamA.setPadding(30, 0, 0, 0);
				    		myTv_teamA.setTextColor(Color.parseColor("#424242"));
				    		myTv_teamA.setTextSize(12);
				    		String teamA_ID =myHomeTeam;
				    				 
				    		myTv_teamA.setText(teamA_ID); 
		    		 
				    		layout_teamA.addView(myTv_teamA);
				    		
				    		LinearLayout layout_scoreA = new LinearLayout(getActivity());
				    		layout_scoreA.setOrientation(LinearLayout.HORIZONTAL);
				    		layout_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
				    		
				    		RelativeLayout.LayoutParams layout_scoreAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				    		layout_scoreAParams.addRule(RelativeLayout.RIGHT_OF,layout_teamA.getId());
				    		layout_scoreAParams.addRule(RelativeLayout.CENTER_VERTICAL);
				    		layout_scoreAParams.setMargins(180, 0, 0, 0);
				    		
				    		TextView myTv_scoreA = new TextView(getActivity());
				    		myTv_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
				    		myTv_scoreA.setGravity(Gravity.RIGHT);
				    		myTv_scoreA.setPadding(20, 0, 5, 0); 
				    		myTv_scoreA.setTextColor(Color.parseColor("#424242"));
				    		myTv_scoreA.setTextSize(12); 
		                    //String teamB_ID = details_.get(2);
				    		myTv_scoreA.setText(myTeamAScore); 
		                    
		                    layout_scoreA.addView(myTv_scoreA); 
				    		
				    		RelativeLayout.LayoutParams param_football = new RelativeLayout.LayoutParams(20,20); 
				        	param_football.addRule(RelativeLayout.CENTER_VERTICAL | RelativeLayout.CENTER_HORIZONTAL);
				        	param_football.addRule(RelativeLayout.CENTER_IN_PARENT);
				        	football.setId(3);
				        	football.setLayoutParams(param_football);
				        	football.setImageResource(R.drawable.football);
				        	
				        	Animation myRotation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotator);
				        	football.startAnimation(myRotation); 
				        	
				        	LinearLayout layout_teamB = new LinearLayout(getActivity());
				        	layout_teamB.setOrientation(LinearLayout.HORIZONTAL);
				        	layout_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
				        	
				        	RelativeLayout.LayoutParams layout_teamBParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				        	layout_teamBParams.addRule(RelativeLayout.RIGHT_OF,football.getId());
				        	layout_teamBParams.addRule(RelativeLayout.CENTER_VERTICAL); 
				    		 
				    		final TextView myTv_teamB = new TextView(getActivity());
				    		myTv_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				    		myTv_teamB.setGravity(Gravity.LEFT);
				    		myTv_teamB.setId(2);
				    		myTv_teamB.setPadding(20, 0, 0, 0);
				    		myTv_teamB.setTextColor(Color.parseColor("#424242"));
				    		myTv_teamB.setTextSize(12);
				    		String teamB_ID = myAwayTeam;
				    		myTv_teamB.setText(teamB_ID); 
				    		
				    		TextView tv_scoreB = new TextView(getActivity());
				    		tv_scoreB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
				    		tv_scoreB.setGravity(Gravity.RIGHT);
				    		tv_scoreB.setPadding(20, 0, 5, 0); 
				    		tv_scoreB.setTextColor(Color.parseColor("#424242"));
				    		tv_scoreB.setTextSize(12); 
		                    //String teamB_ID = details_.get(2);
				    		tv_scoreB.setText(myTeamBScore); 
				    		
				    		layout_teamB.addView(tv_scoreB);
				    		layout_teamB.addView(myTv_teamB); 
				    		
				    		ImageView next = new ImageView(getActivity()); 
				    		RelativeLayout.LayoutParams params_next = new RelativeLayout.LayoutParams(30,30); 
				        	params_next.addRule(RelativeLayout.CENTER_VERTICAL);
				        	params_next.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				        	next.setLayoutParams(params_next);
				        	next.setImageResource(R.drawable.ic_action_next_item);
				        	next.setPadding(0, 0, 5, 0);
				        	
				           final TextView myTv_venue = new TextView(getActivity());
	                       String venue =myVenue1;
	                       myTv_venue.setText(venue); 
	                       
	                       TextView tv_date = new TextView(getActivity()); 
	                       
	                       date = getDate(myeventDate); 
	                       tv_date.setText(date); 
				    	
		    			     myOuter.addView(layout_time,layout_timeParams);
		    			     myOuter.addView(layout_teamA,layout_teamAParams);
		    			     myOuter.addView(layout_scoreA,layout_scoreAParams);
		    			     myOuter.addView(football);
		    			     myOuter.addView(layout_teamB,layout_teamBParams);
		    			     myOuter.addView(next);
		    			     
	  			    	 eventLayout.addView(myHead);	
	  			    	 eventLayout.addView(myOuter);
			    
	  			    	myOuter.setOnClickListener(new OnClickListener() {				
	  			    		  @Override
	  			    		  public void onClick(View v) {   
							    				
	  			    			  RelativeLayout out = (RelativeLayout)v;
								  myFixture_id = hash_myEvent_click.get(out);
	  							    
								  Bundle b = new Bundle();
	  							    
	  							    current_event_editor.putString("fixture_id",myFixture_id );
	    			                current_event_editor.putString("Event_id",myEvent_id );
	    			                current_event_editor.putString("created_by",created_by );
	    			                current_event_editor.putString("eventDate_db",myeventDate );
	    			                current_event_editor.putString("homeTeam", myTv_teamA.getText().toString() );
	    			                current_event_editor.putString("awayTeam", myTv_teamB.getText().toString() );
	    			                current_event_editor.putString("eventDate", myTv_name.getText().toString() );
	    			                current_event_editor.putString("venue", myTv_venue.getText().toString() );
	    			                current_event_editor.putString("time", myTv_time.getText().toString() );
	    			                current_event_editor.putString("user_event_name", myTvEventname.getText().toString() );
	    			                current_event_editor.putString("myResultFlag",myResultFlag);
	    						    current_event_editor.commit();			  	    							  
	    							  
	  							    
	  							  
	  							   /* b.putString("Event_id", ueid);
	  							    b.putString("_id", fixture_id);
	  							    b.putString("created_by", created_by);
	  							    b.putString("userID", userID.toString());
	  							    b.putString("eventDate_db", event_Date);
	  							    b.putString("homeTeam", tv_teamA.getText().toString());
	  							    b.putString("awayTeam", tv_teamB.getText().toString());
	  							    b.putString("eventDate", tv_date.getText().toString());
		    						b.putString("venue", tv_venue.getText().toString());
		    						b.putString("time", tv_time.getText().toString());
		    						b.putString("matchname", tv_name.getText().toString())*/;
		    						Intent i = new Intent(getActivity(), Sportz_Event_Details.class);
		    						i.putExtras(b);
		    						startActivity(i);
						  
	  			    		  }
	  			    	 });
				    	 
						}
			    
						@Override
						public void onCancelled() { 
					
						}
					});
			
	          }
	 	}
	         
		} else{
			Toast.makeText(getActivity(), "No Myevents", Toast.LENGTH_SHORT).show();
		}
	    hostLayout_e.addView(eventLayout);
	    hostLayout_e.setVisibility(View.GONE);
	} 		
    
	protected void viewMyFrndsEvents(){ 
		
		hostLayout_frnds = (LinearLayout)getActivity().findViewById(R.id.frndevent_layout);   
		
		FrndsEventLayout = new LinearLayout(getActivity().getApplicationContext());
		FrndsEventLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		FrndsEventLayout.setOrientation(LinearLayout.VERTICAL); 
		if(hash_myFrndEvent_details.size() != 0){
			ArrayList<String> myDetails_= new ArrayList<String> ();
		 	for (int j = 0; j < hash_myFrndEvent_details.size(); j++) {
		 		  
		 		  String id =	hash_myFrndEvent_date_id.get(myFrndsEventsDate.get(j));
				  myDetails_ = hash_myFrndEvent_details.get(id);
				  SimpleDateFormat  dateformat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss'Z'");  
		          Date cur_date = new Date(Calendar.DAY_OF_YEAR, 1, j);	
			
		          if(cur_date.before(myFrndsEventsDate.get(j))){
		          	
		        	Event_id = myDetails_.get(0);
		  			created_by = myDetails_.get(1);
		  			final String fixture_id = myDetails_.get(2);
		  			 final String eventName = myDetails_.get(3);
		  		
		  			 
				
						Event_ref = new Firebase(FIREBASE_URL).child(""+fixture_id);			    	
						Event_ref.addListenerForSingleValueEvent(new ValueEventListener() {
							@SuppressLint("ResourceAsColor")
							@Override
							public void onDataChange(DataSnapshot snapshot1) { 
				    	
				    			String id = snapshot1.child("id").getValue(String.class);
				    			String homeTeam = snapshot1.child("homeTeam").getValue(String.class);
				    			String awayTeam = snapshot1.child("awayTeam").getValue(String.class);
				    			final String eventDate = snapshot1.child("eventDate").getValue(String.class);
				    			String startTime = snapshot1.child("startTime").getValue(String.class);
				    			String venue1 = snapshot1.child("venue").getValue(String.class);  
				    			final String myFriendflagid = snapshot1.child("startflag").getValue().toString();
				    			
				    			String teamAScore="0";
								String teamBScore="0";
								
								
								if(snapshot1.child("startflag").getValue(String.class).equals("1")){
									 
									if(snapshot1.child("result/awayTeamFulltime").getValue()!= null){
										teamAScore = snapshot1.child("result/awayTeamFulltime").getValue().toString();
										teamBScore =  snapshot1.child("result/homeTeamFullTime").getValue().toString();
									}else{
										 
										teamAScore = snapshot1.child("result/awayTeamHalfTime").getValue().toString();
										teamBScore =  snapshot1.child("result/homeTeamHalfTime").getValue().toString();
									}
									
								}
				    			
				    			LinearLayout head = new LinearLayout(getActivity());
						        head.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
						        head.setOrientation(LinearLayout.HORIZONTAL);
						        head.setBackgroundColor(R.color.green_new);
						        final TextView tvEventname = new TextView(getActivity());
						       
						        tvEventname.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.6f));
						        tvEventname.setTextColor(Color.parseColor("#FAFAFA"));
						        tvEventname.setTextSize(15);	     		    				     
						        tvEventname.setGravity(Gravity.CENTER_VERTICAL);
						        tvEventname.setText(eventName);
						        
						        final TextView tvEventCountdown = new TextView(getActivity());					       
						        tvEventCountdown.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.6f));
						        tvEventCountdown.setTextColor(Color.parseColor("#FAFAFA"));
						        tvEventCountdown.setTextSize(11);	     		    				     
						        tvEventCountdown.setGravity(Gravity.CENTER_VERTICAL);
						        
						        String oldFormat = "dd-MM-yyyy HH:mm:ss";
						        SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
						        TimeZone obj = TimeZone.getTimeZone("CST");
						        sdf1.setTimeZone(obj); 
						        Date date1;
						        String dd[] = eventDate.split("\\,"); 
						        long  millis = 00;
									try {
										 date1 = sdf1.parse(dd[1]+" "+ startTime+":00");
										 millis = date1.getTime();
									} catch (ParseException e) {									
											e.printStackTrace();
									}
									
						        long currentTimeInMili = new Date().getTime(); // Current time
						        final CountDownTimer Counter1 = new CountDownTimer(millis - currentTimeInMili, 1 * 1000) {
							       
						        	public void onTick(long millisUntilFinished) {
						        		tvEventCountdown.setText("" + formatTime(millisUntilFinished) +"");
							        }

							        public void onFinish() {
							        	tvEventCountdown.setText("Finished!");
						            }
						         }.start();
						      
					        	final TextView tv_name = new TextView(getActivity());
					        	tv_name.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.4f));
					        	tv_name.setTextColor(Color.parseColor("#FAFAFA"));
					        	tv_name.setTextSize(12);
					        	final String event_Date = eventDate;
					        	String date = getDate(event_Date); 
					        	tv_name.setGravity(Gravity.RIGHT);
					        	tv_name.setText(date);
					        	
						        head.addView(tvEventname);	
						        head.addView(tvEventCountdown);
						        head.addView(tv_name);
				        
						        RelativeLayout outer = new RelativeLayout(getActivity());
						        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
						        RelativeLayout.LayoutParams.MATCH_PARENT,100); 
						        outer.setLayoutParams(rlp);
						        outer.setClickable(true);
						        outer.setBackgroundResource(R.drawable.gradient1);
				        
						        hash_myFrndsEvent_click.put(outer,snapshot1.child("id").getValue(String.class));
				        
						        ImageView football = new ImageView(getActivity());
				    	
						        LinearLayout layout_time = new LinearLayout(getActivity());
						    	layout_time.setOrientation(LinearLayout.HORIZONTAL);
						    	layout_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
						    	
						    	RelativeLayout.LayoutParams layout_timeParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						    	layout_timeParams.addRule(RelativeLayout.ALIGN_LEFT);
						    	layout_timeParams.addRule(RelativeLayout.CENTER_VERTICAL);
						    	layout_timeParams.setMargins(10, 0, 0, 0);
						    	
						    		    				    	
						        final TextView tv_time = new TextView(getActivity());
						        tv_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
						        tv_time.setGravity(Gravity.CENTER_VERTICAL);
								tv_time.setId(1);
					    		tv_time.setTextColor(Color.parseColor("#424242"));
					    		tv_time.setTextSize(12); 
					    		tv_time.setPadding(10, 0, 0, 0);
					    		tv_time.setText(startTime); 
			    		
					    		layout_time.addView(tv_time);
					    		
					    		LinearLayout layout_teamA = new LinearLayout(getActivity());
					    		layout_teamA.setOrientation(LinearLayout.HORIZONTAL);
					    		layout_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
		                      
					    		RelativeLayout.LayoutParams layout_teamAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		                        layout_teamAParams.addRule(RelativeLayout.RIGHT_OF,tv_time.getId());
		                        layout_teamAParams.addRule(RelativeLayout.CENTER_VERTICAL);
		                        layout_teamAParams.setMargins(50, 0, 0, 0);  
			    		 
					    		final TextView tv_teamA = new TextView(getActivity());
					    		tv_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					    		tv_teamA.setGravity(Gravity.LEFT);
					    		tv_teamA.setId(2);
					    		tv_teamA.setPadding(30, 0, 0, 0);
					    		tv_teamA.setTextColor(Color.parseColor("#424242"));
					    		tv_teamA.setTextSize(12);
					    		String teamA_ID =homeTeam;
					    				 
					    		tv_teamA.setText(teamA_ID); 
			    		 
					    		layout_teamA.addView(tv_teamA);
					    		
					    		LinearLayout layout_scoreA = new LinearLayout(getActivity());
					    		layout_scoreA.setOrientation(LinearLayout.HORIZONTAL);
					    		layout_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
					    		
					    		RelativeLayout.LayoutParams layout_scoreAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					    		layout_scoreAParams.addRule(RelativeLayout.RIGHT_OF,layout_teamA.getId());
					    		layout_scoreAParams.addRule(RelativeLayout.CENTER_VERTICAL);
					    		layout_scoreAParams.setMargins(180, 0, 0, 0);
					    		
					    		final TextView tv_scoreA = new TextView(getActivity());
					    		tv_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
			                    tv_scoreA.setGravity(Gravity.RIGHT);
			                    tv_scoreA.setPadding(20, 0, 5, 0); 
			                    tv_scoreA.setTextColor(Color.parseColor("#424242"));
			                    tv_scoreA.setTextSize(12); 
			                    //String teamB_ID = details_.get(2);
			                    tv_scoreA.setText(teamAScore); 
			                    
			                    layout_scoreA.addView(tv_scoreA); 
					    		
					    		RelativeLayout.LayoutParams param_football = new RelativeLayout.LayoutParams(20,20); 
					        	param_football.addRule(RelativeLayout.CENTER_VERTICAL | RelativeLayout.CENTER_HORIZONTAL);
					        	param_football.addRule(RelativeLayout.CENTER_IN_PARENT);
					        	football.setId(3);
					        	football.setLayoutParams(param_football);
					        	football.setImageResource(R.drawable.football);
					        	
					        	final Animation myRotation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotator);
					        	football.startAnimation(myRotation); 
					        	
					        	LinearLayout layout_teamB = new LinearLayout(getActivity());
					        	layout_teamB.setOrientation(LinearLayout.HORIZONTAL);
					        	layout_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
					        	
					        	RelativeLayout.LayoutParams layout_teamBParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					        	layout_teamBParams.addRule(RelativeLayout.RIGHT_OF,football.getId());
					        	layout_teamBParams.addRule(RelativeLayout.CENTER_VERTICAL); 
					    		 
					    		final TextView tv_teamB = new TextView(getActivity());
					    		tv_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					    		tv_teamB.setGravity(Gravity.LEFT);
					    		tv_teamB.setId(2);
					    		tv_teamB.setPadding(20, 0, 0, 0);
					    		tv_teamB.setTextColor(Color.parseColor("#424242"));
					    		tv_teamB.setTextSize(12);
					    		String teamB_ID = awayTeam;
					    		tv_teamB.setText(teamB_ID); 
					    		
					    		final TextView tv_scoreB = new TextView(getActivity());
					    		tv_scoreB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
					    		tv_scoreB.setGravity(Gravity.RIGHT);
					    		tv_scoreB.setPadding(20, 0, 5, 0); 
					    		tv_scoreB.setTextColor(Color.parseColor("#424242"));
					    		tv_scoreB.setTextSize(12); 
			                    //String teamB_ID = details_.get(2);
					    		tv_scoreB.setText(teamBScore); 
					    		
					    		layout_teamB.addView(tv_scoreB);
					    		layout_teamB.addView(tv_teamB); 
					    		
					    		ImageView next = new ImageView(getActivity()); 
					    		RelativeLayout.LayoutParams params_next = new RelativeLayout.LayoutParams(30,30); 
					        	params_next.addRule(RelativeLayout.CENTER_VERTICAL);
					        	params_next.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					        	next.setLayoutParams(params_next);
					        	next.setImageResource(R.drawable.ic_action_next_item);
					        	next.setPadding(0, 0, 5, 0);
					        	
					           final TextView tv_venue = new TextView(getActivity());
		                       String venue =venue1;
		                       tv_venue.setText(venue); 
		                       
		                       final TextView tv_date = new TextView(getActivity()); 
		                       
		                       date = getDate(eventDate); 
		                       tv_date.setText(date); 
					    	
			    			     outer.addView(layout_time,layout_timeParams);
			    			     outer.addView(layout_teamA,layout_teamAParams);
			    			     outer.addView(layout_scoreA,layout_scoreAParams);
			    			     outer.addView(football);
			    			     outer.addView(layout_teamB,layout_teamBParams);
			    			     outer.addView(next);
			    			     
			    			     FrndsEventLayout.addView(head);	
			    			     FrndsEventLayout.addView(outer);
				    
		  			    	 outer.setOnClickListener(new OnClickListener() {				
		  			    		  @Override
		  			    		  public void onClick(View v) {   
								    			
		  			    			   RelativeLayout out = (RelativeLayout)v;
									   myFrndsFixture_id = hash_myFrndsEvent_click.get(out); 
									   
		  							    Bundle b = new Bundle();
		  							   
		  							    current_event_editor.putString("fixture_id",myFrndsFixture_id );
		    			                current_event_editor.putString("Event_id",Event_id );
		    			                current_event_editor.putString("created_by",created_by );
		    			                current_event_editor.putString("eventDate_db",event_Date );
		    			                current_event_editor.putString("homeTeam", tv_teamA.getText().toString() );
		    			                current_event_editor.putString("awayTeam", tv_teamB.getText().toString() );
		    			                current_event_editor.putString("eventDate", tv_date.getText().toString() );
		    			                current_event_editor.putString("venue", tv_venue.getText().toString() );
		    			                current_event_editor.putString("time", tv_time.getText().toString() );
		    			                current_event_editor.putString("user_event_name", tvEventname.getText().toString() );
		    			                current_event_editor.putString("myResultFlag", myFriendflagid);
		    						    current_event_editor.commit();			  	    							  
		    							  
		    						    
		  							  
		  							   /* b.putString("Event_id", ueid);
		  							    b.putString("_id", fixture_id);
		  							    b.putString("created_by", created_by);
		  							    b.putString("userID", userID.toString());
		  							    b.putString("eventDate_db", event_Date);
		  							    b.putString("homeTeam", tv_teamA.getText().toString());
		  							    b.putString("awayTeam", tv_teamB.getText().toString());
		  							    b.putString("eventDate", tv_date.getText().toString());
			    						b.putString("venue", tv_venue.getText().toString());
			    						b.putString("time", tv_time.getText().toString());
			    						b.putString("matchname", tv_name.getText().toString())*/;
			    						Intent i = new Intent(getActivity(), Sportz_Event_Details.class);
			    						i.putExtras(b);
			    						startActivity(i);
							  
		  			    		  }
		  			    	 });
					    	 
							}
				    
							@Override
							public void onCancelled() { 
						
							}
						});
				
		          }
		         
			}
		}else{
			Toast.makeText(getActivity(), "No Friends Events", Toast.LENGTH_SHORT).show();
		}
		 
	    hostLayout_frnds.addView(FrndsEventLayout);
	    hostLayout_frnds.setVisibility(View.GONE);
	} 
		
	@SuppressLint("ResourceAsColor") 
	protected void ViewAllFixtures(ArrayList<Date> a,Hashtable<String, ArrayList<String>> hashEventDetails ,Hashtable<Date,String> hashDate) {  
		 
		hostLayout_f = (LinearLayout)getActivity().findViewById(R.id.host_layout);
		if(fixtureLayout != null){
    		fixtureLayout.removeAllViews();
    	} 
		
		fixtureLayout = new LinearLayout(getActivity().getApplicationContext());
		fixtureLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		fixtureLayout.setOrientation(LinearLayout.VERTICAL);
				
		Log.i("Size....",hashEventDetails.size()+"");
    	for(int j = 0; j < hashEventDetails.size(); j++){
			 
			new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss'Z'");  
            Date cur_date = new Date();	
            String id =	hashDate.get(a.get(j));
            Log.i("details id  ", " id   "+hashDate.get(a.get(j)));
		            if(cur_date.before(a.get(j))){		            
		            
					
					ArrayList<String> details_ = hashEventDetails.get(id);	
					Log.i("details   ", ""+details_.toString());
					String teamAfulltimegoal = "0";
		            String teamBfulltimegoal = "0";
		            result_Flag = details_.get(6);		         
					if(result_Flag.equals("1")){
						 Log.i("Size.... of hash", ""+details_.size());
						if(details_.get(10)!=null){
							teamAfulltimegoal = details_.get(10).toString();
							teamBfulltimegoal =  details_.get(11).toString();
						}else{
							 
							 teamAfulltimegoal = details_.get(8).toString();
				             teamBfulltimegoal =  details_.get(9).toString();
						}
					}
					
				 	LinearLayout head = new LinearLayout(getActivity());
			        head.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
			        head.setOrientation(LinearLayout.HORIZONTAL);
			        head.setBackgroundColor(R.color.green_new);
			        
			        	final TextView tv_date = new TextView(getActivity());
			        	tv_date.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			        	tv_date.setTextColor(Color.parseColor("#FAFAFA"));
			        	tv_date.setTextSize(12);
			        	tv_date.setGravity(Gravity.CENTER_VERTICAL);
			        	final String event_Date = (String)details_.get(3);
			        	String date = getDate(event_Date); 
			        	tv_date.setText(date);
			        	
			        	final TextView tv_name = new TextView(getActivity());
				        tv_name.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,.4f));
				        tv_name.setTextColor(Color.parseColor("#FAFAFA"));
				        tv_name.setTextSize(11);				        	
				        tv_name.setGravity(Gravity.RIGHT);
				       
			        	
			        	String oldFormat = "dd-MM-yyyy HH:mm:ss";
				        SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
				        TimeZone obj = TimeZone.getTimeZone("CST");
				        sdf1.setTimeZone(obj); 
				        Date date1;
				        String dd[] = event_Date.split("\\,"); 
				        long  millis = 00;
							try {
								 date1 = sdf1.parse(dd[1]+" "+ (String)details_.get(4) +":00");
								 millis = date1.getTime();
							} catch (ParseException e) {									
									e.printStackTrace();
							}
							
				        long currentTimeInMili = new Date().getTime(); // Current time
				        final CountDownTimer Counter1 = new CountDownTimer(millis - currentTimeInMili, 1 * 1000) {
					       
				        	public void onTick(long millisUntilFinished) {
				        		tv_name.setText("" + formatTime(millisUntilFinished) +"");
					        }

					        public void onFinish() {
					        	tv_name.setText("Finished!");
				            }
				         }.start();
				         
				         
			        	
			        head.addView(tv_date);
			        head.addView(tv_name);
			        
			        RelativeLayout outer = new RelativeLayout(getActivity());
			        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
			        RelativeLayout.LayoutParams.MATCH_PARENT,100); 
			        outer.setLayoutParams(rlp);
			        outer.setClickable(true);
			        outer.setBackgroundResource(R.drawable.gradient1);
			        
			        System.out.println("%%%%%%%%%%%%%"+(String)details_.get(0));
			        hash_fixtureEvent_click.put(outer,(String)details_.get(0));
			        
			        ImageView football = new ImageView(getActivity());
			    	
			        LinearLayout layout_time = new LinearLayout(getActivity());
			    	layout_time.setOrientation(LinearLayout.HORIZONTAL);
			    	layout_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
			    	
			    	RelativeLayout.LayoutParams layout_timeParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			    	layout_timeParams.addRule(RelativeLayout.ALIGN_LEFT);
			    	layout_timeParams.addRule(RelativeLayout.CENTER_VERTICAL);
			    	layout_timeParams.setMargins(10, 0, 0, 0);
			    	
			    		    				    	
			        final TextView tv_time = new TextView(getActivity());
			        tv_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			        tv_time.setGravity(Gravity.CENTER_VERTICAL);
					tv_time.setId(1);
		    		tv_time.setTextColor(Color.parseColor("#424242"));
		    		tv_time.setTextSize(12); 
		    		tv_time.setPadding(10, 0, 0, 0);
		    		tv_time.setText((String)details_.get(4)); 
		    		
		    		layout_time.addView(tv_time);
		    		
		    		LinearLayout layout_teamA = new LinearLayout(getActivity());
		    		layout_teamA.setOrientation(LinearLayout.HORIZONTAL);
		    		layout_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
                    
		    		RelativeLayout.LayoutParams layout_teamAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                    layout_teamAParams.addRule(RelativeLayout.RIGHT_OF,tv_time.getId());
                    layout_teamAParams.addRule(RelativeLayout.CENTER_VERTICAL);
                    layout_teamAParams.setMargins(50, 0, 0, 0);   
		    		 
		    		final TextView tv_teamA = new TextView(getActivity());
		    		tv_teamA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		    		tv_teamA.setGravity(Gravity.LEFT);
		    		tv_teamA.setId(2);
		    		tv_teamA.setPadding(30, 0, 0, 0);
		    		tv_teamA.setTextColor(Color.parseColor("#424242"));
		    		tv_teamA.setTextSize(12);
		    		String teamA_ID = (String) details_.get(1);
		    				 
		    		tv_teamA.setText(teamA_ID); 
		    		 
		    		layout_teamA.addView(tv_teamA);
		    		
		    		LinearLayout layout_scoreA = new LinearLayout(getActivity());
		    		layout_scoreA.setOrientation(LinearLayout.HORIZONTAL);
		    		layout_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
		    		
		    		RelativeLayout.LayoutParams layout_scoreAParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    		layout_scoreAParams.addRule(RelativeLayout.RIGHT_OF,layout_teamA.getId());
		    		layout_scoreAParams.addRule(RelativeLayout.CENTER_VERTICAL);
		    		layout_scoreAParams.setMargins(180, 0, 0, 0);
		    		
		    		final TextView tv_scoreA = new TextView(getActivity());
		    		tv_scoreA.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
                    tv_scoreA.setGravity(Gravity.RIGHT);
                    tv_scoreA.setPadding(20, 0, 5, 0); 
                    tv_scoreA.setTextColor(Color.parseColor("#424242"));
                    tv_scoreA.setTextSize(12); 
                    //String teamB_ID = details_.get(2);
                    tv_scoreA.setText(teamAfulltimegoal);  
                   
                    layout_scoreA.addView(tv_scoreA); 
		    		
		    		RelativeLayout.LayoutParams param_football = new RelativeLayout.LayoutParams(20,20); 
		        	param_football.addRule(RelativeLayout.CENTER_VERTICAL | RelativeLayout.CENTER_HORIZONTAL);
		        	param_football.addRule(RelativeLayout.CENTER_IN_PARENT);
		        	football.setId(3);
		        	football.setLayoutParams(param_football);
		        	football.setImageResource(R.drawable.football);
		        	
		        	final Animation myRotation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotator);
		        	football.startAnimation(myRotation);  
		        	
		        	LinearLayout layout_teamB = new LinearLayout(getActivity());
		        	layout_teamB.setOrientation(LinearLayout.HORIZONTAL);
		        	layout_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 30));
		        	
		        	RelativeLayout.LayoutParams layout_teamBParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		        	layout_teamBParams.addRule(RelativeLayout.RIGHT_OF,football.getId());
		        	layout_teamBParams.addRule(RelativeLayout.CENTER_VERTICAL);
		    		
		    		 
		    		final TextView tv_teamB = new TextView(getActivity());
		    		tv_teamB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		    		tv_teamB.setGravity(Gravity.LEFT);
		    		tv_teamB.setId(2);
		    		tv_teamB.setPadding(20, 0, 0, 0);
		    		tv_teamB.setTextColor(Color.parseColor("#424242"));
		    		tv_teamB.setTextSize(12);
		    		String teamB_ID = (String)details_.get(2);
		    		tv_teamB.setText(teamB_ID); 
		    		
		    		final TextView tv_scoreB = new TextView(getActivity());
		    		tv_scoreB.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));;
		    		tv_scoreB.setGravity(Gravity.RIGHT);
		    		tv_scoreB.setPadding(20, 0, 5, 0); 
		    		tv_scoreB.setTextColor(Color.parseColor("#424242"));
		    		tv_scoreB.setTextSize(12); 
                    //String teamB_ID = details_.get(2);
		    		tv_scoreB.setText(teamBfulltimegoal); 
		    		
		    		layout_teamB.addView(tv_scoreB);
		    		layout_teamB.addView(tv_teamB); 
		    		
		    		ImageView next = new ImageView(getActivity()); 
		    		RelativeLayout.LayoutParams params_next = new RelativeLayout.LayoutParams(30,30); 
		        	params_next.addRule(RelativeLayout.CENTER_VERTICAL);
		        	params_next.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		        	next.setLayoutParams(params_next);
		        	next.setImageResource(R.drawable.ic_action_next_item);
		        	next.setPadding(0, 0, 5, 0);
		        	
		        	final TextView tv_venue = new TextView(getActivity());
		        	String venue = (String)details_.get(5);
		        	tv_venue.setText(venue); 
		    	
		    	outer.addView(layout_time,layout_timeParams);
		    	outer.addView(layout_teamA,layout_teamAParams);
		    	outer.addView(layout_scoreA,layout_scoreAParams);
		    	outer.addView(football);
		    	outer.addView(layout_teamB,layout_teamBParams);
		    	outer.addView(next);
		    	
		    	hash_event_teamIDs.put(outer, teamA_ID+"*"+teamB_ID);
		    	
			    fixtureLayout.addView(head);	
			    fixtureLayout.addView(outer);  
			  
				    outer.setOnClickListener(new OnClickListener() {				
						@Override
						public void onClick(View v) { 
							
							  RelativeLayout out = (RelativeLayout)v;
							  fixture_id = hash_fixtureEvent_click.get(out); 
							  							  
							  
							current_event_editor.putString("fixture_id",fixture_id );
  			                current_event_editor.putString("Event_id",Event_id );
  			                current_event_editor.putString("created_by",created_by );
  			                current_event_editor.putString("eventDate_db",event_Date );
  			                current_event_editor.putString("homeTeam", tv_teamA.getText().toString() );
  			                current_event_editor.putString("awayTeam", tv_teamB.getText().toString() );
  			                current_event_editor.putString("eventDate", tv_date.getText().toString() );
  			                current_event_editor.putString("venue", tv_venue.getText().toString() );
  			                current_event_editor.putString("time", tv_time.getText().toString() );
  			                current_event_editor.putString("resultDetails", result_Flag);
  						    current_event_editor.commit();			  	    		
							  
							  Bundle b = new Bundle();
							  Intent i = new Intent(getActivity(), Sportz_new_user_event.class);
							  i.putExtras(b);
							  startActivity(i);
							  
						}
				});  
			}  
		}
    	if(fixtureLayout == null){
    		Log.i("fixtureLayout", "Null");
    	} 
		
    	hostLayout_f.addView(fixtureLayout);  
	}
	
	private String getDate(String event_Date){
		String date = "";
		String month = "";
		
		String a[] = event_Date.split("\\,"); 
		String day = a[0];
		String date_[] = a[1].split("\\-");
		switch(date_[1]){
		case "01":
			month = "January";
			break;
		case "02":
			month = "February";
			break;
		case "03":
			month = "March";
			break;
		case "04":
			month = "April";
			break;
		case "05":
			month = "May";
			break;
		case "06":
			month = "June";
			break;
		case "07":
			month = "July";
			break;
		case "08":
			month = "August";
			break;
		case "09":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
			
		}	
		
		date = day+" "+date_[0]+" "+month+" "+date_[2]; 
		return date;
	}
	
	class DateCompare implements Comparator<Date> {
		public int compare(Date one, Date two){
		return one.compareTo(two);
		}
	}

	@Override
	public void onPause() { 
		/*flag = false;
		Log.e("Inside   ", "onPause "+ hash_event_details.size());*/
		super.onPause();
	} 

	@Override
	public void onSaveInstanceState(Bundle outState) { 
		super.onSaveInstanceState(outState); 
		
		String keys_id = "";
		Enumeration<String> keys = array_list_FV.keys();
		for(int p = 0; keys.hasMoreElements(); p++){
			
			String key = keys.nextElement();
			keys_id = keys_id + "*" + key;
		} 
		Enumeration<String> keys_2 = array_list_FV.keys();
		for(int i = 0; keys_2.hasMoreElements(); i++){
			String key = keys_2.nextElement();
			outState.putParcelableArrayList(key, array_list_FV.get(key));
			Log.e("Inside   ", "onSavedInstanceState ");	
		}
		outState.putString("keys", keys_id);
		
		if(outState != null)
			Log.e("Inside   ", "onSavedInstanceState ");
		 
		Test t = new Test();
		t.setBundle(outState);
		t.setView_F(hostLayout_f);
		t.setView_E(hostLayout_e);
		t.setView_FR(hostLayout_frnds);
	} 

	@Override
	public void onResume() {
		/*
		if(flag == false){
			flag = true;
			Log.e("Inside   ", "onResume "+ hash_event_details.size());
		}*/
		super.onResume();
	} 
	
	public static String formatTime(long millis) {
		  String output = "00:00";
		  try {
		   long seconds = millis / 1000;
		   long minutes = seconds / 60;
		   long hours = seconds / 3600;
		   long days = seconds / (3600 * 24);

		   seconds = seconds % 60;
		   minutes = minutes % 60;
		   hours = hours % 24;
		   days = days % 30;

		   String sec = String.valueOf(seconds);
		   String min = String.valueOf(minutes);
		   String hur = String.valueOf(hours);
		   String day = String.valueOf(days);

		   if (seconds < 10)
		    sec = "0" + seconds;
		   if (minutes < 10)
		    min = "0" + minutes;
		   if (hours < 10)
		    hur = "0" + hours;
		   if (days < 10)
		    day = "0" + days;

		   output = day + " D  " + hur + " : " + min + " : " + sec;
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return output;
		 } 

	

}
