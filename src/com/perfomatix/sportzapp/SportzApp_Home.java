package com.perfomatix.sportzapp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem; 
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class SportzApp_Home extends Activity {
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle; 
	private CharSequence mDrawerTitle;
 
	private CharSequence mTitle; 
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	
	SharedPreferences My_Details;
	String Myid = "";
	

	@SuppressLint({ "NewApi", "InflateParams" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportzapp_home);

		mTitle = mDrawerTitle = getTitle();
		
		My_Details =  getApplicationContext().getSharedPreferences("My_Details", Context.MODE_PRIVATE);
      
 
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items); 
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		
		View header = getLayoutInflater().inflate(R.layout.listview_header, null); 
		mDrawerList.addHeaderView(header); 

		navDrawerItems = new ArrayList<NavDrawerItem>();
 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1))); 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1))); 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1))); 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1))); 
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navMenuIcons.recycle();  

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener()); 
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter); 

		mDrawerList.setItemChecked(0, true);  
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		Myid = My_Details.getString("Myid",null);
		
		

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, 
				R.string.app_name,  
				R.string.app_name  ) {
			
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle); 
				invalidateOptionsMenu();
			    
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle); 
				invalidateOptionsMenu();				
				}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) { 
			displayView(0);
		} 
		
		
		displayView(1);
		
		
	}
 
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) { 
			if(Myid == null && position == 1)
				view.setVisibility(View.INVISIBLE);
				
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		} 
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
 
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) { 
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
 
	private void displayView(int position) { 
		Fragment fragment = null;
		switch (position) {
		case 1:
			fragment = new Sportz_HomeFragment();
			//startActivity(new Intent(this,Sportz_HomeFragment.class));
			break;
		case 2: 
			startActivity(new Intent(this,Sportz_FB_Tab.class));
			break;
		case 3:
			startActivity(new Intent(this,SportzApp_UserDetails.class));
			//fragment = new Sportz_PointsFragment();
			break;
		case 4:
			startActivity(new Intent(this,Sportz_Notifications.class));
			break; 
		case 5:
			startActivity(new Intent(this,Sports_TellAFriend.class));
			break;
		case 6:
			startActivity(new Intent(this,Sportz_app_used_friends.class));
			
			break;

		default:
			break;
		}

		if(fragment != null) {
			
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();
 
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position - 1]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else { 
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}
 
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState); 
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig); 
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}

