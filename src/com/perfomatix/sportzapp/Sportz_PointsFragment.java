package com.perfomatix.sportzapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Sportz_PointsFragment extends Fragment {
	
	public Sportz_PointsFragment(){}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 			
		View rootView = inflater.inflate(R.layout.sportz_pointsfragment, container, false);
        
		return rootView;
    }

}
