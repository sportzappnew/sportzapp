package com.perfomatix.sportzapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler; 
import android.view.View;
import android.view.Window;


public class Sportzapp_SplashScreen  extends Activity {
 
		
		Handler h=new Handler();
		
	    public void onCreate(Bundle savedInstanceState) 
	    {
	        super.onCreate(savedInstanceState);
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        
	        setContentView(R.layout.sportzapp_splash_screen);
	        
	        h.postDelayed(task, 3000);
	        
	    }
	    Runnable task=new Runnable() 
	    {		
			public void run() 
			{
				Sportzapp_SplashScreen.this.finish();
				Intent i=new Intent(Sportzapp_SplashScreen.this,SportzApp_Home.class);
				startActivity(i);
			}
		};
		 
}
