package com.perfomatix.sportzapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log; 

public class Parselable_Fixture implements Parcelable
{
    private String id;
    private String homeTeam;
    private String awayTeam;
    private String eventDate;
    private String startTime;
    private String venue;
    
    private static String TAG = "** address **";

    public Parselable_Fixture (String a, String b, String c, String d, String e, String f)
    {
        id = a;
        homeTeam = b;
        awayTeam = c;
        eventDate = d;
        startTime = e;
        venue = f;
    }

   public Parselable_Fixture(Parcel in)
   { 
	   id = in.readString ();
       homeTeam = in.readString ();
       awayTeam = in.readString ();
       eventDate = in.readString ();
       startTime = in.readString ();
       venue = in.readString ();
        
   }

    public String getId ()
    {
        Log.d (TAG, "getId()");
        return (id);
    }

    public String getHomeTeam()
    {
        Log.d (TAG, "getHomeTeam()");
        return (homeTeam);
    }
    
    public String getAwayTeam(){
    	
    	 Log.d (TAG, "getAwayTeam()");
         return (awayTeam);
    }
    
    public String getEventDate(){
    	
    	Log.d (TAG, "getEventDate()");
        return (eventDate);
    }
    public String getStartTime(){
    	
    	Log.d (TAG, "getStartTime()");
        return (startTime);
    }
    public String getVenue(){
    	
    	Log.d (TAG, "getVenue()");
        return (venue);
    }

   public static final Parcelable.Creator<Parselable_Fixture> CREATOR = new Parcelable.Creator<Parselable_Fixture>() 
   {
         public Parselable_Fixture createFromParcel(Parcel in) 
         {
            Log.d (TAG, "createFromParcel()");
             return new Parselable_Fixture(in);
         }

         public Parselable_Fixture[] newArray (int size) 
         {
            Log.d (TAG, "createFromParcel() newArray ");
             return new Parselable_Fixture[size];
         }
    };

    @Override
   public int describeContents ()
   {
       Log.d (TAG, "describe()");
       return 0;
   }

	@Override
	public void writeToParcel(Parcel dest, int flags) { 
	 
		dest.writeString (id);
	    dest.writeString (homeTeam);
	    dest.writeString (awayTeam);
	    dest.writeString (eventDate);
	    dest.writeString (startTime);
	    dest.writeString (venue);
		
	}

}