package com.perfomatix.sportzapp;

import android.os.Bundle;
import android.widget.LinearLayout;

public class Test {
	
	static Bundle bundle;
	static LinearLayout host_f;
	static LinearLayout host_e;
	static LinearLayout host_frnds;
	
	public Test(){
		
	}
	
	public void setBundle(Bundle bundle){
		
		this.bundle = bundle;
	}
	public void setView_F(LinearLayout host_f){
		
		this.host_f = host_f;
	}
	public void setView_E(LinearLayout host_e){
	
		this.host_e = host_e;
	}
	public void setView_FR(LinearLayout host_frnds){
		
		this.host_frnds = host_frnds;
	}
	public Bundle getBundle(){
		return this.bundle;
	}
	
	public LinearLayout getHost_F(){
		return this.host_f;
	}
	
	public LinearLayout getHost_E(){
		return this.host_e;
	}
	public LinearLayout getHost_FR(){
		return this.host_frnds;
	}
}
