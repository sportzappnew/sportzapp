
package com.perfomatix.sportzapp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;
 

public class SportzApp_FB_Invite extends Activity {  
	
	private static String APP_ID = "751359634927818";  
	public static Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	
	LinearLayout linear_image_list;
	CheckBox checkBox;
	RelativeLayout rowViewLayout;
	ArrayList<CheckBox> checkbox_List;
	boolean flag_ = true;
	Menu menu;
	
	ImageLoader imageLoader;
	ImageButton btn_invite;
	ImageView football_Image;
	
	Hashtable<CheckBox, String> hash_check_fbname;
	Hashtable<CheckBox, String> hash_check_fburl;
	Hashtable<CheckBox, String> hash_check_fbappuserid;
	
	String username;
	String userid;
	String userpic;
	String Myname,Myid,Mypic;
	String Created_user_id;
	String userID = "483763955060529";
	String _match_date_db;
	String _match_time;
	String fixture_id;
	String Event_id;
	String Event_name;
	String UserEventid;
	String flag="false";
	String inviteflag="";
	String falg="";
	String id;
	String name;
	String pic;
	String result, winner, teamA_half, teamB_half, teamA_full, teamB_full,eventName;
	String teamA_yellowCard, teamB_yellowCard, teamA_RedCard, teamB_RedCard;
	
	
	 public static final int DIALOG_LOADING = 1;
	
	 Button btnClosePopup;
	 Button btnClose;
	 EditText event_Name;
	
	 Firebase invitefriends;
	 Firebase userEvent_ref;
	 Firebase userEvent_id;
	 Firebase challenges;
	 Firebase ateendes;
	 Firebase user_Notifications;
	 Firebase userEvent_id1;
	 Firebase userDetails;
	 Firebase MyDetails;
	 Firebase userDetailsPredictions;
	 Firebase user_Notificationsprediction;
	 Firebase user_Notif_CreatedDetails;
	 
	 UsereventResult  UserEvent;
	 RequestNotification requestnotifictaion;
	 Userevents newevent,newevent_challenge;
	 User userdetails;
	 User Mydetails;
	 
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	
 @SuppressWarnings("deprecation")
@Override
 public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState); 
    setContentView(R.layout.sportz_app_fb_invite); 
    
   /* ActionBar actionBar = getActionBar(); 
    actionBar.setDisplayHomeAsUpEnabled(true);*/
    
   	    	    
    btn_invite = (ImageButton)findViewById(R.id.btn_invite);
    checkbox_List = new ArrayList<CheckBox>();
    UserEvent= new UsereventResult();
    
    hash_check_fbname = new Hashtable<CheckBox, String>();
    hash_check_fburl = new Hashtable<CheckBox, String>();
    hash_check_fbappuserid = new Hashtable<CheckBox, String>();
    
    imageLoader = new ImageLoader(getApplicationContext());
    linear_image_list = (LinearLayout)findViewById(R.id.linear_image_list);
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);  
   
    loginToFacebook_FRIENDS();
    
    current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
	current_event_editor = current_event.edit();
	
	My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
	My_Details_editor = My_Details.edit();
	
	  
    
    Event_id = current_event.getString("Event_id",null); 		
	Created_user_id = current_event.getString("created_by",null); 
	fixture_id = current_event.getString("fixture_id",null); 
	_match_time = current_event.getString("time",null); 
	_match_date_db = current_event.getString("eventDate_db",null); 
	Event_name = current_event.getString("user_event_name",null); 		
	inviteflag = current_event.getString("inviteflag",null); 
    
	Bundle b = this.getIntent().getExtras();
	winner = b.getString("winner");
	teamA_half = b.getString("teamA_half");
	teamB_half = b.getString("teamB_half");
	teamA_full = b.getString("teamA_full");
	teamB_full = b.getString("teamB_full");
	teamA_yellowCard=b.getString("teamA_yellowCard");
	teamB_yellowCard=b.getString("teamB_yellowCard");
	teamA_RedCard=b.getString("teamA_RedCard");
	teamB_RedCard=b.getString("teamB_RedCard");
	
	
    inviteflag = current_event.getString("inviteflag","null"); 
	
    btn_invite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				 
				 if(inviteflag.equals("invitchallengename")){	
						
					 //Create new event from Create event
						newevent_challenge = new Userevents(Myid,fixture_id,_match_date_db,_match_time,Event_name);
						
						userEvent_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/");
						
						 Firebase listRef  = userEvent_ref.push();
					     listRef.setValue(newevent_challenge);

						 listRef.addListenerForSingleValueEvent(new ValueEventListener() {
							    @Override
							    public void onDataChange(DataSnapshot snapshot) {
							    result = snapshot.getName().toString();
							    if(result!=null){
							    	
							    Mydetails = new User(Myid,Mypic,Myname);
							    Challenges predictions = new Challenges(result, Myid, winner, teamA_half, teamB_half, teamA_full, teamB_full,teamA_yellowCard,teamB_yellowCard,teamA_RedCard,teamB_RedCard);
							    requestnotifictaion = new RequestNotification("createChallenge",result,Myid,fixture_id,_match_date_db,_match_time,Event_name);
							    
							    userEvent_id = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/ueid");
							    userEvent_id.setValue(result,null);  
							    
						        challenges = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/attendees/"+Myid+"/predictions");
						        challenges.setValue(predictions);
						        
						        userDetailsPredictions=new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/attendees/"+Myid+"/attendeesDetails");
						        userDetailsPredictions.setValue(Mydetails);  
						          
						          if(hash_check_fbname.size() > 0){
						        	  
										Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
										for(int i = 0; enum_check.hasMoreElements(); i++){
											
											CheckBox cb = enum_check.nextElement();									
											
											 id = hash_check_fbappuserid.get(cb);
										     name = hash_check_fbname.get(cb);
											 pic = hash_check_fburl.get(cb);
											 
											 // notification to friends
											 user_Notifications = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/");											
											 user_Notifications.setValue(requestnotifictaion);
											// notification to friends created by details 
											 user_Notif_CreatedDetails = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/CreatedBy");
											 Mydetails = new User(Myid,Mypic,Myname);
											 user_Notif_CreatedDetails.setValue(Mydetails);
											 
											// notification to friends with createduser prediction
											 user_Notificationsprediction = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/predictions");
											 user_Notificationsprediction.setValue(predictions);
											 
											// invitees details to created user event
											 invitefriends = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/invitees/"+id+"/");
											 userdetails = new User(id,pic,name);
											 invitefriends.setValue(userdetails); 
											 
										}
						    			
						    		}else{
						    			Toast.makeText(getApplicationContext(), "Select Friends to send invitation", Toast.LENGTH_SHORT).show();
						    		}
									
									flag_ = true;
						    		//((ImageButton)v).setImageResource(R.drawable.ic_action_add_person);
						    		
						    		for(int i = 0; i < checkbox_List.size(); i++){
						    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
						    			checkbox_List.get(i).setChecked(false);
						    		}  
						    		hash_check_fbname.clear();
						    		hash_check_fburl.clear();
						    		hash_check_fbappuserid.clear();
						    		
						    	
						    		
						    		Bundle bundle = new Bundle();
						    		Intent intent = new Intent(getApplicationContext(), SportzApp_Home.class);
						    		intent.putExtras(bundle);
						    		startActivity(intent);
						         
							    }
							   
							    }
							    @Override
								public void onCancelled() {
									// TODO Auto-generated method stub
									
								}
							});																				
						 }
				 
				
				if (inviteflag.equals("challenge_edit")){
					
	            	 final Challenges predictions = new Challenges(Event_id, Created_user_id, winner, teamA_half, teamB_half, teamA_full, teamB_full,teamA_yellowCard,teamB_yellowCard,teamA_RedCard,teamB_RedCard);
	            	 challenges = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+Event_id+"/attendees/"+Myid+"/predictions");
	            	 challenges.setValue(predictions);	
	            	 
                        if(hash_check_fbname.size() > 0){
                        	
						Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
						for(int i = 0; enum_check.hasMoreElements(); i++){
							
							CheckBox cb = enum_check.nextElement();
							
						     id = hash_check_fbappuserid.get(cb);
						     name = hash_check_fbname.get(cb);
							 pic = hash_check_fburl.get(cb);
							
							 user_Notifications = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+Event_id+"/predictions");
			 			     user_Notifications.setValue(predictions);
			 			     user_Notifications.setValue(requestnotifictaion);
			 			     
			 			     user_Notif_CreatedDetails = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/CreatedBy");
			 			     user_Notif_CreatedDetails.setValue(Mydetails);
							 
							 invitefriends = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+Event_id+"/invitees/"+id+"/");
							 invitefriends.setValue(userdetails);
							
							
							
							ateendes = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+Event_id+"/attendees/"+Myid+"/predictions");
							ateendes.setValue(predictions);
							
							userDetailsPredictions=new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+Event_id+"/attendees/"+Myid+"/attendeesDetails");
							
							Mydetails = new User(Myid,Mypic,Myname);
						    userDetailsPredictions.setValue(Mydetails); 
								
							Bundle bundle = new Bundle();
							Intent in = new Intent(getApplicationContext(), Sportz_Event_Details.class);				
	    					Bundle b = new Bundle();
	    					in.putExtras(b);
          					startActivity(in);
							
						  }		
						}
	 			     
				
				}
				
				
				  if(inviteflag.equals("invite_edit")){
		
						 newevent = new Userevents(Myid,fixture_id,_match_date_db,_match_time,Event_name);
						 requestnotifictaion = new RequestNotification("editInvite",Event_id,Myid,fixture_id,_match_date_db,_match_time,Event_name);
						
						 if(hash_check_fbname.size() > 0){
								
								Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
								for(int i = 0; enum_check.hasMoreElements(); i++){
									
									CheckBox cb = enum_check.nextElement();
									
								     id = hash_check_fbappuserid.get(cb);
								     name = hash_check_fbname.get(cb);
									 pic = hash_check_fburl.get(cb);
									
									userdetails = new User(id,pic,name);
									
									
									user_Notifications = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+Event_id+"/");
									user_Notifications.setValue(requestnotifictaion);
									
									invitefriends = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+Event_id+"/invitees/"+id+"/");
									invitefriends.setValue(userdetails);
																		
									Toast.makeText(getApplicationContext(), " event created", Toast.LENGTH_SHORT).show();	
									Bundle bundle = new Bundle();
									Intent intent = new Intent(getApplicationContext(), SportzApp_Home.class);
									intent.putExtras(bundle);
									startActivity(intent);
									
								}
				   			
						 }
						 
					 }
				
			else if(inviteflag.equals("invite")|| inviteflag.equals("challeng")){
				initiatePopupWindow();
			}
					
				 
				
			   }
		    public void onCancelled() {
				// TODO Auto-generated method stub
									
				}
				});
    
  
						
			}	 
				
 private PopupWindow pwindo;
 //popup method decleration
	private void initiatePopupWindow() {
	try {
	// We need to get the instance of the LayoutInflater
	LayoutInflater inflater = (LayoutInflater) SportzApp_FB_Invite.this
	.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View layout = inflater.inflate(R.layout.sportzapp_popup,
	(ViewGroup) findViewById(R.id.popup_element));
	pwindo = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
	pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

	btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
	btnClosePopup.setText("Create");
	//btnInvite = (Button) layout.findViewById(R.id.btn_invite);
	//btnChallenge=(Button) layout.findViewById(R.id.btn_challenge);
	btnClose=(Button) layout.findViewById(R.id.btn_close);
	event_Name=(EditText) layout.findViewById(R.id.eventName);
	
	event_Name.addTextChangedListener(new TextWatcher() {
		
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        	checkFieldsForEmptyValues();
        }

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			checkFieldsForEmptyValues();
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start,
				int count, int after) {
			// TODO Auto-generated method stub
			
		}
		public void checkFieldsForEmptyValues(){
			String eventData= event_Name.getText().toString();
        	if(eventData==""){
				btnClosePopup.setEnabled(false);
        	}else
        	  {
        	     btnClosePopup.setEnabled(true);
        	  }
		}

    });
	
	
	 userEvent_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/");
	 
	
	btnClosePopup.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			eventName=event_Name.getText().toString();
			
		/*	if(inviteflag.equals("invite")){ 
				
				     newevent = new Userevents(Myid,fixture_id,_match_date_db,_match_time,eventName);
				     Firebase listRef  = userEvent_ref.push();
			         listRef.setValue(newevent); 
			         
					 listRef.addListenerForSingleValueEvent(new ValueEventListener() {
						    @Override
						    public void onDataChange(DataSnapshot snapshot) {
						    result = snapshot.getName().toString();
						    if(result!=null){
						    userEvent_id = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/ueid");
						    userEvent_id.setValue(result);  
							 
							 if(hash_check_fbname.size() > 0){
									
									Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
									for(int i = 0; enum_check.hasMoreElements(); i++){
										
										 CheckBox cb = enum_check.nextElement(); 
										 id = hash_check_fbappuserid.get(cb);
									     name = hash_check_fbname.get(cb);
										 pic = hash_check_fburl.get(cb);
									    
									    
									 requestnotifictaion = new RequestNotification("createInvite",result,Myid,fixture_id,_match_date_db,_match_time,eventName);	 
									 user_Notifications = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+UserEventid+"/");
									 user_Notifications.setValue(requestnotifictaion);
									 
									 invitefriends = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+UserEventid+"/invitees/"+id+"/"); 
									 userdetails = new User(id,pic,name);
									 invitefriends.setValue(userdetails);
									 
									
										
									}
					    			
					    		}else{
					    			Toast.makeText(getApplicationContext(), "Select Friends to send invitation", Toast.LENGTH_SHORT).show();
					    		}
				
					
					flag_ = true;
		    		//((ImageButton)v).setImageResource(R.drawable.ic_action_add_person);
		    		
		    		for(int i = 0; i < checkbox_List.size(); i++){
		    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
		    			checkbox_List.get(i).setChecked(false);
		    		}  
		    		hash_check_fbname.clear();
		    		hash_check_fburl.clear();
		    		hash_check_fbappuserid.clear();
		    		
		    		Bundle bundle = new Bundle();
		    		Intent intent = new Intent(getApplicationContext(), SportzApp_Home.class);
		    		intent.putExtras(bundle);
		    		startActivity(intent);
																											
						    }
						   
						    }
						    @Override
							public void onCancelled() {
								// TODO Auto-generated method stub
								
							}
						});
					
			}*/
			
			if(inviteflag.equals("challeng")){
				
				 newevent_challenge = new Userevents(Myid,fixture_id,_match_date_db,_match_time,eventName); 
				 userEvent_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/");
				 Firebase listRef  = userEvent_ref.push();
			     listRef.setValue(newevent_challenge);

				 listRef.addListenerForSingleValueEvent(new ValueEventListener() {
					    @Override
					    
					    public void onDataChange(DataSnapshot snapshot) {
					    result = snapshot.getName().toString();
					    
					    if(result!=null){
					       
						    userEvent_id = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/ueid");
						    userEvent_id.setValue(result,null);
						    
						    Mydetails = new User(Myid,Mypic,Myname);
					        requestnotifictaion = new RequestNotification("createChallenge",result,Myid,fixture_id,_match_date_db,_match_time,eventName); 
					        Challenges predictions = new Challenges(result, Myid, winner, teamA_half, teamB_half, teamA_full, teamB_full,teamA_yellowCard,teamB_yellowCard,teamA_RedCard,teamB_RedCard);
					        
					        challenges = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/attendees/"+Myid+"/predictions");
					        challenges.setValue(predictions);
					        
					        userDetailsPredictions=new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/attendees/"+Myid+"/attendeesDetails");
					        userDetailsPredictions.setValue(Mydetails);      
					       
				          if(hash_check_fbname.size() > 0){
				        	  
								Enumeration<CheckBox> enum_check = hash_check_fbname.keys();
								for(int i = 0; enum_check.hasMoreElements(); i++){
									
									CheckBox cb = enum_check.nextElement();									
									
									 id = hash_check_fbappuserid.get(cb);
								     name = hash_check_fbname.get(cb);
									 pic = hash_check_fburl.get(cb);
									 
														
									 user_Notifications = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/");
									 user_Notifications.setValue(requestnotifictaion);
									 
									 user_Notificationsprediction = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/predictions");									
									 user_Notificationsprediction.setValue(predictions);
									 
									 invitefriends = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/invitees/"+id+"/");									
									 invitefriends.setValue(userdetails);
									 
									 user_Notif_CreatedDetails = new Firebase(FIREBASE_URL).child("User/"+id+"/Notifications/"+result+"/CreatedBy");
									 user_Notif_CreatedDetails.setValue(Mydetails);
								 
								}
				    			
				    		}else{
				    			Toast.makeText(getApplicationContext(), "Select Friends to send invitation", Toast.LENGTH_SHORT).show();
				    		}
							
							flag_ = true;
				    		//((ImageButton)v).setImageResource(R.drawable.ic_action_add_person);
				    		
				    		for(int i = 0; i < checkbox_List.size(); i++){
				    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
				    			checkbox_List.get(i).setChecked(false);
				    		}  
				    		hash_check_fbname.clear();
				    		hash_check_fburl.clear();
				    		hash_check_fbappuserid.clear();
				    		
				    		Bundle bundle = new Bundle();
				    		Intent intent = new Intent(getApplicationContext(), SportzApp_Home.class);
				    		intent.putExtras(bundle);
				    		startActivity(intent);
				         
					    }
					   
					    }
					    @Override
						public void onCancelled() {
							// TODO Auto-generated method stub
							
						}
					});																				
				 }
					
			pwindo.dismiss();
		}
	});
	btnClose.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			pwindo.dismiss();
		}
	});
	
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token); 

			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" },
					new DialogListener() { 
						@Override
						public void onCancel() { 
						}

						@Override
						public void onComplete(Bundle values) { 
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token", facebook.getAccessToken());
							editor.putLong("access_expires", facebook.getAccessExpires());
							editor.commit(); 
						} 
						@Override
						public void onError(DialogError error) { 

						} 
						@Override
						public void onFacebookError(FacebookError fberror) { 

						}

					});
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	} 
	
	public void logoutFromFacebook() {
		mAsyncRunner.logout(this, new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Logout from Facebook", response);
				if (Boolean.parseBoolean(response) == true) { 
				}
			} 
			@Override
			public void onIOException(IOException e, Object state) {
			} 
			@Override
			public void onFileNotFoundException(FileNotFoundException e, Object state) {
			} 
			@Override
			public void onMalformedURLException(MalformedURLException e, Object state) {
			} 
			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}
	
	public void loginToFacebook_FRIENDS() {
		
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token); 
			Log.d("FB Sessions", "" + facebook.isSessionValid());  
		       
		     new LoginFb().execute(); 
			 
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		} 
		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" ,"user_photos", "publish_actions", "publish_checkins", "user_friends", "read_friendlists","manage_friendlists"},
					new DialogListener() { 
						@Override
						public void onCancel() { 
						} 
						@Override
						public void onComplete(Bundle values) { 
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token", facebook.getAccessToken());
							editor.commit();   
						     
						    new LoginFb().execute();  
						}

						@Override
						public void onError(DialogError error) { 
						}
						@Override
						public void onFacebookError(FacebookError fberror) { 
						} 
				});
		}
		 
	} 
	
	class LoginFb extends AsyncTask<String, String, String> {
		 @SuppressWarnings("deprecation")
		 @Override
	     protected void onPreExecute() {
	          super.onPreExecute(); 
	          
	          showDialog(DIALOG_LOADING);
	     } 
		 protected String doInBackground(String... f_url) {
			 
			 Bundle params = new Bundle();
		     params.putString("fields", "name, picture");
		    // params.putString("fields", "id");
		     
			 try { 
				 JSONObject jsonme = Util.parseJson(facebook.request("me", params));
		    	 JSONObject jsonFrends = Util.parseJson(facebook.request("me/friends", params));
		    	 Log.d("JSonArray", jsonFrends.toString()); 
		    	 Log.d("jsonme", jsonme.toString()); 
		    	 new GetFriendsList().execute(jsonFrends.toString());
		    	 
		    	 
		    	 
		    	    Myname = jsonme.getString("name");								 			
		 			Myid = jsonme.getString("id");
		 			Mypic =  jsonme.getString("picture");
		 			
		 			Log.i("Myid", Myid);	
		 			
		 			    My_Details_editor.putString("Mypic", Mypic );
			    		My_Details_editor.putString("Myname", Myname);
		                My_Details_editor.putString("Myid",Myid );
		                My_Details_editor.commit();
		                
		                current_event_editor.putString("Mypic", Mypic );
		                current_event_editor.putString("Myname", Myname);
		                current_event_editor.putString("Myid",Myid );
		                current_event_editor.commit();
		                
		                Myid = My_Details.getString("Myid","null");
		            	Log.i("Myid", Myid);
		                
		            MyDetails = new Firebase(FIREBASE_URL).child("User/"+Myid+"/User_profile/");
		 			userdetails = new User(Myid,Mypic,Myname);
		 			MyDetails.setValue(userdetails);
		    	 
		    	 //new CallAPI().execute();
		    	 
			} catch (MalformedURLException e) { 
				e.printStackTrace();
			} catch (JSONException e) { 
				e.printStackTrace();
			} catch (IOException e) { 
				e.printStackTrace();
			} catch (FacebookError e) { 
				e.printStackTrace();
			}  
			 
			 return null;
	     }  
	     
		@SuppressWarnings("deprecation")
		@Override
	     protected void onPostExecute(String file_url) {
	        	 
	    	 dismissDialog(DIALOG_LOADING);
	     }
	}
	 
    class GetFriendsList extends AsyncTask<String, String, String> { 
        @Override
        protected void onPreExecute() {
            super.onPreExecute(); 
        } 
        @Override
        protected String doInBackground(String... f_url) {
        	
        	JSONObject jsonFrends;
			try {
				jsonFrends = new JSONObject(f_url[0]);
				
				JSONArray arrayFriend = jsonFrends.getJSONArray("data"); 
			 	
			 	if (arrayFriend != null) {
			 		
			 		for (int i = 0; i < arrayFriend.length(); i++) {
			 			
			 			final String name = arrayFriend.getJSONObject(i).getString("name");								 			
			 			final String id = arrayFriend.getJSONObject(i).getString("id");
			 			String pic = arrayFriend.getJSONObject(i).getString("picture");
			 			
			 			JSONObject job_user = new JSONObject(pic);
			 			String user_url = job_user.getString("data");
			 			JSONObject job_user_url = new JSONObject(user_url);
			 			final String user_pic_url = job_user_url.getString("url");
			 			
			 			Log.d("*** ",id);
			 			
			 			runOnUiThread(new Runnable() {							
							@SuppressLint("ResourceAsColor")
							@Override
							public void run() {
								
								rowViewLayout = new RelativeLayout(getApplicationContext()); 
								RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
						                RelativeLayout.LayoutParams.MATCH_PARENT,110);
								rowViewLayout.setLayoutParams(rlp);
								rowViewLayout.setClickable(true);
								rowViewLayout.setBackgroundResource(R.drawable.gradient1); 
							 								
									ImageView imageView = new ImageView(getApplicationContext()); 
									RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
									imageView.setLayoutParams(ilp); 
									ilp.addRule(RelativeLayout.CENTER_VERTICAL);
									ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
									
									TextView textView = new TextView(getApplicationContext());
									RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
									RelativeLayout.LayoutParams.WRAP_CONTENT,
							        RelativeLayout.LayoutParams.WRAP_CONTENT);
									tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
									textView.setLayoutParams(tlp);
									textView.setTextColor(Color.parseColor("#424242")); 
									textView.setTextSize(12); 
									textView.setText(name); 
									
									checkBox = new CheckBox(getApplicationContext());
									RelativeLayout.LayoutParams clp = new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.WRAP_CONTENT,
							                RelativeLayout.LayoutParams.WRAP_CONTENT);
									clp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT); 
									clp.addRule(RelativeLayout.CENTER_IN_PARENT);
									checkBox.setLayoutParams(clp);
									checkBox.setVisibility(CheckBox.INVISIBLE);
									checkbox_List.add(checkBox);
									
									LinearLayout linearLayout = new LinearLayout(getApplicationContext());
									RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.MATCH_PARENT, 1);
									llp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
									linearLayout.setBackgroundColor(Color.parseColor("#212121"));
 									linearLayout.setLayoutParams(llp);  
 									
									rowViewLayout.addView(imageView);
									rowViewLayout.addView(textView);
									rowViewLayout.addView(checkBox);
									rowViewLayout.addView(linearLayout);
									
									linear_image_list.addView(rowViewLayout);
					 			 
					 			imageLoader.DisplayImage(user_pic_url, imageView); 
					 			
					 			rowViewLayout.setOnClickListener(new View.OnClickListener() {									
									@Override
									public void onClick(View v) {  
										
										CheckBox cb = (CheckBox)((RelativeLayout)v).getChildAt(2);
										
										if(cb.isChecked() == true){ 
											cb.setChecked(false);
										}else if(cb.isSelected() == false){ 
											cb.setChecked(true);
										}											
									}
								});
					 			
					 			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() { 
									@Override
									public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
										 
										CheckBox chbox = (CheckBox)buttonView;
										if(isChecked == true){
											
											hash_check_fbname.put(chbox, name);
											hash_check_fburl.put(chbox, user_pic_url); 
											hash_check_fbappuserid.put(chbox, id); 
											
											//Toast.makeText(getApplicationContext(), hash_check_fbname.size()+"", Toast.LENGTH_SHORT).show();
											
										}else if(isChecked == false){ 
											
											hash_check_fbname.remove(chbox);
											hash_check_fburl.remove(chbox);
											hash_check_fbappuserid.remove(chbox);
											
											//Toast.makeText(getApplicationContext(), hash_check_fbname.size()+"", Toast.LENGTH_SHORT).show();
											
										}
									}
									
								}); 
					 			for(int j = 0; j < checkbox_List.size(); j++){
					    			checkbox_List.get(j).setVisibility(CheckBox.VISIBLE);
					    			checkbox_List.get(j).setChecked(false);
					    		}
							}
							
						}); 
			 			
			 		}
			 	}
			} catch (JSONException e) { 
				e.printStackTrace();
			} 
        	
            return null;
        }  
        @Override
        protected void onPostExecute(String file_url) {
        	
        	//btn_next.setVisibility(Button.VISIBLE);
        	//football_Image.setAnimation(null);
        }
    }    

	@Override
	public void onBackPressed() { 
		if(flag_ == true){
			startActivity(new Intent(this,SportzApp_Home.class));
		}else if(flag_ == false){ 
			for(int i = 0; i < checkbox_List.size(); i++){
    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
    		}
			MenuItem item_done = menu.findItem(R.id.action_send);
		    item_done.setVisible(false);
		    
		    
			flag_ = true;
		}	
	} 
	
	
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case R.id.action_send:	 
	    		for(int i = 0; i < checkbox_List.size(); i++){
	    			checkbox_List.get(i).setVisibility(CheckBox.VISIBLE);
	    			checkbox_List.get(i).setChecked(false);
	    		}
	    		flag_ = false;
	    		
	    		hash_check_fbname.clear();
	    		hash_check_fburl.clear();
	    		
	    		MenuItem item_done = menu.findItem(R.id.action_send);
	    	    item_done.setVisible(true); 
	    	    item_done.setTitle(R.string.action_send_invitation); 
			    item_done.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			    
	            return true; 
	    
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	 
	 @Override
	 protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DIALOG_LOADING:
	            final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent);
	            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
	            dialog.setContentView(R.layout.custom_progress_dialog);
	            dialog.setCancelable(true);
	            dialog.setOnCancelListener(new OnCancelListener() {
	                public void onCancel(DialogInterface dialog) { 
	 
	                }
	            });
	        return dialog;  
	 
	        default:
	            return null;
	        }
	 };
	 
	 public void getFriendID(){
		 
		 mAsyncRunner = new AsyncFacebookRunner(facebook);
		 Bundle params = new Bundle();
		 params.putString("fields", "name,id");
		 mAsyncRunner.request("/me/friends", params, "GET", new FriendIDListener(), null); 
		 
	 }
	 
	 public class FriendIDListener implements RequestListener { 

		@Override
		public void onComplete(String response, Object state) {
			 
			Log.d("***JSon***", response); 
			
			/*JSONObject json;
			try {
				json = Util.parseJson(response);
				JSONArray friendsData = json.getJSONArray("data");
				String ids[] = new String[friendsData.length()]; 
				String names[] = new String[friendsData.length()];
				for(int i = 0; i < friendsData .length(); i++){ 
				         ids[i] = friendsData .getJSONObject(i).getString("id");
				         names[i] = friendsData .getJSONObject(i).getString("name");
				}
				
				Log.d("***JSon***", ids.length + " * " + names.length);
				
			} catch (JSONException e) { 
				e.printStackTrace();
			} catch (FacebookError e) { 
				e.printStackTrace();
			}*/
			
		}

		@Override
		public void onIOException(IOException e, Object state) { 
		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) { 
			
		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) { 
			
		}

		@Override
		public void onFacebookError(FacebookError e, Object state) { 
			
		}		  
	 }
	 
		private class CallAPI extends AsyncTask<String, String, String> {		 
		    @Override
		    protected String doInBackground(String... params) {
		    	
		    	Log.i("API Call", "Inside API CAll");
		    	
		    	try{  
		    		
		    		HttpClient httpClient = new DefaultHttpClient();
		    		HttpContext localContext = new BasicHttpContext();
		    		HttpGet httpGet = new HttpGet("http://sportzchat-perfomatix03.rhcloud.com/SportsChat/team/all");	    		 
					
					/*JSONStringer json = new JSONStringer()
				    .object() 
				       .key("id").value(userID)
				       .key("latitude").value(lat)
				       .key("longitude").value(lon)
				       .key("deviceType").value("ANDROID")
				    .endObject();

				    StringEntity entity = new StringEntity(json.toString());
				    entity.setContentType("application/json;charset=UTF-8"); 
				    entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
				    httpPost.setEntity(entity);  */ 
		    		
		    		try {
		    			
		    			HttpResponse response = httpClient.execute(httpGet, localContext);
		    			if(response != null){  
		    				 
		    				InputStream inputStream = response.getEntity().getContent(); 
		    	            String result = convertInputStreamToString(inputStream);
		    				
		    	            Log.i("API Call", "Success "+ result);
		    			}

		    		} catch (Exception e) { 
		    			Log.i("API Call", e+"");
		    			return e.getLocalizedMessage();
		    		}
		    		
		    	}catch(Exception e){ 
		    		Log.i("API Call", e+"");
		    	}	    	
		        return "";
		 
		 }
		 
		 protected void onPostExecute(String result) {	 
		 }
		  
		}
		private static String convertInputStreamToString(InputStream inputStream) throws IOException{ 
			BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream)); 
			String line = ""; 
			String result = "";
			while((line = bufferedReader.readLine()) != null) 
				result += line; 
				inputStream.close(); 
				return  result; 
		}
	
	
		
		public String uploadPhoto(Facebook facebook, Bitmap image, String albumId) {
			
			Log.d("FB ", "1");
			if (albumId == null) {
				albumId = "me";
				Log.d("FB ", "2");
			}
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			Log.d("FB ", "3");
			image.compress(CompressFormat.JPEG, 75, bos);
			Log.d("FB ", "4");
			byte[] photoBytes = bos.toByteArray();
			Log.d("FB ", "5");

			Bundle params = new Bundle();
			Log.d("FB ", "6");
			params.putByteArray("picture", photoBytes);
			Log.d("FB ", "7");
			try {

				String resp = facebook.request(albumId + "/photos", params, "POST");
				Log.d("FB ", "8");
				JSONObject json = Util.parseJson(resp);
				Log.d("FB ", "9");
				return json.getString("id");			
			} catch (IOException e) {
				Log.d("FB ", "10");
				System.out.println("IOException " + e);
				//Toast.makeText(this, "IOException " + e, Toast.LENGTH_SHORT).show();
			} catch (FacebookError e) {
				Log.d("FB ", "11");
				System.out.println("FacebookError " + e);
				//Toast.makeText(this, "FacebookError " + e, Toast.LENGTH_SHORT).show();
			} catch (JSONException e) {
				Log.d("FB ", "12");
				System.out.println("JSONException " + e);
				//Toast.makeText(this, "JSONException " + e, Toast.LENGTH_SHORT).show();
			}
			Log.d("FB ", "13");
			return null;
		}
 
		 
}


