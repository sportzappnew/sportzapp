
package com.perfomatix.sportzapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;


public class Sportz_new_user_event extends Activity implements OnClickListener{
	
	String fixture_id = "";
	String _teamIds = "";
	String _teamAID = "";
	String _teamBID = "";
	String _teamA_logo_url = "";
	String _teamB_logo_url = "";
	String _teamA_Name = "";
	String _teamB_Name = ""; 
	String _match_venue = "";
	String _match_date = "";
	String _match_time = "";
	String userName = "";
	String Myid ;
	String ueid;
	String _match_date_db;
	String resultDetails;
	
	
	Button btnClosePopup;
	Button btnInvite;
	Button btnChallenge;
	Button btnClose;
	  
	private SharedPreferences current_event;
	Editor current_event_editor;
	
	 private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	ImageLoader imageLoader; 
	
	ImageView im1;
	ImageView im2;
	ImageView football_center;
	TextView tv_teamA;
	TextView tv_teamB;
	TextView tv_venue;
	TextView tv_date;
	TextView tv_time;
	TextView teamAscore;
	TextView teamBscore;
	CheckedTextView checkedTextView;
	
	 
	
	RelativeLayout r_challenges;
	RelativeLayout r_invite;
	RelativeLayout r_create_event;
	RelativeLayout resultDetailsLayout;
	
	EditText event_Name;
		
	LinearLayout l_venue;
	boolean flag = true;
	
	 private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	 
	 Firebase Team_ref;
	 Firebase userEvent_ref;
	 Firebase userEvent_id;
	 Firebase detEvent_ref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_new_user_event); 
		
		
		r_challenges = (RelativeLayout)findViewById(R.id.r_challenges);
		r_create_event = (RelativeLayout)findViewById(R.id.r_create_event);
		r_invite = (RelativeLayout)findViewById(R.id.r_invite_friends);
		l_venue = (LinearLayout)findViewById(R.id.l_venue);
		resultDetailsLayout =(RelativeLayout)findViewById(R.id.resultDetails);
		
		teamAscore=(TextView)findViewById(R.id.scoreteam1);
		teamBscore=(TextView)findViewById(R.id.scoreteam2);
		 
		My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
         My_Details_editor = My_Details.edit();
         
         current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
 		 current_event_editor = current_event.edit();
         
 		Myid = My_Details.getString("Myid",null); 
 		
		  
 		userEvent_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/");
		
		Bundle b = this.getIntent().getExtras();
		
		fixture_id = current_event.getString("fixture_id",null);
		_teamA_Name = current_event.getString("homeTeam",null);
		_teamB_Name = current_event.getString("awayTeam",null);
		_match_venue = current_event.getString("venue",null);
		_match_time = current_event.getString("time",null);
		_match_date = current_event.getString("eventDate",null);
		_match_date_db = current_event.getString("eventDate_db",null);
		resultDetails = current_event.getString("resultDetails",null);
		  
		 

		detEvent_ref = new Firebase(FIREBASE_URL).child(fixture_id);
		detEvent_ref.addValueEventListener(new ValueEventListener(){

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDataChange(DataSnapshot snapshot) {
				// TODO Auto-generated method stub
				try { 
					String teamAScore="0";
					String teamBScore="0";
					
					
					if(snapshot.child("startflag").getValue(String.class).equals("1")){
						 
						if(snapshot.child("result/awayTeamFulltime").getValue()!= null){
							teamAScore = snapshot.child("result/awayTeamFulltime").getValue().toString();
							teamBScore =  snapshot.child("result/homeTeamFullTime").getValue().toString();
						}else{
							 
							teamAScore = snapshot.child("result/awayTeamHalfTime").getValue().toString();
							teamBScore =  snapshot.child("result/homeTeamHalfTime").getValue().toString();
						}
						teamAscore.setText(teamAScore); 
						teamBscore.setText(teamBScore);
					}
					
					
				
    			    	
    			    	
	    			} catch (Exception e) { 
						e.printStackTrace();
				}
			}
			
		});
		
		
	
		
		
		r_create_event.setOnClickListener(new View.OnClickListener() {	
			String result;
			@Override
			public void onClick(View v) {
				if(resultDetails.equals("1"))
					Log.i("data", ""+resultDetails);
				else
				initiatePopupWindow();
			}
						
		});
		
		r_challenges.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(resultDetails.equals("1"))
					Log.i("data", ""+resultDetails);
				else{					
				current_event_editor.putString("inviteflag", "challeng");
				current_event_editor.commit();
				   Intent i = new Intent(Sportz_new_user_event.this, Sportsapp_challengesnew.class);				
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);		
				}
            }
		});
		
		r_invite.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(resultDetails.equals("1"))
					Log.i("data", ""+resultDetails);
				else{	
				current_event_editor.putString("inviteflag", "challeng");
				current_event_editor.commit();
				   Intent i = new Intent(Sportz_new_user_event.this, Sportsapp_challengesnew.class);				
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);
				}
				
			}
		});
		
		ActionBar actionBar = getActionBar(); 
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        imageLoader = new ImageLoader(Sportz_new_user_event.this);
        tv_teamA = (TextView)findViewById(R.id.teamA_id);
        tv_teamB = (TextView)findViewById(R.id.teamB_id);
        tv_venue = (TextView)findViewById(R.id.matchvenue);
        tv_date = (TextView)findViewById(R.id.matchdate);
        tv_time = (TextView)findViewById(R.id.matchtime);
      
        
        football_center = (ImageView)findViewById(R.id.football_center);
        final Animation myRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator);
        football_center.startAnimation(myRotation);
        
       
		
		im1 = (ImageView)findViewById(R.id.im1);
		im2 = (ImageView)findViewById(R.id.im2);
		
		tv_teamA.setText(_teamA_Name);
		tv_teamB.setText(_teamB_Name);
		tv_venue.setText(_match_venue);
		tv_date.setText(_match_date);
		tv_time.setText(_match_time);
		
        
    	Team_ref = new Firebase(FIREBASE_URL).child("teams/"+_teamA_Name);
    	
    	
    	Log.i("Team_ref", Team_ref.toString());
    	
    	
    	Team_ref.addValueEventListener(new ValueEventListener() {
    	    public void onDataChange(DataSnapshot snapshot) {
    	        // do some stuff once
    	    	
    	    	 String result = snapshot.getValue().toString();               
	                Log.i("@@@@@", result);
	                
	                _teamA_logo_url = snapshot.child("tLogo").getValue(String.class);
	                new DownloadImage_A().execute(_teamA_logo_url);
    	    }
    	    public void onCancelled(FirebaseError firebaseError) {
    	    }
			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
    	});
    	
    	Team_ref = new Firebase(FIREBASE_URL).child("teams/"+_teamB_Name);
    	
    	
    	Log.i("Team_ref", Team_ref.toString());
    	
    	
    	Team_ref.addValueEventListener(new ValueEventListener() {
    	    public void onDataChange(DataSnapshot snapshot) {
    	        // do some stuff once
    	    	
    	    	 String result = snapshot.getValue().toString();               
	                Log.i("@@@@@", result);
	                
	                _teamB_logo_url = snapshot.child("tLogo").getValue(String.class);
	                new DownloadImage_B().execute(_teamB_logo_url);
    	    }
    	   
			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
    	});
    	
        
        /*String details_teamA[] = getTeamDetails(_teamAID).split("\\*");
        _teamA_Name = details_teamA[0].trim();
        _teamA_logo_url = details_teamA[1].trim();*/
        
       /* String details_teamB[] = getTeamDetails(_teamBID).split("\\*");
        _teamB_Name = details_teamB[0].trim();
        _teamB_logo_url = details_teamB[1].trim(); */
        
        //new DownloadImage_A().execute(_teamA_logo_url); 
       // new DownloadImage_B().execute(_teamB_logo_url);   
	} 
	
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{ 
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream)); 
		String line = ""; 
		String result = "";
		while((line = bufferedReader.readLine()) != null) 
			result += line; 
			inputStream.close(); 
			return  result; 
	}
	
	
	 private class DownloadImage_A extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            im1.setImageBitmap(result); 
	        }
	    } 
	
	 private class DownloadImage_B extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            im2.setImageBitmap(result); 
	        }
	    } 
	 private PopupWindow pwindo;
	    //popup method decleration
		private void initiatePopupWindow() {
			
		try {
			
		// We need to get the instance of the LayoutInflater
		LayoutInflater inflater = (LayoutInflater) Sportz_new_user_event.this
		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.sportzapp_popup,
		(ViewGroup) findViewById(R.id.popup_element));
		pwindo = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

		btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
		btnInvite = (Button) layout.findViewById(R.id.btn_invite);
		//btnChallenge=(Button) layout.findViewById(R.id.btn_challenge);
		btnClose=(Button) layout.findViewById(R.id.btn_close);
		checkedTextView=(CheckedTextView) layout.findViewById(R.id.checkedTextView1);
		
		event_Name=(EditText) layout.findViewById(R.id.eventName);
		event_Name.addTextChangedListener(new TextWatcher() {
			
		        @Override
		        public void onTextChanged(CharSequence s, int start, int before, int count) {
		        	checkFieldsForEmptyValues();
		        }

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					checkFieldsForEmptyValues();
					
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub
					
				}
				public void checkFieldsForEmptyValues(){
					
					String eventData= event_Name.getText().toString();
					
					current_event_editor.putString("user_event_name", eventData);
					current_event_editor.commit();
		        	
					if(eventData==""){
						btnClosePopup.setEnabled(false);
						 if(Myid.equals(null)||Myid.equals("")){
			        	     checkedTextView.setVisibility(View.INVISIBLE);
			        	     }else{checkedTextView.setVisibility(View.VISIBLE); }
		        	}else
		        	  {
		        	     btnClosePopup.setEnabled(true);
		        	    
		        	     if(Myid==" "){
		        	     checkedTextView.setVisibility(View.INVISIBLE);
		        	     }else{checkedTextView.setVisibility(View.VISIBLE); }
		        	     
		        	  }
				}

		    });
			
		btnClosePopup.setOnClickListener(this);
		checkedTextView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(Myid.equals(null)||Myid.equals("")){
	        	     checkedTextView.setVisibility(View.INVISIBLE);
	        	     }else{
	        	    		pwindo.dismiss();
	        				
	        				Userevents newevent = new Userevents(Myid,fixture_id,_match_date_db,_match_time,event_Name.getText().toString());
	        				
	        				  Firebase listRef  = userEvent_ref.push();
	        				   listRef.setValue(newevent);
	        			
	        				  listRef.addValueEventListener(new ValueEventListener() {
	        				    @Override
	        				    public void onDataChange(DataSnapshot snapshot) {
	        				     result = snapshot.getName().toString();
	        				     userEvent_id = new Firebase(FIREBASE_URL).child("userEvents/"+Myid+"/"+result+"/ueid");
	        				     userEvent_id.setValue(result, null);
	        				     
	        		                Log.i("@@@@@", result);
	        		                Toast.makeText(getApplicationContext(), " New event created", Toast.LENGTH_SHORT).show();   
	        		                 
	                         }
	        				   
	        					@Override
	        					public void onCancelled() {
	        						// TODO Auto-generated method stub
	        						
	        					}
	        				});
	        				  
	        				  Bundle b = new Bundle();
	        				  Intent i = new Intent(Sportz_new_user_event.this, SportzApp_Home.class);
	        				  i.putExtras(b);
	        				  startActivity(i);
	        				  
	        			
	        	     }
			}
		});
		
		btnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				pwindo.dismiss();
				
				
			}
		});
		

		} catch (Exception e) {
		e.printStackTrace();
		}
		} 
		
			
		String  result;
		
		//popup button calling
		@Override
		public void onClick(View v) { 
			
			Button btn = (Button)v;
			if(btn == btnClosePopup){
				
				current_event_editor.putString("inviteflag", "invitchallengename");
				current_event_editor.commit();
				  Intent i = new Intent(Sportz_new_user_event.this, Sportsapp_challengesnew.class);				
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);	
				 
			}else if(btn==btnClose){
			    pwindo.dismiss();   
		    }	
		}
		
	 				   
				   
		 

		
		
}
