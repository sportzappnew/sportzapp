package com.perfomatix.sportzapp;

public class Friends {
	private String id;
	private String name;
	private String url;

	// Required default constructor for Firebase object mapping
	@SuppressWarnings("unused")
	private Friends() { }

	Friends(String id, String name,String url) {
	    this.id = id;
	    this.name = name;
	    this.url = url;
	}
	
	public String getid() {
	
		return id;
	}
	
	public String getname() {
	    return name;
	}

	public String geturl() {
	    return url;
	}
}
