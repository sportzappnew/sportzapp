package com.perfomatix.sportzapp;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;



import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class SportzApp_ViewPredictions extends Activity {

	String winner;
	String teamA;
	String teamB;
	String redcardTeamA;
	String redcardTeamB;
	String yellowcarTeamA;
	String yellowcarTeamB;
	String halfTeamA;
	String halfTeamB;
	String fullTeamA;
	String fullTeamB;
	String id;
	String name;
	String pic;
	String Event_id;
	String Created_user_id;
	
	TextView tv_winner;
	TextView tv_teamA;
	TextView tv_teamB;
	TextView tv_redcardTeamA;
	TextView tv_redcardTeamB;
	TextView tv_yellowcarTeamA;
	TextView tv_yellowcarTeamB;
	TextView tv_halfTeamA;
	TextView tv_halfTeamB;
	TextView tv_fullTeamA;
	TextView tv_fullTeamB;
	
	
	
	
	
	Firebase prediction_ref;
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.sportzapp_viewpredictions);
		
		
		tv_winner =(TextView)findViewById(R.id.winprediction);
		tv_teamA =(TextView)findViewById(R.id.teamA);
		tv_teamB =(TextView)findViewById(R.id.TeamB);
		tv_redcardTeamA =(TextView)findViewById(R.id.resultredcard_teamA);
		tv_redcardTeamB =(TextView)findViewById(R.id.resultredcard_teamB);
		tv_yellowcarTeamA =(TextView)findViewById(R.id.resultyellow_teamA);
		tv_yellowcarTeamB =(TextView)findViewById(R.id.resultyellow_teamB);
		tv_halfTeamA =(TextView)findViewById(R.id.result_teamA);
		tv_halfTeamB =(TextView)findViewById(R.id.result_teamB);
		tv_fullTeamA =(TextView)findViewById(R.id.result_full_teamA);
		tv_fullTeamB =(TextView)findViewById(R.id.result_full_teamB);
		
		current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		
		Event_id = current_event.getString("Event_id",null);
		Created_user_id = current_event.getString("created_by",null);
		id = current_event.getString("id",null);
		teamA = current_event.getString("homeTeam",null);
		teamB = current_event.getString("awayTeam",null);
		
		
		prediction_ref = new Firebase(FIREBASE_URL).child("userEvents/"+Created_user_id+"/"+Event_id+"/attendees/"+id+"/predictions");
		
		prediction_ref.addListenerForSingleValueEvent(new ValueEventListener() {
    	    public void onDataChange(DataSnapshot snapshot) {
    	        // do some stuff once
    	    	
    	    	 String result = snapshot.getValue().toString();               
	                Log.i("@@@@@", result);
	                
	                
	                winner = snapshot.child("winner").getValue(String.class);
	                redcardTeamA = snapshot.child("teamA_RedCard").getValue(String.class);
	                redcardTeamB= snapshot.child("teamB_RedCard").getValue(String.class);
	                halfTeamA= snapshot.child("teamA_half_goal").getValue(String.class);
	                halfTeamB= snapshot.child("teamB_half_goal").getValue(String.class);
	                fullTeamA= snapshot.child("teamA_yellowCard").getValue(String.class);
	                fullTeamB= snapshot.child("teamB_full_goal").getValue(String.class);
	                yellowcarTeamA= snapshot.child("teamA_yellowCard").getValue(String.class);
	                yellowcarTeamB= snapshot.child("teamB_yellowCard").getValue(String.class);
	                
	                Log.i("winner", winner);
	                Log.i("redcardTeamA", redcardTeamB);
	                Log.i("halfTeamA", halfTeamA);
	                Log.i("fullTeamA", fullTeamA);
	                Log.i("yellowcarTeamA", yellowcarTeamA);
	                Log.i("halfTeamA", halfTeamA);
	                Log.i("yellowcarTeamB", yellowcarTeamB);
	                
	                tv_teamA.setText(teamA);
	        		tv_teamB.setText(teamB);
	        		tv_winner.setText(winner);
	        		tv_teamA.setText(teamA);
	        		tv_redcardTeamA.setText(redcardTeamA);
	        		tv_redcardTeamB.setText(redcardTeamB);
	        		tv_yellowcarTeamA.setText(yellowcarTeamA);
	        		tv_yellowcarTeamB.setText(yellowcarTeamB);
	        		tv_halfTeamA.setText(halfTeamA);
	        		tv_halfTeamB.setText(halfTeamB);
	        		tv_fullTeamA.setText(fullTeamA);
	        		tv_fullTeamB.setText(fullTeamB);
	                
	                
    	    }
    	    public void onCancelled(FirebaseError firebaseError) {
    	    }
			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
    	});
		
		
		
	}
	
	

}
