package com.perfomatix.sportzapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.ImageHelper;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;

public class Sportz_create_group extends Activity {
	ArrayList<Bitmap> bitmaps;
	ImageView imageView;
	EditText groupname;
	Button btn_create;
	ImageHelper imageHelper;
	Bitmap round_bitmap;
	String gname;
	String content ;
	String mycontent;
	
    private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	String Myname;
	String Myid ;
	String friendid,friendName, friendURL;
	String MyURL;
	String[] user_;
	String[] user;
	User userdetails;
	
	private Firebase Sportzrefuser;
	private Firebase Sportzrefmembers;
	private Firebase Sportz_ref_group;
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sports_create_group);
		imageHelper = new ImageHelper();
		round_bitmap =  BitmapFactory.decodeResource(getResources(), R.drawable.groupicon);
		bitmaps = new ArrayList<>();
		imageView = (ImageView)findViewById(R.id.groupimage);
		groupname = (EditText)findViewById(R.id.enter_groupname);
		btn_create = (Button)findViewById(R.id.b_create_group);
		

        My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
        My_Details_editor = My_Details.edit();
		
		 Sportz_ref_group = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Group/");
		 
		 Myid = My_Details.getString("Myid",null);
		
		try{
			
			Bundle bundle = this.getIntent().getExtras();
			String content = bundle.getString("content");
			
						
			 user_ = content.split("\\#");
			 user = content.split("\\#");
			Log.d("Length", user_.length+"");	
			
			
			for(int i = 0; i < user_.length; i++){
			
				String[] val = user_[i].split("\\*");
				Log.d("URL", val[1].trim());
				
				Bitmap bitmap = getBitmapFromURL(val[1].trim()); 
				bitmaps.add(bitmap);
			}
			
			Bitmap newBitmap = ProcessingBitmap();
			Bitmap finalBitmap = scaleDown(newBitmap, 100, true);
			round_bitmap = imageHelper.getRoundedCornerBitmap(finalBitmap, 50);
		    
			imageView.setImageBitmap(round_bitmap);
			 
		}catch(Exception e){
			
			imageView.setImageResource(R.drawable.groupicon);
		}
		
  btn_create.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(Myid != null && Myid != "" ){
					gname = groupname.getText().toString();
					Sportz_ref_group = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Group/");
					
					Group groupDetails = new Group(gname, Myid);
				      Firebase listRef  = Sportz_ref_group.push();
				      listRef.setValue(groupDetails);

					 listRef.addListenerForSingleValueEvent(new ValueEventListener() {
						    @Override
						    public void onDataChange(DataSnapshot snapshot) {
						   
						    	String result = snapshot.getName().toString();
						   
						   if(result!=null){
						    	Sportzrefuser = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Group/"+result+"/Groupid");
						    	Sportzrefuser.setValue(result);
						    	
						    	Log.i("user_.length", user.toString());
						    	for(int i = 0; i < user.length; i++){
									
									String[] val = user[i].split("\\*");
									String URL = val[1].trim();
									String id = val[2].trim();
									String name = val[0].trim();	
									Log.i("id", id);
									
									Sportzrefmembers = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Group/"+result+"/Members/"+id);
									userdetails = new User(id,URL,name); 
									Sportzrefmembers.setValue(userdetails);
									
								}
						   
						   }
						    }

							@Override
							public void onCancelled() {
								// TODO Auto-generated method stub
								
							}
					 });
			
		Bundle bundle = new Bundle();
		Intent intent = new Intent(getApplicationContext(), Sportz_FB_Tab.class);
		intent.putExtras(bundle);
		startActivity(intent);
				}
	   }
     });
		
	}
	
	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	private Bitmap ProcessingBitmap(){
		
		  Bitmap bm1 = null;
		  Bitmap bm2 =  null;
		  Bitmap bm3 = null;
		  Bitmap bm4 =  null;
		  
		  Bitmap newBitmap = null;
		  
		  bm1 = bitmaps.get(0);
		  bm2 = bitmaps.get(1);
		  bm3 = bitmaps.get(2);
		  bm4 = bitmaps.get(3);		   
		  
		  int w = bm1.getWidth() + bm2.getWidth();
		  int h ;
		  if(bm1.getHeight() >= bm2.getHeight()){
		   h = bm1.getHeight() * 2;
		  }else{
		   h = bm2.getHeight() * 2;
		  }
		  
		  Config config = bm1.getConfig();
		  if(config == null){
		   config = Bitmap.Config.ARGB_8888;
		  }
		   
		  newBitmap = Bitmap.createBitmap(w, h, config);
		  Canvas newCanvas = new Canvas(newBitmap);
		   
		  newCanvas.drawBitmap(bm1, 0, 0, null);
		  newCanvas.drawBitmap(bm2, bm1.getWidth(), 0, null);
		  newCanvas.drawBitmap(bm3, 0, bm1.getHeight(), null);
		  newCanvas.drawBitmap(bm4, bm1.getWidth(), bm1.getHeight(), null); 
		  
		  return newBitmap;
	}
	    
	public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
		  float ratio = Math.min(
		            (float) maxImageSize / realImage.getWidth(),
		            (float) maxImageSize / realImage.getHeight());
		  int width = Math.round((float) ratio * realImage.getWidth());
		  int height = Math.round((float) ratio * realImage.getHeight());
          Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
		  return newBitmap;
	}
	
	   
	    

}
