package com.perfomatix.sportzapp;

import com.fedorvlasov.lazylist.ImageLoader;

import android.app.Fragment;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Sportz_FriendsFragment extends Fragment { 
	
	ImageView football_Image;
	LinearLayout linear_image_list;
	
	ImageLoader imageLoader;
	
	public Sportz_FriendsFragment(){}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.sportz_app_fb_invite, container, false);
		
		imageLoader = new ImageLoader(this.getActivity());
		linear_image_list = (LinearLayout)rootView.findViewById(R.id.linear_image_list);
		   
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
        return rootView;
    }

}
