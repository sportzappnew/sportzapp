package com.perfomatix.sportzapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.firebase.client.Firebase;

public class Sportsapp_challenges extends Activity {
	
	RelativeLayout r_halftimegoals;
	
	SharedPreferences current_event;
	Editor current_event_editor;
	
	Button btninvite;
	RadioGroup rgbutton;
	EditText teamA_half_goals;
	EditText teamB_half_goals;
	EditText teamA_full_goals;
	EditText teamB_full_goals;
	
	String teamA_half = "0";
	String teamB_half = "0";
	String teamA_full = "0";
	String teamB_full = "0";
	String winner;
	String Event_id;
	String Created_user_id;
	String userID;
	String userEvent_id;
	String fixture_id;
	String _match_time = "";
	String _match_date_db = "";
	
	Firebase challenges;
	Firebase firebaseuserEvent_id;
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_challenges); 
		
		rgbutton = (RadioGroup) findViewById(R.id.rgbutton);
		btninvite = (Button) findViewById(R.id.btninvite_friends);
		teamA_half_goals = (EditText)findViewById(R.id.teamA_half_goals);
		teamB_half_goals = (EditText)findViewById(R.id.teamB_half_goals);
		teamA_full_goals = (EditText)findViewById(R.id.teamB_full_goals);
		teamB_full_goals = (EditText)findViewById(R.id.teamA_full_goals);
		
		
		
		Bundle b = this.getIntent().getExtras();
		userEvent_id = b.getString("userEvent_id");
		Event_id = b.getString("Event_id");
		userID = b.getString("userID");
		Created_user_id = b.getString("Created_user_id");
		fixture_id = b.getString("fixture_id");
		_match_time = b.getString("_match_time");
		_match_date_db = b.getString("_match_date_db");
		
		current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		
		challenges = new Firebase(FIREBASE_URL).child("userEvents/"+Created_user_id+"/"+Event_id+"/challenges/"+userID);
					
		btninvite.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				teamA_half = teamA_half_goals.getText().toString();
	        	teamB_half = teamB_half_goals.getText().toString();
	        	teamB_full = teamA_full_goals.getText().toString();
	        	teamA_full = teamB_full_goals.getText().toString();
	        	
	        	
	            // Get Selected Radio Button and display output
	            RadioButton selectRadio = (RadioButton) findViewById(rgbutton
	                    .getCheckedRadioButtonId());
	             winner = selectRadio.getText().toString();
	             
	             //firebase data push
	             
	             
	            /* Challenges predictions = new Challenges(Event_id, userID, winner, teamA_half, teamB_half, teamA_full, teamB_full);
	             
	             challenges.setValue(predictions);*/
	             
	             
	                current_event_editor.putString("invitefalg", "challeng");
					current_event_editor.commit();
	             
	                Intent i = new Intent(getApplicationContext(), SportzApp_FB_Invite.class);				
					Bundle b = new Bundle();
					
					b.putString("fixture_id", fixture_id.toString());
					b.putString("_match_date_db", _match_date_db.toString());
					b.putString("_match_time", _match_time.toString());
					
					b.putString("winner", winner.toString());
					b.putString("teamA_half", teamA_half.toString());
					b.putString("teamB_half", teamB_half.toString());
					b.putString("teamA_full", teamA_full.toString());
					b.putString("teamB_full", teamB_full.toString());
					
					i.putExtras(b);
					startActivity(i);
	             
	             
			}
		});
		
	}
	
}