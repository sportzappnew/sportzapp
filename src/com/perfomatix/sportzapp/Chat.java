package com.perfomatix.sportzapp;

public class Chat {

	private String name;
    private String message;
    
    
    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Chat() { }

    Chat(String name, String message) {
        this.name = name;
        this.message = message;
        
    }

    public String getname() {
        return name;
    }

    public String getmessage() {
        return message;
    }
   
	
}
