package com.perfomatix.sportzapp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Window;

public class DFragment extends DialogFragment{ 

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {  
		super.onCreateDialog(savedInstanceState);
		
		final Dialog dialog = new Dialog(this.getActivity(), android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
        dialog.setContentView(R.layout.custom_progress_dialog);
        dialog.setCancelable(true); 
        
        return dialog;  
	} 

}
