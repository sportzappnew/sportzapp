package com.perfomatix.sportzapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;







import java.util.Map;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;
import com.perfomatix.sportzapp.R.layout;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore.Video.Thumbnails;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class Sportz_Chat_Window extends Activity implements SurfaceHolder.Callback{
	
	RelativeLayout chatwindow_ll;
	ImageButton bt_video;
	ImageButton bt_send;
	EditText chatText;
	String Event_id = "";
	String created_by = "483763955060529";
	String Myname = "william";
	String _match_name;
	
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	
	
	MediaRecorder mediaRecorder;
	SurfaceHolder surfaceHolder;
	SurfaceView sv_videoView;
	
	private LinearLayout singleMessageContainer;
	public static String userMessage = "";
  
	private Firebase eventchat;
  
   
	
	private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_chat_window); 

		    current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
			current_event_editor = current_event.edit();
			
			My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
		    My_Details_editor = My_Details.edit();
       
		
		chatwindow_ll = (RelativeLayout)findViewById(R.id.chatwindow_ll);
		bt_video = (ImageButton)findViewById(R.id.bt_video);
		bt_send = (ImageButton) findViewById(R.id.bt_send);
		
		sv_videoView = new SurfaceView(Sportz_Chat_Window.this);
		
		
		chatText = (EditText) findViewById(R.id.edit_chat_msg);
		
		chatText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
	            @Override
	            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
	                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
	                	
	                }
	                return true;
	            }
	        });
	
        singleMessageContainer = (LinearLayout)findViewById(R.id.chat_layout); 
	       
        Myname = My_Details.getString("Myname",null);
        Event_id = current_event.getString("Event_id",null);
        created_by = current_event.getString("created_by",null);        
        _match_name = current_event.getString("user_event_name",null); 
        
        setTitle(_match_name);
        
        created_by = "483763955060529";
        
        Map<String, String> post1 = new HashMap<String, String>();
        
        eventchat = new Firebase(FIREBASE_URL).child("userEvents/"+created_by+"/"+Event_id+"/chat");
        
        
        
        eventchat.addChildEventListener(new ChildEventListener() {
            // Retrieve new posts as they are added to Firebase
            public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
              //  Map<String, Object> newPost = (Map<String, Object>) snapshot.getValue();
               // System.out.println("Author: " + newPost.get("name"));
               // System.out.println("Title: " + newPost.get("message"));
                
                String name = snapshot.child("name").getValue(String.class);
    			String message = snapshot.child("message").getValue(String.class);
    			
    			
    			
    			final LinearLayout chathead = new LinearLayout(getApplicationContext());
		 		chathead.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		 		chathead.setOrientation(LinearLayout.HORIZONTAL);
		 		
		    	 
			 	final TextView chatmsg = new TextView(getApplicationContext());
			 	chatmsg.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			 	chatmsg.setTextColor(Color.parseColor("#000000"));
			 	chatmsg.setTextSize(16);
			 	
			 	chatmsg.setMovementMethod(new ScrollingMovementMethod());
			 	
			 	if(Myname.equals(name)){
			 		
			 		String chatmsguser = message;
			 		
			 		chatmsg.setText(chatmsguser);
					chatmsg.setBackgroundResource(R.drawable.bubble_a);
					chathead.setGravity(Gravity.RIGHT );
					
					chathead.addView(chatmsg);
					singleMessageContainer.addView(chathead);
					
			 	
			 	}else{
			 		
			 		String chatmsg1 = name+": "+message;
			 		
			 		chatmsg.setText(chatmsg1);
			 		chatmsg.setBackgroundResource(R.drawable.bubble_a);
					chathead.setGravity(Gravity.LEFT );
					
					chathead.addView(chatmsg);
					singleMessageContainer.addView(chathead);
			 	}
            }
            //... ChildEventListener also defines onChildChanged, onChildRemoved,
            //    onChildMoved and onCanceled, covered in later sections.

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildChanged(DataSnapshot arg0, String arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildMoved(DataSnapshot arg0, String arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildRemoved(DataSnapshot arg0) {
				// TODO Auto-generated method stub
				
			}
        });
        
        
				    	
						
					
		    	
		    	
			 	
		    	
		    
        
        
        bt_send.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View arg0) {
	            	EditText userTypedMessage = (EditText) findViewById(R.id.edit_chat_msg);
	        		userMessage = userTypedMessage.getText().toString();
	        		Chat message = new Chat(Myname,userMessage);
	                eventchat.push().setValue(message);
	        		chatText.setText("");
	                
	            }
	        });

		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				(int) LayoutParams.MATCH_PARENT, 300);
	        
	    params.addRule(RelativeLayout.CENTER_HORIZONTAL);
	    params.topMargin = 10;
	      
	    sv_videoView.setLayoutParams(params); 
	    
	   mediaRecorder = new MediaRecorder();
	    Camera camera = Camera.open();
	    camera.setDisplayOrientation(90);
	    camera.unlock();
	    mediaRecorder.setCamera(camera);
        initMediaRecorder();
         
        surfaceHolder = sv_videoView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        bt_video.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	
            	if(event.getAction() == MotionEvent.ACTION_DOWN){
            		
            		chatwindow_ll.addView(sv_videoView); 
            		
            	}else if(event.getAction() == MotionEvent.ACTION_UP){
            		 
            		mediaRecorder.stop();
            		chatwindow_ll.removeView(sv_videoView);
            		
            		Bitmap bmThumbnail;
            	    bmThumbnail = ThumbnailUtils.createVideoThumbnail("/sdcard/myvideo.mp4", Thumbnails.MICRO_KIND);
            	                	    
            	    ImageView imageThumbnail = new ImageView(getApplicationContext());
            	    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(100, 100);                    		 
                     
                    params2.addRule(RelativeLayout.ALIGN_PARENT_START);
                    
                    imageThumbnail.setLayoutParams(params2);
                    imageThumbnail.setImageBitmap(bmThumbnail);
                    
                    chatwindow_ll.addView(imageThumbnail);
                    
            	}
            	
                return true;
            }
        });
	}
	
	private void initMediaRecorder(){
		
		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        CamcorderProfile camcorderProfile_HQ = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        mediaRecorder.setProfile(camcorderProfile_HQ);
        mediaRecorder.setOutputFile("/sdcard/myvideo.mp4");
        mediaRecorder.setMaxDuration(30000);  
        mediaRecorder.setMaxFileSize(5000000);  
	}
	
	private void prepareMediaRecorder(){
		
		mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
		
		try {
			
			mediaRecorder.prepare(); 
			
		} catch (IllegalStateException e) { 
			e.printStackTrace();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}

	     @Override
		public void surfaceCreated(SurfaceHolder holder) { 
				prepareMediaRecorder(); 
				mediaRecorder.start();
			} 
			@Override
			
		
		public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) { 
				
			} 
			@Override
			
		public void surfaceDestroyed(SurfaceHolder holder) { 
				
			}
			
		
	 
	
	@Override
	protected void onPause() {

	    // hide the keyboard in order to avoid getTextBeforeCursor on inactive InputConnection
	    InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

	    inputMethodManager.hideSoftInputFromWindow(chatText.getWindowToken(), 0);

	    super.onPause();
	}
	 
          private void sendMessage() {
	        EditText inputText = (EditText)findViewById(R.id.edit_chat_msg);
	        String input = inputText.getText().toString();
	        if (!input.equals("")) {
	            // Create our 'model', a Chat object
	         //  Group chat = new Group(input, username);
	            // Create a new, auto-generated child of that chat location, and save our chat data there
	           // Sportzrefuser.push().setValue(chat);
	            inputText.setText("");
	        }
	    }


}
