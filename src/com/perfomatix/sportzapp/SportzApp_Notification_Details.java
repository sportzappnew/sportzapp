package com.perfomatix.sportzapp;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;

import org.json.JSONObject;

import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SportzApp_Notification_Details extends Activity {
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	private SharedPreferences My_Details;
	Editor My_Details_editor;
	
	Firebase Event_ref;
	Firebase Team_ref;
	Firebase notNowRef;
	Firebase notNowRefCreated;
	Firebase declineRef;
	Firebase declineRefCreated;
	Firebase Notification;
	
	String fixture_id;
	String event_id;
	String flag;
	String created_by;
	String _match_time ;
	String _match_Date;
	String eventName ;
	String _teamA_logo_url = "";
	String _teamB_logo_url = "";	
	String homeTeam;
	String awayTeam;
	String Myid;
	String id,pic,name;
	String eventDate;
	
	ImageView nfIm1;
	ImageView nfIm2;
	ImageLoader imageLoader; 
	
	TextView txtEventName;
	TextView txtMatchDate;
	TextView txtMatchTime;
	TextView txtTeamAname;
	TextView txtTeamBname;
	TextView txtMatchVenue;
	
	Hashtable<RelativeLayout,String > hash_attendes_details;
	Hashtable<RelativeLayout, String> hash_attendes_click;
	
	
	RelativeLayout decline;
	RelativeLayout outer;
	LinearLayout attendes_layout;
	RelativeLayout accept;
	RelativeLayout notNow;
	final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	
	 public void onCreate(Bundle savedInstanceState) {
		 
		    super.onCreate(savedInstanceState); 
		    setContentView(R.layout.sportzapp_notification_details); 
		    
		    current_event = this.getSharedPreferences("current_event", Context.MODE_PRIVATE);
	 		current_event_editor = current_event.edit();
	 		
	 		 My_Details = this.getSharedPreferences("My_Details", Context.MODE_PRIVATE);
             My_Details_editor = My_Details.edit();
		    
             hash_attendes_details = new Hashtable<RelativeLayout, String>();
     		hash_attendes_click = new Hashtable<RelativeLayout, String>();
             
     		
     		
            attendes_layout = (LinearLayout)findViewById(R.id.attendes_layout);
	 		event_id = current_event.getString("Event_id",null);
		    flag = current_event.getString("flag",null);
			fixture_id = current_event.getString("fixture_id",null);
			created_by = current_event.getString("created_by",null);
			_match_Date = current_event.getString("_match_Date",null);
			_match_time = current_event.getString("_match_time",null);
			eventName = current_event.getString("user_event_name",null);
			
			Notification = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Notifications/"+event_id);
						
			Myid = My_Details.getString("Myid",null);
			
			
			txtEventName=(TextView)findViewById(R.id.ntmatchname);
			txtMatchDate=(TextView)findViewById(R.id.ntmatchdate);
			txtMatchTime=(TextView)findViewById(R.id.ntmatchtime);
			txtTeamAname=(TextView)findViewById(R.id.teamAname);
			txtTeamBname=(TextView)findViewById(R.id.teamBname);
			txtMatchVenue=(TextView)findViewById(R.id.ntmatchvenue);
			
			decline=(RelativeLayout)findViewById(R.id.iddecline);
			notNow=(RelativeLayout)findViewById(R.id.idnotnow);
			accept=(RelativeLayout)findViewById(R.id.id_accept); 
			
			txtEventName.setText(eventName);
			txtMatchDate.setText(getDate(_match_Date));
			txtMatchTime.setText(_match_time); 
			
			nfIm1 = (ImageView)findViewById(R.id.nfim1);
			nfIm2 = (ImageView)findViewById(R.id.nfim2);
			
			Log.i("event url", ""+fixture_id);
			
			Event_ref = new Firebase(FIREBASE_URL).child(fixture_id);
			Event_ref.addListenerForSingleValueEvent(new ValueEventListener() {
				
				@SuppressLint("ResourceAsColor")
				@Override
		        public void onDataChange(DataSnapshot snapshot) {
					
					try {
						
						String id = snapshot.child("id").getValue(String.class);
		    			 homeTeam = snapshot.child("homeTeam").getValue(String.class);
		    			 awayTeam = snapshot.child("awayTeam").getValue(String.class);
		    			 eventDate = snapshot.child("eventDate").getValue(String.class);
		    			String startTime = snapshot.child("startTime").getValue(String.class);
		    			String venue1 = snapshot.child("venue").getValue(String.class); 
			    		
		    			txtTeamAname.setText(homeTeam);
		    			txtTeamBname.setText(awayTeam);
		    			txtMatchVenue.setText(venue1);
		    			
		    			 Team_ref = new Firebase(FIREBASE_URL).child("teams/"+awayTeam);
		    		    	
		    		    	Team_ref.addListenerForSingleValueEvent(new ValueEventListener() {
		    		    	    public void onDataChange(DataSnapshot snapshot) {
		    		    	        // do some stuff once
		    		    	    			    			                
		    			                _teamB_logo_url = snapshot.child("tLogo").getValue(String.class);
		    			                new DownloadImage_B().execute(_teamB_logo_url);
		    		    	    }
		    		    	   
		    					@Override
		    					public void onCancelled() {
		    						// TODO Auto-generated method stub
		    						
		    					}
		    		    	});
		    		    	Team_ref = new Firebase(FIREBASE_URL).child("teams/"+homeTeam);
		    		    	
		    		    	Team_ref.addListenerForSingleValueEvent(new ValueEventListener() {
		    		    	    public void onDataChange(DataSnapshot snapshot) {
		    		    	        // do some stuff once
		    		    	    	
		    			                _teamA_logo_url = snapshot.child("tLogo").getValue(String.class);
		    			                new DownloadImage_A().execute(_teamA_logo_url);
		    		    	    }
		    		    	   
		    					@Override
		    					public void onCancelled() {
		    						// TODO Auto-generated method stub
		    						
		    					}
		    		    	});
		    			
		    			} catch (Exception e) { 
							e.printStackTrace();
					}	                
		          }
		          @Override
		          public void onCancelled() { 
						
				}
					
		    });
			
			notNow.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					notNowRef = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Notifications/"+event_id);
					notNowRef.child("flag").setValue("notNow");
					
					notNowRefCreated = new Firebase(FIREBASE_URL).child("userEvents/"+created_by+"/"+event_id+"/invitees/"+Myid);
					notNowRefCreated.child("flag").setValue("notNow");
					Intent i = new Intent(SportzApp_Notification_Details.this, Sportz_Notifications.class);				
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);
				}
			});
			
			accept.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) { 
					
					    current_event_editor.putString("fixture_id",fixture_id );
		                current_event_editor.putString("Event_id",event_id );
		                current_event_editor.putString("created_by",created_by );
		                current_event_editor.putString("eventDate_db",eventDate );
		                current_event_editor.putString("homeTeam", txtTeamAname.getText().toString() );
		                current_event_editor.putString("awayTeam", txtTeamBname.getText().toString() );
		                current_event_editor.putString("eventDate", _match_Date);
		                current_event_editor.putString("venue",txtMatchVenue.getText().toString() );
		                current_event_editor.putString("time", txtMatchTime.getText().toString() );
		                current_event_editor.putString("inviteflag", "notificationConfirm");
		                current_event_editor.putString("user_event_name",eventName);
		                current_event_editor.commit();
					Intent i = new Intent(SportzApp_Notification_Details.this, Sportsapp_challengesnew.class);				
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);
				}
			});
			
			decline.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Ddialog d= new Ddialog();
					d.show(getFragmentManager(), "");
				}
			});
		
			 
			 Notification.addListenerForSingleValueEvent(new ValueEventListener() {
		         @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
					@SuppressLint("ResourceAsColor")
					public void onDataChange(DataSnapshot snapshot) {                             
		            
		         	 System.out.println(snapshot.getValue());
		 		    try {
			    		    	
				    			     id = snapshot.child("predictions/myid").getValue(String.class);
				    			     pic = snapshot.child("predictions/mypic").getValue(String.class);
				    			    				    		    	
			    		    		
			    		    		outer = new RelativeLayout(getApplicationContext()); 
			    			    	RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
			    			                RelativeLayout.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
			    			    	outer.setLayoutParams(rlp);
			    			    	outer.setClickable(true);
			    			    	outer.setBackgroundResource(R.drawable.gradient1); 
			    			    	
			    			    	 hash_attendes_click.put(outer,id);	
			    			    	 
			    			    	 JSONObject job_user = new JSONObject(pic);
			 			 			 String user_url = job_user.getString("data");
			 			 			 JSONObject job_user_url = new JSONObject(user_url);
			 			 			 final String user_pic_url = job_user_url.getString("url");
			    			    	 
			    			    		ImageView imageView = new ImageView(getApplicationContext()); 
			    			    		RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
			    			    		imageView.setLayoutParams(ilp); 
			    			    		ilp.addRule(RelativeLayout.CENTER_VERTICAL);
			    			    		ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
			    			    		
			    			    		TextView textView = new TextView(getApplicationContext());
			    			    		RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
			    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
			    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
			    			    		tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
			    			    		textView.setLayoutParams(tlp);
			    			    		textView.setTextColor(Color.parseColor("#424242")); 
			    			    		textView.setTextSize(15);
			    			    		textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
			    			    		textView.setText(name); 
			    			    		
			    			    		TextView tv_vp= new TextView(getApplicationContext());
			    			    		RelativeLayout.LayoutParams tlp1 = new RelativeLayout.LayoutParams(
			    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
			    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
			    			    		tlp1.addRule(RelativeLayout.ALIGN_PARENT_END);
			    			    		tlp1.addRule(RelativeLayout.CENTER_VERTICAL); 
			    			    		tv_vp.setLayoutParams(tlp1);
			    			    		tv_vp.setPaddingRelative(0, 0, 2, 0);
			    			    		tv_vp.setTypeface(Typeface.SERIF,Typeface.BOLD);
			    			    		tv_vp.setTextColor(Color.parseColor("#4dd0e1")); 
			    			    		tv_vp.setTextSize(12); 
			    			    		tv_vp.setText("     View\nPredictions"); 
			    			    		
			    			    		
			    			    		  outer.addView(imageView);
			    			    		  outer.addView(textView);
			    			    		  outer.addView(tv_vp);
			    			    			
			    			    		  
			    			    			attendes_layout.addView(outer);
						 			        imageLoader.DisplayImage(user_pic_url, imageView); 
						 			
						 			       outer.setOnClickListener(new View.OnClickListener() {
												@Override
												public void onClick(View v) {  
													 RelativeLayout out = (RelativeLayout)v;
													 id = hash_attendes_click.get(out); 
													
													
													    current_event_editor.putString("id",id );
							  			                current_event_editor.putString("name",name );
							  			                current_event_editor.putString("pic",pic );
							  			                current_event_editor.commit();		
							  			                
													 
													      Bundle b = new Bundle();
						    							  Intent i = new Intent(getApplicationContext(), SportzApp_ViewPredictions.class);
						    							  i.putExtras(b);
						    							  startActivity(i);
											
											}		
										});	
			    		    	
		 		    }catch(Exception e){
		 		    	
		 		    	e.printStackTrace();
		 		    	
		 		      }
		 		    }

					@Override
					public void onCancelled() {
						// TODO Auto-generated method stub
						
					}

		 		    
		         });
			
		   }
	 
	 private String getDate(String event_Date){
			String date = "";
			String month = "";
			
			String a[] = event_Date.split("\\,"); 
			String day = a[0];
			String date_[] = a[1].split("\\-");
			switch(date_[1]){
			case "01":
				month = "January";
				break;
			case "02":
				month = "February";
				break;
			case "03":
				month = "March";
				break;
			case "04":
				month = "April";
				break;
			case "05":
				month = "May";
				break;
			case "06":
				month = "June";
				break;
			case "07":
				month = "July";
				break;
			case "08":
				month = "August";
				break;
			case "09":
				month = "September";
				break;
			case "10":
				month = "October";
				break;
			case "11":
				month = "November";
				break;
			case "12":
				month = "December";
				break;
				
			}	
			
			date = day+" "+date_[0]+" "+month+" "+date_[2]; 
			return date;
		}
	 
	 private class DownloadImage_A extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            nfIm1.setImageBitmap(result); 
	        }
	    } 
	
	 private class DownloadImage_B extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            nfIm2.setImageBitmap(result); 
	        }
	    } 
	 public class Ddialog extends DialogFragment {
		    public Dialog onCreateDialog(Bundle savedInstanceState) {
		    	
		        // Use the Builder class for convenient dialog construction
		        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		        builder.setMessage(R.string.delete)
		               .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	    declineRef = new Firebase(FIREBASE_URL).child("User/"+Myid+"/Notifications/"+event_id);
			   					declineRef.removeValue();
			   					declineRefCreated =  new Firebase(FIREBASE_URL).child("userEvents/"+created_by+"/"+event_id+"/invitees/"+Myid);
			   					declineRef.removeValue();
			   					
			   					Intent i = new Intent(SportzApp_Notification_Details.this, Sportz_Notifications.class);				
								Bundle b = new Bundle();
								i.putExtras(b);
								startActivity(i);
		                   }
		               })
		               .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                       // User cancelled the dialog
		                   }
		               });
		        // Create the AlertDialog object and return it
		        return builder.create();
		    }
		
	 }
		

	 
}
			 

