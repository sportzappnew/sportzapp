package com.perfomatix.sportzapp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.fedorvlasov.lazylist.ImageLoader;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;



public class Sportz_Event_Details extends Activity{
	
	String Event_id = "";
	String _teamIds = "";
	String _teamAID = "";
	String _teamBID = "";
	String _teamA_logo_url = "";
	String _teamB_logo_url = "";
	String _teamA_Name = "";
	String _teamB_Name = ""; 
	String _match_venue = "";
	String _match_date = "";
	String _match_time = "";
	String _match_date_db = "";
	String Created_user_id = "";
	String fixture_id = "";
	String Myid ;
	String id,name,pic ;
	String inviteflag="";
	String _match_name;
	String screen_comment;
	String myResultFlag;
	
	private SharedPreferences current_event;
	Editor current_event_editor;
	
 	private SharedPreferences My_Details;
	Editor My_Details_editor;

	ImageLoader imageLoader; 
	ImageView imageViewSelectedImage_screen;
	AlertDialog imageDialog_screen;
	
	ImageView im1;
	ImageView im2;
	ImageView football_center;
	TextView tv_teamA;
	TextView tv_teamB;
	TextView tv_venue;
	TextView tv_date;
	TextView tv_time;
	TextView tv_name;
	TextView edit_text;
	TextView tv_share;
	TextView teamAscore;
	TextView teamBscore;
	EditText ed_screen_shot;
	 
	Hashtable<RelativeLayout,String > hash_attendes_details;
	Hashtable<RelativeLayout, String> hash_attendes_click;
	
	RelativeLayout r_challenges;
	RelativeLayout r_invite;
	RelativeLayout r_chat;
	RelativeLayout outer;
	
	LinearLayout attendes_layout;
	boolean flag = true;
	
	 private static final String FIREBASE_URL = "https://sportzapp.firebaseio.com";
	 
	 Firebase Team_ref;
	 Firebase Atendees;
	 Firebase detEvent_ref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_event_details); 
		
		
		r_challenges = (RelativeLayout)findViewById(R.id.r_challenges);
		r_chat = (RelativeLayout)findViewById(R.id.r_chat);
		r_invite = (RelativeLayout)findViewById(R.id.r_invite_friends);
		attendes_layout = (LinearLayout)findViewById(R.id.attendes_layout); 
		teamAscore=(TextView)findViewById(R.id.teamAscore);
		teamBscore=(TextView)findViewById(R.id.teamBscore);
		
		hash_attendes_details = new Hashtable<RelativeLayout, String>();
		hash_attendes_click = new Hashtable<RelativeLayout, String>();
		
		current_event = getApplicationContext().getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		
		My_Details = getApplicationContext().getSharedPreferences("My_Details", Context.MODE_PRIVATE);
        My_Details_editor = My_Details.edit();
		
        Myid = My_Details.getString("Myid",null);
		 Log.i("Myid1111", Myid);
		
		ActionBar actionBar = getActionBar(); 
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        imageLoader = new ImageLoader(getApplicationContext());
       
        tv_teamA = (TextView)findViewById(R.id.teamA_id);
        tv_teamB = (TextView)findViewById(R.id.teamB_id);
        tv_venue = (TextView)findViewById(R.id.matchvenue);
        tv_date = (TextView)findViewById(R.id.matchdate);
        tv_time = (TextView)findViewById(R.id.matchtime);
        tv_name = (TextView)findViewById(R.id.matchname);
        edit_text = (TextView)findViewById(R.id.edit_challenge);
        tv_share = (TextView)findViewById(R.id.tv_share);
       
        
        
        football_center = (ImageView)findViewById(R.id.football_center);
        final Animation myRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator);
        football_center.startAnimation(myRotation);
        
		im1 = (ImageView)findViewById(R.id.im1);
		im2 = (ImageView)findViewById(R.id.im2);
				
		Bundle b = this.getIntent().getExtras();
		
        
        fixture_id = current_event.getString("fixture_id",null);
		_teamA_Name = current_event.getString("homeTeam",null);
		_teamB_Name = current_event.getString("awayTeam",null);
		_match_venue = current_event.getString("venue",null);
		_match_time = current_event.getString("time",null);
		_match_date = current_event.getString("eventDate",null);
		_match_name = current_event.getString("user_event_name",null);
		Event_id = current_event.getString("Event_id",null);
		Created_user_id = current_event.getString("created_by",null);
		_match_date_db = current_event.getString("eventDate_db",null);
		myResultFlag = current_event.getString("myResultFlag",null);
		
		Myid = My_Details.getString("Myid",null);
		 Log.i("Myid1111", Myid);
		 
		 if(Created_user_id.equals(Myid))
		 {   tv_share.setText("Invite");
			 
		 }else{
			
			 r_invite.setBackgroundResource(R.drawable.fbsshare);
		 }
		
		Atendees=new Firebase(FIREBASE_URL).child("userEvents/"+Created_user_id+"/"+Event_id+"/attendees/");
		
		Log.i("resulttt", Atendees.toString());
		
		  r_challenges.setOnClickListener(new View.OnClickListener() {			
	  			@Override
	  			public void onClick(View v) {
	  				 
	  				
	  				if(myResultFlag.equals("1")){
						Log.i("data ","result flag"+myResultFlag);
						
					}else{
	  				Atendees.addListenerForSingleValueEvent(new ValueEventListener() {
	  		            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	  					@SuppressLint("ResourceAsColor")
	  					public void onDataChange(DataSnapshot snapshot) {                             
	  		               String val = "zero";
	  		             String val1 = "zero";
	  		            	 System.out.println(snapshot.getValue());
	  		    		    try {
	  			    		    	for ( DataSnapshot snapshot1 : snapshot.getChildren()) {
	  			    		    		Myid = My_Details.getString("Myid",null);
	  			    		    		 Log.i("Myid", Myid);
	  				    			    	  id = snapshot1.child("attendeesDetails/myid").getValue(String.class);
	  				    			    	 if(id.equals(Myid)){
	  				    			    		 
	  				    			    		    edit_text.setText("edit challenge");
	  				    			    		  val1 = "one";
	  				    			    		Log.i("notificationConfirm", val);
	  				    			    	  }
	  				    			    	
	  			    		    	}
	  			    		    	if(val1.equals("one")){
	  			    		    		    current_event_editor.putString("inviteflag", "notificationConfirm");
				    		  				current_event_editor.commit();
				    		  				sendchallenge();
	  			    		    	}else{
	  			    		    		current_event_editor.putString("inviteflag", "challenge_edit");
			    		  				current_event_editor.commit();
			    		  				sendchallenge();
	  			    		    	}
	  			    		    	
	  		    		    }catch(Exception e){
	  		    		    	
	  		    		    	e.printStackTrace();
	  		    		    }
	  		            }

						@Override
						public void onCancelled() {
							// TODO Auto-generated method stub
							
						}
	  				});
	  				     				
	  			 }
	  			}
	  		});
		  
		    detEvent_ref = new Firebase(FIREBASE_URL).child(fixture_id);
			detEvent_ref.addValueEventListener(new ValueEventListener(){

				@Override
				public void onCancelled() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onDataChange(DataSnapshot snapshot) {
					// TODO Auto-generated method stub
					try { 
						String teamAScore="0";
						String teamBScore="0";
						
						
						if(snapshot.child("startflag").getValue(String.class).equals("1")){
							 
							if(snapshot.child("result/awayTeamFulltime").getValue()!= null){
								teamAScore = snapshot.child("result/awayTeamFulltime").getValue().toString();
								teamBScore =  snapshot.child("result/homeTeamFullTime").getValue().toString();
							}else{
								 
								teamAScore = snapshot.child("result/awayTeamHalfTime").getValue().toString();
								teamBScore =  snapshot.child("result/homeTeamHalfTime").getValue().toString();
							}
							teamAscore.setText(teamAScore); 
							teamBscore.setText(teamBScore);
						}
						
						
					
	    			    	
	    			    	
		    			} catch (Exception e) { 
							e.printStackTrace();
					}
				}
				
			});
		
		  r_chat.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				Intent i = new Intent(Sportz_Event_Details.this, Sportz_Chat_Window.class);				
				Bundle b = new Bundle();
				i.putExtras(b);
				startActivity(i);
			}
		});
		
		  r_invite.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				
					if(Created_user_id.equals(Myid) ) {
						if(myResultFlag.equals("1")){
							Log.i("data ","result flag"+myResultFlag);
							
						}else{
						   
						    Intent i = new Intent(Sportz_Event_Details.this, SportzApp_FB_Invite.class);				
							Bundle b = new Bundle();
							current_event_editor.putString("inviteflag", "invite_edit");
							current_event_editor.commit();
							
							i.putExtras(b);
							startActivity(i);
						 
					 }
					}else{
						 
						    r_invite.setBackgroundResource(R.drawable.fbsshare);
						    View view = v.getRootView();
							view.setDrawingCacheEnabled(true);
							Bitmap bitmap_screen = view.getDrawingCache();
							
						 showImageDialog_Screen(bitmap_screen, "facebook");
						 
						  
					 }
				
			}
		});
		
		/*r_venue.setOnClickListener(new View.OnClickListener() {			
			@SuppressLint("ResourceAsColor")
			@Override
			public void onClick(View v) {
				ImageView i;
				LinearLayout l;
				if(flag == true){
					
					l = new LinearLayout(getApplicationContext());
					l.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 2));
					l.setBackgroundColor(Color.BLACK); 
					
					i = new ImageView(getApplicationContext());
					i.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					i.setImageResource(R.drawable.football_ground);
					
					l_venue.addView(l);
					l_venue.addView(i);
					
					flag = false;
				}else
				{
					l_venue.removeAllViews();
					flag = true;
				}
				
			}
		});*/
		
		
		tv_teamA.setText(_teamA_Name);
		tv_teamB.setText(_teamB_Name);
		tv_venue.setText(_match_venue);
		tv_date.setText(_match_date);
		tv_time.setText(_match_time);
		tv_name.setText(_match_name);
		
		
		 
        
		Team_ref = new Firebase(FIREBASE_URL).child("teams/"+_teamA_Name);
    	
    	Team_ref.addListenerForSingleValueEvent(new ValueEventListener() {
    	    public void onDataChange(DataSnapshot snapshot) {
    	        // do some stuff once
    	    	
    	    	 String result = snapshot.getValue().toString();               
	                Log.i("@@@@@", result);
	                
	                _teamA_logo_url = snapshot.child("tLogo").getValue(String.class);
	                new DownloadImage_A().execute(_teamA_logo_url);
    	    }
    	    public void onCancelled(FirebaseError firebaseError) {
    	    }
			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
    	});
    	
    	Team_ref = new Firebase(FIREBASE_URL).child("teams/"+_teamB_Name);
    	
    	Team_ref.addListenerForSingleValueEvent(new ValueEventListener() {
    	    public void onDataChange(DataSnapshot snapshot) {
    	        // do some stuff once
    	    	
    	    	 String result = snapshot.getValue().toString();               
	                Log.i("@@@@@", result);
	                
	                _teamB_logo_url = snapshot.child("tLogo").getValue(String.class);
	                new DownloadImage_B().execute(_teamB_logo_url);
    	    }
    	    public void onCancelled(FirebaseError firebaseError) {
    	    }
			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
    	});
    	
    	
    	
		Atendees.addListenerForSingleValueEvent(new ValueEventListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
			@SuppressLint("ResourceAsColor")
			public void onDataChange(DataSnapshot snapshot) {                             
               
            	 System.out.println(snapshot.getValue());
    		    try {
	    		    	for ( DataSnapshot snapshot1 : snapshot.getChildren()) {
	    		    		
		    			    	  id = snapshot1.child("attendeesDetails/myid").getValue(String.class);
		    			    	  pic = snapshot1.child("attendeesDetails/mypic").getValue(String.class);
		    			    	

			    		    		Myid = My_Details.getString("Myid",null);
			    		    		 Log.i("Myid", Myid);
				    			    	  id = snapshot1.child("attendeesDetails/myid").getValue(String.class);
				    			    	 if(id.equals(Myid)){
				    			    		  edit_text.setText("edit challenge");
				    			    		  name = "Owner";
				    			    		
				    			    	  }else{
				    			    		  name = snapshot1.child("attendeesDetails/myname").getValue(String.class);
				    			    	  }
				    			    			    		    	
	    		    		
	    		    		outer = new RelativeLayout(getApplicationContext()); 
	    			    	RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
	    			                RelativeLayout.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
	    			    	outer.setLayoutParams(rlp);
	    			    	outer.setClickable(true);
	    			    	outer.setBackgroundResource(R.drawable.gradient1); 
	    			    	
	    			    	 hash_attendes_click.put(outer,id);	
	    			    	 
	    			    	 JSONObject job_user = new JSONObject(pic);
	 			 			 String user_url = job_user.getString("data");
	 			 			 JSONObject job_user_url = new JSONObject(user_url);
	 			 			 final String user_pic_url = job_user_url.getString("url");
	    			    	 
	    			    		ImageView imageView = new ImageView(getApplicationContext()); 
	    			    		RelativeLayout.LayoutParams ilp = new RelativeLayout.LayoutParams(100,100);
	    			    		imageView.setLayoutParams(ilp); 
	    			    		ilp.addRule(RelativeLayout.CENTER_VERTICAL);
	    			    		ilp.addRule(RelativeLayout.ALIGN_RIGHT ); 
	    			    		
	    			    		TextView textView = new TextView(getApplicationContext());
	    			    		RelativeLayout.LayoutParams tlp = new RelativeLayout.LayoutParams(
	    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
	    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
	    			    		tlp.addRule(RelativeLayout.CENTER_IN_PARENT); 
	    			    		textView.setLayoutParams(tlp);
	    			    		textView.setTextColor(Color.parseColor("#424242")); 
	    			    		textView.setTextSize(15);
	    			    		textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
	    			    		textView.setText(name); 
	    			    		
	    			    		TextView tv_vp= new TextView(getApplicationContext());
	    			    		RelativeLayout.LayoutParams tlp1 = new RelativeLayout.LayoutParams(
	    			    		RelativeLayout.LayoutParams.WRAP_CONTENT,
	    			            RelativeLayout.LayoutParams.WRAP_CONTENT);
	    			    		tlp1.addRule(RelativeLayout.ALIGN_PARENT_END);
	    			    		tlp1.addRule(RelativeLayout.CENTER_VERTICAL); 
	    			    		tv_vp.setLayoutParams(tlp1);
	    			    		tv_vp.setPaddingRelative(0, 0, 2, 0);
	    			    		tv_vp.setTypeface(Typeface.SERIF,Typeface.BOLD);
	    			    		tv_vp.setTextColor(Color.parseColor("#4dd0e1")); 
	    			    		tv_vp.setTextSize(12); 
	    			    		tv_vp.setText("     View\nPredictions"); 
	    			    		
	    			    		
	    			    		  outer.addView(imageView);
	    			    		  outer.addView(textView);
	    			    		  outer.addView(tv_vp);
	    			    			
	    			    		  
	    			    			attendes_layout.addView(outer);
				 			        imageLoader.DisplayImage(user_pic_url, imageView); 
				 			
				 			       outer.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {  
											 RelativeLayout out = (RelativeLayout)v;
											 id = hash_attendes_click.get(out); 
											
											
											    current_event_editor.putString("id",id );
					  			                current_event_editor.putString("name",name );
					  			                current_event_editor.putString("pic",pic );
					  			                current_event_editor.commit();		
					  			                
											 
											      Bundle b = new Bundle();
				    							  Intent i = new Intent(getApplicationContext(), SportzApp_ViewPredictions.class);
				    							  i.putExtras(b);
				    							  startActivity(i);
									
									}		
								});	
	    		    	}
    		    }catch(Exception e){
    		    	
    		    	e.printStackTrace();
    		    	
    		      }
    		    }

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}

		
    		    
            });
       
	} 
	
		
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{ 
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream)); 
		String line = ""; 
		String result = "";
		while((line = bufferedReader.readLine()) != null) 
			result += line; 
			inputStream.close(); 
			return  result; 
	}
	
			
	private class DownloadImage_A extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            im1.setImageBitmap(result); 
	        }
	    }
	
	 private class DownloadImage_B extends AsyncTask<String, Void, Bitmap> {  
	        @Override
	        protected Bitmap doInBackground(String... URL) { 
	            String imageURL = URL[0];
	 
	            Bitmap bitmap = null;
	            try { 
	                InputStream input = new java.net.URL(imageURL).openStream(); 
	                bitmap = BitmapFactory.decodeStream(input);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return bitmap;
	        } 
	        @Override
	        protected void onPostExecute(Bitmap result) { 
	            im2.setImageBitmap(result); 
	        }
	    }
	
	 public void sendchallenge(){		   
		 
            Intent i = new Intent(Sportz_Event_Details.this, Sportsapp_challengesnew.class);				
			Bundle b = new Bundle();
			i.putExtras(b);
			startActivity(i);
	    }
		public String uploadPhoto(Facebook facebook, Bitmap image, String albumId) {
			
			Log.d("FB ", "1");
			if (albumId == null) {
				albumId = "me";
				Log.d("FB ", "2");
			}
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			Log.d("FB ", "3");
			image.compress(CompressFormat.JPEG, 75, bos);
			Log.d("FB ", "4");
			byte[] photoBytes = bos.toByteArray();
			Log.d("FB ", "5");

			Bundle params = new Bundle();
			Log.d("FB ", "6");
			params.putByteArray("picture", photoBytes);
			Log.d("FB ", "7");
			try {

				String resp = facebook.request(albumId + "/photos", params, "POST");
				Log.d("FB ", "8");
				JSONObject json = Util.parseJson(resp);
				Log.d("FB ", "9");
				return json.getString("id");			
			} catch (IOException e) {
				Log.d("FB ", "10");
				System.out.println("IOException " + e);
				//Toast.makeText(this, "IOException " + e, Toast.LENGTH_SHORT).show();
			} catch (FacebookError e) {
				Log.d("FB ", "11");
				System.out.println("FacebookError " + e);
				//Toast.makeText(this, "FacebookError " + e, Toast.LENGTH_SHORT).show();
			} catch (JSONException e) {
				Log.d("FB ", "12");
				System.out.println("JSONException " + e);
				//Toast.makeText(this, "JSONException " + e, Toast.LENGTH_SHORT).show();
			}
			Log.d("FB ", "13");
			return null;
		}
		
		private void showImageDialog_Screen(final Bitmap bitmap_screen,	final String option) {
			try {
				LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(
						R.layout.sportzapp_facebookshare_dialog,(ViewGroup) findViewById(R.id.ViewImageDialogRoot_screen));

				ed_screen_shot = (EditText) layout.findViewById(R.id.ed_screenshot_comment);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setView(layout);
				builder.setTitle("Screenshot");
				builder.setPositiveButton("Close", null);
				builder.setNegativeButton("Post Image",
						new DialogInterface.OnClickListener() {
							@SuppressLint("NewApi")
							@Override
							public void onClick(DialogInterface dialog, int which) {

								String filepath = "";

								if (option.equals("gallery")) {

									File root = new File(Environment.getExternalStorageDirectory(),	"SportsApp");
									if (!root.exists()) {
										root.mkdir();
									}
									filepath = root.getAbsolutePath() + "/"+ getFileName();
									FileOutputStream fos = null;
									try {
										fos = new FileOutputStream(filepath);
										bitmap_screen.compress(Bitmap.CompressFormat.JPEG, 100,fos);
										fos.flush();
										fos.close();
									} catch (FileNotFoundException e) {
										e.printStackTrace();
									} catch (Exception e) {
										e.printStackTrace();
									}

									Toast.makeText(getApplicationContext(),	"Screenshot Saved to " + filepath,Toast.LENGTH_SHORT).show();

								}  else if (option.equals("facebook")) {

									screen_comment = ed_screen_shot.getText().toString();
									uploadPhoto(SportzApp_FB_Invite.facebook, bitmap_screen, null);
									

									Toast.makeText(getApplicationContext(),	"Image Uploaded to Facebook wall",Toast.LENGTH_SHORT).show();
								}
								imageDialog_screen.dismiss();
							}

							private String getFileName() {
								// TODO Auto-generated method stub
								return null;
							}
						});
				imageDialog_screen = builder.create();
				imageDialog_screen.show();
				this.imageViewSelectedImage_screen = (ImageView) imageDialog_screen.findViewById(R.id.ImageViewSourceImage_screen);
				imageViewSelectedImage_screen.setImageBitmap(bitmap_screen);

			} catch (Exception x) {
				Log.e("Exception", x + " 2");
				//Toast.makeText(getApplicationContext(), "" + x, Toast.LENGTH_SHORT).show();
			}
		}

}
