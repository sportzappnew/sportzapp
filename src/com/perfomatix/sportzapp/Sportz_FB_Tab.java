package com.perfomatix.sportzapp; 

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class Sportz_FB_Tab extends TabActivity {  
	
	SharedPreferences current_event;
    Editor current_event_editor;  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportz_fb_tab);
		
		current_event = getSharedPreferences("current_event", Context.MODE_PRIVATE);
		current_event_editor = current_event.edit();
		
	
		
		 @SuppressWarnings("unused")
		Resources ressources = getResources(); 
	     TabHost tabHost = getTabHost(); 
	     
	     ActionBar actionBar = getActionBar(); 
	     actionBar.setDisplayHomeAsUpEnabled(true);
	     
	     Intent intentGroups = new Intent().setClass(this, SportzApp_FB_Groups.class);
	     TabSpec tabSpecGroups = tabHost.newTabSpec("Groups")
	                .setIndicator("Group", null)
	                .setContent(intentGroups);
	     Intent intentInvite = new Intent().setClass(this, SportzApp_Friends.class);
	     current_event_editor.putString("GroupFriends","Friends" );
		    current_event_editor.commit();
	     TabSpec tabSpecInvite = tabHost.newTabSpec("Friends")
	                .setIndicator("Friends", null)
	                .setContent(intentInvite);  

	     tabHost.addTab(tabSpecInvite);
	     tabHost.addTab(tabSpecGroups);
	    
	      
	     tabHost.setCurrentTab(0); 
	 
 
	} 
}
